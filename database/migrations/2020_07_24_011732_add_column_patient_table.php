<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->string('type_blood')->nullable();
            $table->string('occupation')->nullable();
            $table->string('civil_status')->nullable();
            $table->string('sex')->nullable();
            $table->string('phone')->nullable();
            $table->string('state_province')->nullable();
            $table->string('country')->nullable();
            $table->string('cedula')->nullable();
            $table->string('name')->nullable();
            $table->text('home')->nullable();

        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('type_blood');
            $table->dropColumn('occupation');
            $table->dropColumn('civil_status');
            $table->dropColumn('sex');
            $table->dropColumn('phone');
            $table->dropColumn('state_province');
            $table->dropColumn('country');
            $table->dropColumn('cedula');
            $table->dropColumn('name');
            $table->dropColumn('home');

        });
    }
}
