<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuotesMedical extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicals_quotes', function (Blueprint $table) {
            $table->integer('id_medical')->nullable();
            $table->integer('id_user')->nullable();
            $table->date('date_quote')->nullable();
            $table->time('time_quote')->nullable();
            $table->integer('status')->nullable();
            //status --> 0 pending
            //status --> 1 confirm
            //status --> 2 denegate
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
