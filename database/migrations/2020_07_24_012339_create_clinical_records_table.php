<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicalRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinical_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string("height")->nullable();
            $table->string("weight")->nullable();
            $table->string("imc")->nullable();
            $table->string("temperature")->nullable();
            $table->string("systolic_pressure")->nullable();
            $table->string("diastolic_pressure")->nullable();
            $table->string("heart_rate")->nullable();
            $table->text("anamnesis")->nullable();
            $table->text("diagnosis")->nullable();
            $table->text("observation")->nullable();
            $table->text("exploration")->nullable();
            $table->text("treatment")->nullable();
            $table->text("evolution")->nullable();
            $table->string("oxygen_saturation")->nullable();

            $table->timestamps();
        });

        Schema::table('clinic_histories', function (Blueprint $table) {
            $table->unsignedBigInteger('clinical_records_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinical_records');
        Schema::table('clinic_histories', function (Blueprint $table) {
            $table->dropColumn('clinical_records_id');
        });
    }
}
