<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsMedicinesRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicines_requests', function (Blueprint $table) {
          
            $table->string('phone')->nullable();
            $table->string('state_province')->nullable();
            $table->string('country_name')->nullable();
            $table->string('name_customer')->nullable();

          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { Schema::table('medicines_requests', function (Blueprint $table) {
        $table->dropColumn('phone');
        $table->dropColumn('state_province');
        $table->dropColumn('county_name');
        $table->dropColumn('name_customer');

    });
    }
}
