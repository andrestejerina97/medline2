<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsRecipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->string('id_disease')->nullable();
            $table->integer('id_medical_quote')->nullable();
            $table->integer('id_medicine')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('id_aditional_information')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes', function (Blueprint $table) {
    
            $table->dropColumn('id_disease');
            $table->dropColumn('id_medical_quote');
            $table->dropColumn('id_medicine');
            $table->dropColumn('quantity');
            $table->dropColumn('id_aditional_information');
        });
    }
}
