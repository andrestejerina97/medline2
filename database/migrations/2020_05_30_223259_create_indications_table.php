<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_medicine')->nullable();
            $table->string('consume_quantity')->nullable();
            $table->string('consume_frecuency')->nullable();
            $table->string('quantity_days')->nullable();
            $table->string('observation')->nullable();
            $table->integer('id_aditional_information')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indications');
    }
}
