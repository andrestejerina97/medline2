<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMedicalsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicals', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->string('age')->nullable();
            $table->string('phone')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('home')->nullable();
            $table->string('mail')->nullable();
            $table->string('ci')->nullable();
            $table->string('birthdate')->nullable();
            $table->integer('id_clinic')->nullable();
            $table->integer('id_specialty')->nullable();
            $table->string('patients_per_day')->nullable();
            $table->string('work_phone')->nullable();
            $table->float('price_inquire')->nullable();
            $table->integer('id_packeges')->nullable();
            
            $table->integer('consulting_room')->nullable();
            $table->string('attention_hours')->nullable();
            $table->string('photo')->nullable();
            
            $table->integer('destake')->nullable();
            $table->integer('active')->nullable();
            $table->integer('id_user')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medicals', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('age');
            $table->dropColumn('phone');
            $table->dropColumn('cellphone');
            $table->dropColumn('home');
            $table->dropColumn('mail');
            $table->dropColumn('ci');
            $table->dropColumn('birthdate');
            $table->dropColumn('id_clinic');
            $table->dropColumn('id_specialty');
            $table->dropColumn('patients_per_day');
            $table->dropColumn('work_phone');
            $table->dropColumn('price_inquire');
            $table->dropColumn('id_packeges');

        });

    }
}
