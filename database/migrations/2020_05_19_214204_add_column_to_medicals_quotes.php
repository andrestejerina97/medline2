<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToMedicalsQuotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicals_quotes', function (Blueprint $table) {
            $table->string("reason")->nullable();
            $table->integer("id_clinical")->nullable();
            $table->string("current_illness")->nullable();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medicals_quotes', function (Blueprint $table) {
            $table->dropColumn("reason");
            $table->dropColumn("id_clinical");
            $table->dropColumn("current_illness");

        });
    }
}
