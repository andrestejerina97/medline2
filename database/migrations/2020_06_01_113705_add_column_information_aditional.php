<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInformationAditional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipe_aditional_informations', function (Blueprint $table) {
            $table->integer('id_medical')->nullable();
   

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipe_aditional_informations', function (Blueprint $table) {
            $table->dropColumn('id_medical');

        });
    }
}
