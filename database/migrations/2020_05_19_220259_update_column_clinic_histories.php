<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnClinicHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_histories', function (Blueprint $table) {
            $table->integer("id_medical_report")->nullable();
            $table->integer("id_medical_quote")->nullable();


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_histories', function (Blueprint $table) {
            $table->dropColumn("id_medical_report");
            $table->dropColumn("id_medical_quote");


        });
    }
}
