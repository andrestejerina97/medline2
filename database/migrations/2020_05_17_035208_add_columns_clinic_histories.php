<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsClinicHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_histories', function (Blueprint $table) {
            $table->string('description')->nullable();
            $table->integer('id_user')->nullable();
            $table->integer('id_medical')->nullable();
            $table->time('time_at')->nullable();
            //status --> 0 pending
            //status --> 1 confirm
            //status --> 2 denegate
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
