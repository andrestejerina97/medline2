<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
define('STDIN',fopen("php://stdin","r"));
Route::get('install', function() {
    Artisan::call('migrate',['--force'=>true]);
});

*/


Route::get('/','HomeController@index')->name('index');
Route::get('/Medical','MedicalsController@index')->name('Medicals');
Route::get('/Profile','MedicalsController@ProfileHome')->name('MedicalProfile');
Route::get('/Hola','MedicalsController@ProfileHome')->name('Hola');
Route::get('/Recipe','MedicalsController@RecipeHome')->name('MedicalRecipe');
Route::get('/Medicines','MedicinesController@index')->name('Medicines');
Route::post('/Medicines/search','MedicinesController@search')->name('SearchMedicines');

Route::post('/SavePurchase','MedicinesRequestController@SavePurchase')->name('SavePurchase');
Route::post('/Checkout','MedicinesRequestController@checkout')->name('Checkout');
Route::post('/UpdateMedical','MedicalsController@Update')->name('UpdateMedical');
Route::get('/Medicines/{category}','MedicineCategoriesController@index')->name('MedicinesForCategory');
Route::get('/Medicals/{category}','MedicalsController@filter')->name('MedicalsForspecialty');
Route::post('/Medicines/CartRegister','MedicinesRequestController@CartRegister')->name('CartRegister');
Route::get('/Medicals','MedicalsController@home')->name('MedicalsHome');
//Route::get('/Patients','MedicalsController@patients')->name('PatientsHome');
Route::get('/clinicHistories','MedicalsController@clinicHistories')->name('ClinicHistoriesHome');

Route::post('/SaveMedicals','MedicalsController@save')->name('SaveMedicals');
Route::post('/UpdateMedicals','MedicalsController@update')->name('UpdateMedicals');
Route::post('/DeleteMedicals','MedicalsController@delete')->name('DeleteMedicals');
Route::post('/ShowMedical/{medic}','MedicalsController@showMedical')->name('ShowMedical');

//quotes
Route::get('/NewQuote/{medical}','MedicalsQuotesController@newQuote')->name('NewQuote');
Route::post('/NewQuote','MedicalsQuotesController@save')->name('SaveQuote');
Route::get('/Quotes','MedicalsQuotesController@home')->name('Quotes');
Route::get('/Quotes/{quote}','MedicalsQuotesController@filter')->name('FilterQuote');
Route::post('/Quotes/AcceptRequest','MedicalsQuotesController@accept')->name('AcceptRequest');
Route::post('/Quotes/SendAcceptedMail','MedicalsQuotesController@SendAcceptedMail')->name('SendAcceptedMail');
Route::post('/Quotes/SendNewMail','MedicalsQuotesController@SendNewMail')->name('SendNewMail');
Route::post('/New-quote/{id}','MedicalsQuotesController@saveQuoteResource')->name('SaveQuoteResource');
Route::post('/FinalizeQuote/{id}','MedicalsQuotesController@finalizeQuote')->name('FinalizeQuote');


//end quotes
Route::get('/Queries','QueriesController@home')->name('Queries');

Route::get('/SearchQuotes','MedicalsQuotesController@search')->name('SearchQuotes');

//patients
Route::get('/Patients','PatientsController@home')->name('PatientsHome');
Route::get('/Patients/Filter/{patient}','PatientsController@filter')->name('FilterPatient');
Route::post('/update-patient','PatientsController@update')->name('UpdatePatient');
//end patients

//HIstory clinical
Route::post('/History/filter','ClinicHistoriesController@filterByQuote')->name('FilterHistory');
Route::post('/ClinicHistory','ClinicHistoriesController@home')->name('ClinicHistory');
Route::post('/guardar-datos-clinicos/{id_clinical_history?}','ClinicHistoriesController@saveClinicalRecord')->name('ClinicalHistory.save.clinical_record');

//End History Clinical





//end patient

// recipes
Route::get('/Recipes/{quote}','RecipesController@home')->name('NewRecipe');
Route::post('/NewReport/{id_clinic_history}','ReportFilesController@save')->name('ClinicalHistory.save.evaluation');
Route::post('/Recipe/Search','RecipesController@searchMedicine')->name('SearchMedicineForRecipe');
Route::post('/Recipes/SaveRecipe','RecipesController@save')->name('SaveRecipe');
Route::post('/Recipes/SearchDiseases','RecipesController@searchDiseases')->name('SearchDiseasesForRecipe');
Route::get('/NewRecipe','RecipesController@newRecipe')->name('RecipeAlternative');
Route::post('/Recipes/SaveNewRecipe','RecipesController@saveNewRecipe')->name('SaveNewRecipe');
Route::post('/Recipes/SendRecipe','RecipesController@downloadPdf')->name('PfdSendRecipe');
Route::get('/Recipes/show/all','RecipesController@showAll')->name('RecipeAlls');
Route::get('/Recipes/delete/{recipe_number}','RecipesController@delete')->name('RecipeDelete');
Route::post('/Recipes/dowload','RecipesController@downloadPdf')->name('RecipeDowloadPdf');

Route::get('/Recipes/sendMail/{recipe_number?}','RecipesController@sendPdf')->name('sendPdf');

//end recipes

Route::namespace('admin')->group(function(){
    Route::get('/admin/home','MedicinesController@RequestCurrent')->name('HomeAdmin');
    Route::get('/admin/Medicines','MedicinesController@index')->name('MedicinesHome');
    Route::get('/admin/MedicinesRequest','MedicinesController@RequestCurrent')->name('MedicinesRequest');
    Route::post('/admin/SaveMedicines','MedicinesController@save')->name('SaveMedicines');
    Route::post('/admin/UpdateMedicines','MedicinesController@update')->name('UpdateMedicines');
    Route::post('/admin/DeleteMedicines','MedicinesController@delete')->name('DeleteMedicines');
    Route::get('/admin/Categories','CategoriesController@home')->name('Categories');
    Route::post('/admin/DeleteCategories','CategoriesController@delete')->name('DeleteCategories');
    Route::post('/admin/SaveCategories','CategoriesController@save')->name('SaveCategories');
    Route::post('/admin/UpdateCategories','CategoriesController@update')->name('UpdateCategories');
    Route::get('/admin/truck/{numbertruck}','MedicinesController@TruckList')->name('truckList');
    Route::get('/admin/sended/{numbertruck}','MedicinesController@MedicineSended')->name('MedicineSended');
    Route::post('/admin/DeleteCarousel','CarouselController@delete')->name('DeleteCarrousel');
    Route::post('/admin/SaveCarousel','CarouselController@save')->name('SaveCarrousel');
    Route::post('/admin/UpdateCarousel','CarouselController@update')->name('UpdateCarousel');
    Route::get('/admin/Carousel','CarouselController@index')->name('CarouselHome');
    Route::get('/admin/Specialties','SpecialtiesController@home')->name('Specialties');
    Route::post('/admin/SaveSpecialties','SpecialtiesController@save')->name('SaveSpecialties');
    Route::post('/admin/UpdateSpecialties','SpecialtiesController@update')->name('UpdateSpecialties');
    Route::post('/admin/DeleteSpecialties','SpecialtiesController@delete')->name('DeleteSpecialties');
    Route::get('/admin/Clinics','ClinicsController@home')->name('Clinics');
    Route::post('/admin/SaveClinics','ClinicsController@save')->name('SaveClinics');
    Route::post('/admin/UpdateClinics','ClinicsController@update')->name('UpdateClinics');
    Route::post('/admin/DeleteClinics','ClinicsController@delete')->name('DeleteClinics');
    Route::get('/admin/Medicals','MedicalsController@home')->name('Medicals');
    Route::post('/admin/SearchMedicalTable','MedicalsController@searchMedicalTable')->name('SearchMedicalTable');
    Route::post('/admin/SaveMedicals','MedicalsController@save')->name('SaveMedicals');
    Route::get('/admin/UpdateMedicalsView/{medical}','MedicalsController@updateView')->name('UpdateMedicalsView');
    Route::post('/admin/UpdateMedicals','MedicalsController@update')->name('UpdateMedicals');
    Route::post('/admin/DeleteMedicals','MedicalsController@delete')->name('DeleteMedicals');
    Route::get('/admin/Packeges','PackegesController@home')->name('Packeges');
    Route::post('/admin/SavePackeges','PackegesController@save')->name('SavePackeges');
    Route::post('/admin/UpdatePackeges','PackegesController@update')->name('UpdatePackeges');
    Route::post('/admin/DeletePackeges','PackegesController@delete')->name('DeletePackeges');
    Route::post('/admin/SearchUser','UsersController@search')->name('SearchUser');
    Route::post('/admin/SearchMedicinesTable','MedicinesController@SearchMedicinesTable')->name('SearchMedicinesTable');
    Route::get('/admin/users','UsersController@home')->name('Users');
    Route::post('/admin/usersFilter','UsersController@SearchUserTable')->name('SearchUserTable');

    
});




Auth::routes();

Route::namespace('Auth')->group(function(){
Route::post('/loginForm', 'LoginAjaxController@login')->name('loginForm');
Route::post('/loginRegister', 'LoginAjaxController@register')->name('loginRegister');

});
Route::get('/home', 'HomeController@index')->name('home');
define('STDIN',fopen("php://stdin","r"));
Route::get('install', function() {
    Artisan::call('migrate',['--force'=>true]);
});