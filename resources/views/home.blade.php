<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Title Tag  -->
    <title>{{ config('app.name', 'MedlineApp') }}</title>
	<!-- Favicon -->
	<link rel="icon" href="{{asset('img/ICONO.ico')}}" type="image/ico">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800;900&display=swap" rel="stylesheet"> 	
	<!-- StyleSheet -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{asset('public/css/bootstrap.css')}}">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('public/css/magnific-popup.min.css')}}">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('public/css/font-awesome.css')}}">
	<!-- Fancybox -->
	<link rel="stylesheet" href="{{asset('public/css/jquery.fancybox.min.css')}}">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="{{asset('public/css/themify-icons.css')}}">
	<!-- Jquery Ui -->
    <link rel="stylesheet" href="{{asset('public/css/jquery-ui.css')}}">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('public/css/niceselect.css')}}">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('public/css/animate.css')}}">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="{{asset('public/css/flex-slider.min.css')}}">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="{{asset('public/css/owl-carousel.css')}}">
	<!-- Slicknav -->
    <link rel="stylesheet" href="{{asset('public/css/slicknav.min.css')}}">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="{{asset('public/css/reset.css')}}">
	<link rel="stylesheet" href="{{asset('public/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/responsive.css')}}">

</head>
<body class="js" >
    
    @include('layouts.preloader')
	
	@if(Auth::check())
	 @switch(Auth::user()->role_id)
		 @case(1)
		 @include('public.header.admin')

			 @break
		 @case(2)
		 @include('public.header.customer-home')

			 @break
		@case(3)
		@include('public.header.medical')

		@break
		 @default
		 @include('public.header.customer-home')

	 @endswitch

	@else
	@include('public.header.customer-home')

  @endif

<!-- Start Single Tab -->
<!-- Start Most Popular -->
<div class="product-area most-popular ">
    <div class="container-fluid">
        <div class="row mt-5" >
            <div class="col-12 col-xs-12 col-lg-12 mx-auto">
                <div class="owl-carousel popular-slider">
                    @isset($carousels)
                    @foreach ($carousels as $carousel)
                        <div class="product-img ">
                            <a>
                                <img  class="default-img" style="max-height: 26rem;" src="{{asset('storage/public/carousels/'.$carousel->file)}}"
                                    alt="#">
                            </a>
                        </div>
                        @endforeach

                        @endisset
                     </div>
            </div>
        </div>
        <div class="col-12 col-xs-12 col-md-12 col-lg-12 mb-4 ">
            <div class="section-title mt-4">
                <button class="button-andres-home" ><i class="fa fa-user-md"></i> <a href="{{route('MedicalsHome')}}">Médicos </a></button>
                <button class="button-andres-home" ><i class="fa fa-search"></i> <a href="{{route('Medicines')}}">Productos </a></button>
                <button class="button-andres-home" ><i class="fa fa-truck "></i> <a href="{{route('MedicalsHome')}}">Telemedicina</a></button>
            </div>
        </div>
        <div class="col-12">
            <div class="section-title">
               <h1 style="color: #35ade2"><a href="#">PRODUCTOS DESTACADOS</a></h1>
            </div>
        </div>
        <div class="row">
            @isset($medicines)

            @foreach ($medicines as $medicine)

            <div class="col-lg-3 col-md-3 col-xs-3 col-4 border">

                <div class="single-product-home">
                    <div class="product-img-farm text-center">
                        <a>
                            <img loading="lazy" class="img-home-andres"  src="{{asset('storage/public/medicines/'.$medicine->resource)}}"
                                alt="Imagen No disponible">
                        </a>
                    </div>
                    <div  id='{{$medicine->id ."message_add_cart"}}'>
                    </div>
                    <h4  style="display:none" class="item_id" >{{$medicine->id}}</h4>
                    <input style="display:none" value="{{$medicine->name}}"  class="item_name">
                    <input style="display:none" value="{{$medicine->offer_price}}"  class="item_price">

					<div class="mt-3">
                        <h5 class="text-center ">{{$medicine->name}}</h5>
                        <p class="text-center ">{{$medicine->active_principle}}</p>
                        <p class="text-center item_description">{{$medicine->description}}</p>

					</div>
					<div class=" row mb-1 justify-content-center">
                        <h4  >${{$medicine->offer_price}}</h4>

                        <span ><del>${{$medicine->price}}<del> </span>
                            
                            <div class="col-md-4 col-lg-5 col-xs-10 col-10">
                                <input type="number" class="form-control item_quantity" value="1">
                              </div>
                    </div>
                    <div class="text-center">
                        <button  class="item_add payment  button-andres-add-cart" >Añadir al carrito <i class="ti-shopping-cart"></i></button>
                      </div>
                    
                </div>
               
            </div>
            @endforeach
            @endisset
		</div>
		<div class="col-12 col-lg-12 mb-4 mx-auto mt-2">
            <div class="section-title">
				<button class="button-andres-home " style="background-color: #144484;" >
					<a href="{{route('Medicines')}}">IR A FARMACIA </i></a></button>
            </div>
		</div>
		<div class="mb-3 mt-1">
			<img src="{{asset('img/home.jpeg')}}" alt="">

		</div>

    </div>
</div>
<!--/ End Single Tab -->
  <!-- Modal -->
  <div class="modal fade" id="modal_payment" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
        </div>
        <div class="modal-body">
          <div class="row no-gutters">           
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
              <div class="quickview-content">
                <h4 class="widget_title">Mi carrito de compra:</h4>

                <div class="simpleCart_items table-responsive" >

                 <!-- <table class="table table-hover">
                    <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Subtotal</th>
                  </tr>
              </table>
                -->
                    <div class="simpleCart_table_body"></div>

                 

                </div>
                -----------------------------<br/>
                Final Total: <span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span> <br />
                <div class="modal-footer">
                 <button type="button" class="btn " data-dismiss="modal">cerrar</button>
                 <button type="button"  class="btn  btn_confirmar1 ">Confirmar Compra</button>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal end -->

    <!-- Modal -->
    <div class="modal fade" id="modal_register" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
          </div>
          <div class="modal-body">
            <div class="row no-gutters">           
              <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
                <div class="quickview-content">
                  <h4 class="widget_title">Formulario de registro</h4>

                  @include('auth.formRegister')

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal end -->

<!-- Modal -->
<div class="modal fade" id="modal_login" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
      </div>
      <div class="modal-body">
        <div class="row no-gutters">           
          <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
            <div class="quickview-content ">
              <div id="login_error" style="display: none" class="alert alert-danger alert-dismissible fade show">
                
            </div>
              <h4 class="widget_title">Ups! parece que no iniciaste sesión</h4>

              @include('auth.formLogin')
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal end -->

<input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">


<!--############SECTION CHECKOUT#############-->


@include('public.medicines.checkout')

@include('public.footer.footer	')

    <!-- Jquery -->
  
    <script src="{{asset('js/public/jquery.min.js')}}"></script>
    <script src="{{asset('js/public/jquery-migrate-3.0.0.js')}}"></script>
	<script src="{{asset('js/public/jquery-ui.min.js')}}"></script>
	<!-- Popper JS -->
	<script src="{{asset('js/public/popper.min.js')}}"></script>
	<!-- Bootstrap JS -->
	<script src="{{asset('js/public/bootstrap.min.js')}}"></script>
	<!-- Color JS -->
	<!-- Slicknav JS -->
	<script src="{{asset('js/public/slicknav.min.js')}}"></script>
	<!-- Owl Carousel JS -->
	<script src="{{asset('js/public/owl-carousel.js')}}"></script>
	<!-- Magnific Popup JS -->
	<script src="{{asset('js/public/magnific-popup.js')}}"></script>
	<!-- Fancybox JS -->
	<script src="{{asset('js/public/facnybox.min.js')}}"></script>
	<!-- Waypoints JS -->
	<script src="{{asset('js/public/waypoints.min.js')}}"></script>
	<!-- Countdown JS -->
	<script src="{{asset('js/public/finalcountdown.min.js')}}"></script>
	<!-- Nice Select JS -->
	<script src="{{asset('js/public/nicesellect.js')}}"></script>
	<!-- Ytplayer JS -->
	<script src="{{asset('js/public/ytplayer.min.js')}}"></script>
	<!-- Flex Slider JS -->
	<script src="{{asset('js/public/flex-slider.js')}}"></script>
	<!-- ScrollUp JS -->
	<script src="{{asset('js/public/scrollup.js')}}"></script>
	<!-- Onepage Nav JS -->
	<script src="{{asset('js/public/onepage-nav.min.js')}}"></script>
	<!-- Easing JS -->
	<script src="{{asset('js/public/easing.js')}}"></script>
	<!-- Active JS -->
	<script src="{{asset('js/public/active.js')}}"></script>
		<script type="text/javascript">
		 $('.popular-slider').owlCarousel({
            items: 1,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 400,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            autoplayHoverPause: true,
            loop: true,
            nav: true,
            merge: true,
            dots: false,
            navText: ["<img src='../img/flecha-carousel-left.PNG'>","<img src='../img/flecha-carousel-right.PNG'>"],
            responsive: {
                0: {
                    items: 1,
                },
                300: {
                    items: 1,
                },
                480: {
                    items: 1,
                },
                768: {
                    items: 1,
                },
                1170: {
                    items: 1,
                },
            }
        });

        
//##############End Last Page - Next Page####################

$(function(){



var dataCart=Array();
var dataItem= new FormData();


//##############Last Page - Next Page#####################
$(document).on('click','.next_page',function() {
$.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });

       dataItem.append('totalItems',dataCart.totalItems);
       dataItem.append('numberItems',dataCart.numberItem);
       dataItem.append('total',dataCart.total);
       dataItem.append('rebate',dataCart.rebate);

     $.ajax({
         url: '{{ route('CartRegister') }}',
         type: 'POST',
         data:dataItem,
         contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
         processData: false,
         success: function(data) {
          location.href=' {{$medicines->nextPageUrl() }}';

         }
     });
});

$(document).on('click','.last_page',function() {
$.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });

       dataItem.append('totalItems',dataCart.totalItems);
       dataItem.append('numberItems',dataCart.numberItem);
       dataItem.append('total',dataCart.total);
       dataItem.append('rebate',dataCart.rebate);

     $.ajax({
         url: '{{ route('CartRegister') }}',
         type: 'POST',
         data:dataItem,
         contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
         processData: false,
         success: function(data) {
          location.href=' {{$medicines->previousPageUrl() }}';

         }
     });
    
});

  //#########CART SESION###########
  @if(Session::has('numberItems'))
          
          dataCart.totalItems= {{ Session::get('totalItems')}};
          dataCart.numberItem= {{ Session::get('numberItems')}};
          dataCart.total= {{ Session::get('total')}};
          dataCart.rebate= {{ Session::get('rebate')}};
          $(".simpleCart_grandTotal").text(parseFloat(dataCart.total.toFixed(2)));
        $(".simpleCart_quantity").text(parseInt(dataCart.totalItems));
    
        @for($aux = 1; $aux <= Session::get('numberItems'); $aux++)
            index={{$aux}};

            @if(Session::has('numberItem'.$aux))

            dataItem.append('idproduct'+index,'{{ Session::get('idproduct'.$aux)}}');
            dataItem.append('numberItem'+index,'{{ Session::get('numberItem'.$aux)}}');
            dataItem.append('name'+index,'{{ Session::get('name'.$aux)}}');
            dataItem.append('description'+index,'{{ Session::get('description'.$aux)}}');
            dataItem.append('quantity'+index,'{{ Session::get('quantity'.$aux)}}');
            dataItem.append('price'+index,'{{ Session::get('price'.$aux)}}');
            dataItem.append('subtotal'+index,'{{ Session::get('subtotal'.$aux)}}');      
            @endif
        @endfor
  @else
  var dataCart={
          totalItems:0,
          numberItem:0,
          total: 0.00,
          rebate: 0 ,
          }
          var dataItem = new FormData();
@endif
  
  //########CART SESION############

  $(".paymentFinalice").click(function() {
    $("#modal_payment").modal("show");
  })
  $(document).on('click','.btn_confirmar1',function() {
//##################REGISTER CART###############
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

          dataItem.append('totalItems',dataCart.totalItems);
          dataItem.append('numberItems',dataCart.numberItem);
          dataItem.append('total',dataCart.total);
          dataItem.append('rebate',dataCart.rebate);

        $.ajax({
            url: '{{ route('CartRegister') }}',
            type: 'POST',
            data:dataItem,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            success: function(data) {
              if($("#id_login").val()==0){
                $("#modal_login").modal("show");


              }else{
              $("#modal_payment").modal("hide");
              // $("#modalPayment2").modal("show");
              $("#shop_section").hide();
              $("#checkout_section").show();
              }

            }
        });
 
//####################END REGISTER CART######################

   
  })
  $(".back_shop").click(function() {
   // $("#modalPayment2").modal("show");
   $("#shop_section").show();
   $("#checkout_section").hide();
  })

  $(document).on('click','#btn_registering',function() {
   // $("#modalPayment2").modal("show");
   $("#modal_login").modal("hide");
   $("#modal_register").modal("show");

  })

//##################FINALLY PAYMENT###############
$(document).on('click','#btn_finally_payment',function (e) {

  $("#btn_finally_payment").attr('disabled',true);
  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

          e.preventDefault();
          dataItem.append('totalItems',dataCart.totalItems);
          dataItem.append('numberItem',dataCart.numberItem);
          dataItem.append('total',dataCart.total);
          dataItem.append('rebate',dataCart.rebate);
          if($("#form_checkout  #name_customer").val() !='' &&
          $("#form_checkout  #home").val()!='' &&
          $(" #form_checkout  #code_postal").val()!='' &&
          $("#form_checkout  #phone").val()!=''){
          dataItem.append('name_customer',$("#form_checkout  #name_customer").val());
          dataItem.append('home',$("#form_checkout  #home").val());
          dataItem.append('code_postal',$(" #form_checkout  #code_postal").val());
          dataItem.append('phone',$("#form_checkout  #phone").val());
          dataItem.append('state_province',$("#state_province option:selected").val());
          dataItem.append('country_name',$("#country_name option:selected").val());
          $.ajax({
            url: '{{ route('SavePurchase') }}',
            type: 'POST',
            data:dataItem,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            success: function(data) {
              $("#btn_finally_payment").attr('disabled',false);
                  if (data.message == 'exito') {
                    let au='<div class="alert alert-success msj" role="alert"><p>'+"Su pedido fue registrado,nos comunicaremos en un momento."+'</p> </div>';
                     $("#message_home").html(au);
                    $("#modal_payment").modal("hide");
                      $("#modal_login").modal("hide");
                      $("#shop_section").show();
                      $("#checkout_section").hide();

                    }else {
                
                      
                } 
            },
            error: function(data){
              $("#btn_finally_payment").attr('disabled',false);

              let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';
                      $("#message_checkout").html(au);
          },
            
        });
          }else{
            $("#btn_finally_payment").attr('disabled',false);
            let au='<div class="alert alert-danger msj" role="alert"><p>'+"Debe completar todos los campos por favor"+'</p> </div>';
                     $("#message_checkout").html(au);
          }
});

//####################END FINALLY PAYMENT######################

//##################REGISTER USER###############
$(document).on('click','#btn_register',function (e) {
  $("#btn_register").attr('disabled',true);

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();
        dataLogin=new FormData();
          if($("#form_register  #name").val() !='' &&
          $("#form_register  #home").val()!='' &&
          $("#form_register  #phone").val()!=''&&
          $("#form_register  #password").val()!=''&&
          $("#form_register  #email").val()!=''
          ){
          dataLogin.append('name',$("#form_register  #name").val());
          dataLogin.append('home',$("#form_register  #home").val());
          dataLogin.append('code_postal',$(" #form_register  #code_postal").val());
          dataLogin.append('phone',$("#form_register  #phone").val());
          dataLogin.append('email',$("#form_register  #email").val());
          dataLogin.append('cedula',$("#form_register  #cedula").val());
          dataLogin.append('password',$("#form_register  #password").val());
          dataLogin.append('password_confirmation',$("#form_register  #password_confirmation").val());
          dataLogin.append('state_province',$("#state_province option:selected").val());
          dataLogin.append('country_name',$("#country_name option:selected").val());
        $.ajax({
            url: '{{ route('loginRegister') }}',
            type: 'POST',
            data:dataLogin,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            success: function(data) {
              $("#btn_register").attr('disabled',false);

                if ($.isEmptyObject(data.error)) {
                  if (data.message == 'exito') {
                    let au='<div class="alert alert-success msj" role="alert"><p>'+"Su registro fué exitoso,ya puede iniciar sesión en Medlineapp </p> </div>";
                     $("#message_login_form").html(au);

                      $("#modal_login").modal("show");
                      $("#modal_register").modal("hide");
                  }else{
                    let au='<div class="alert alert-danger msj" role="alert"><p>'+data.message+'</p> </div>';
                     $("#message_register_form").html(au);
                  }
                      
                } else {
                }
            } ,error: function(data){
              $("#btn_register").attr('disabled',false);

              let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';
                      $("#message_register_form").html(au);
                      //$("#message_login_form .msj").fadeOut(2000);

          },
        });
          }else{
            $("#btn_register").attr('disabled',false);
            let au='<div class="alert alert-danger msj" role="alert"><p>'+"Debe completar todos los campos por favor"+'</p> </div>';
                     $("#message_register_form").html(au);
          }
  });


//####################END REGISTER USER######################
//######################## AJAX LOGIN ###################################
  $(document).on('click','#btn_login',function (e) {
    $("#btn_login").attr('disabled',true);
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();
        var email = $("input[name='login_email']").val();
        var password = $("input[name='login_password']").val();

        $.ajax({
            url: '{{ route('loginForm') }}',
            type: 'POST',
            data: {
                email: email,
                password: password
            },
            success: function(data) {
              $("#btn_login").attr('disabled',false);

                if ($.isEmptyObject(data.errors)) {
                  if (data.message == 'exito') {
                      $("#modal_payment").modal("hide");
                      $("#modal_login").modal("hide");
                      $("#shop_section").hide();
                      $("#checkout_section").show();
                      //Seteo de variables sesión
                      $("#id_login").val(1);
                      $("#btn_header_logout").show();
                      $("#btn_header_login").hide();

                  }else{
                                  $("#btn_login").attr('disabled',false);

                    let au='<div class="alert alert-danger msj" role="alert"><p>'+data.message+' usuario o contraseña no coinciden,vuelva a intentar</p> </div>';
                     $("#message_login_form").html(au);
                    // $("#message_login_form .msj").fadeOut(2000);
                  }
                      
                } else {
                  
                  $("#btn_login").attr('disabled',false);

                }
            },
            error: function(data){
              $("#btn_login").attr('disabled',false);

              let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';
                      $("#message_login_form").html(au);
                      //$("#message_login_form .msj").fadeOut(2000);

               // alert("Por favor,completa todos los campos correctamente para continuar");
               // var errors = data.responseJSON;
                //console.log(errors);
          },
        });
  });

// ########################EVENTOS CARRITO###############################


$(document).on('click','.item_add',function(){
dataCart.numberItem +=1;
name=$(this).parent().parent('div').find('.item_name').val();
quantity=$(this).parent().parent('div').find('.item_quantity').val();
id=$(this).parent().parent('div').find('.item_id').text();
price=$(this).parent().parent('div').find('.item_price').val();
description=$(this).parent().parent('div').find('.item_description').text();
subtotal= parseFloat(price) * parseFloat(quantity);
dataItem.append('idproduct'+dataCart.numberItem,id);
dataItem.append('numberItem'+dataCart.numberItem,dataCart.numberItem);
dataItem.append('name'+dataCart.numberItem,name);
dataItem.append('description'+dataCart.numberItem,description);
dataItem.append('quantity'+dataCart.numberItem,quantity);
dataItem.append('price'+dataCart.numberItem,price);
dataItem.append('subtotal'+dataCart.numberItem,subtotal.toFixed(2));

dataCart.total+= parseFloat(subtotal);
dataCart.totalItems+=parseInt(quantity) ;
$(".simpleCart_grandTotal").text(parseFloat(dataCart.total.toFixed(2)));
$(".simpleCart_quantity").text(parseInt(dataCart.totalItems));
let au='<div class="alert alert-info msj" role="alert"><p>Producto añadido!</p> </div>';
$("#"+id+"message_add_cart").html(au);
$(".msj").fadeOut(2000);


$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

          dataItem.append('totalItems',dataCart.totalItems);
          dataItem.append('numberItems',dataCart.numberItem);
          dataItem.append('total',dataCart.total);
          dataItem.append('rebate',dataCart.rebate);

        $.ajax({
            url: '{{ route('CartRegister') }}',
            type: 'POST',
            data:dataItem,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            success: function(data) {
              if($("#id_login").val()==0){
                $("#modal_login").modal("show");


              }else{
              $("#modal_payment").modal("hide");
              // $("#modalPayment2").modal("show");
              $("#shop_section").hide();
              $("#checkout_section").show();
              }

            }
        });
 

});
// ########################FIN EVENTOS CARRITO###############################

// ########################VER TABLA CARRITO###############################
$(".simpleCart_view").click(function() {
  $("#shop_section").show();
   $("#checkout_section").hide();
var html='<table class="table "><tr><th>Id</th><th>Nombre</th><th>Descripción</th><th>Cantidad</th><th></th><th></th><th>Precio</th><th>Subtotal</th><th></th></tr>';
for (let index = 1; index <= parseInt(dataCart.numberItem); index++) {
  if (dataItem.has('numberItem'+index)) {  // esto por si el item fue borrado
    html+="<tr>  ";
    html+= "<td>"+dataItem.get('numberItem'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('name'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('description'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('quantity'+index) +"</td>"; 
    html+= "<td>"+"<a href='javascript:;'   class='button-andres  btn_add'>+</a>" +"</td>"; 
    html+= "<td>"+"<a href='javascript:;'  class='button-andres-menos btn_subtract'>-</a>" +"</td>"; 
    html+= "<td>"+dataItem.get('price'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('subtotal'+index) +"</td>"; 
    html+= "<td>"+'<a href="javascript:;" class="button-andres btn_delete">Borrar</a> '+"</td>"; 
    html+="</tr>";
  }
  }
  html+='</table>';
$(".simpleCart_table_body").html(html);
});
// ######################## FIN VER TABLA CARRITO###############################


// ########################SUMAR UNA UNIDAD EN LA TABLA CARRITO###############################
$(document).on('click','.btn_add',function () {
     numberItem = $(this).parents("tr").find("td").eq(0).html(); 
     quantity= $(this).parents("tr").find("td").eq(3).html();
     price = $(this).parents("tr").find("td").eq(6).html();
     subtotal = $(this).parents("tr").find("td").eq(7).html();
     quantity=parseInt(quantity) + 1;
     subtotal= parseFloat(price) * parseFloat(quantity);
     
   dataItem.set('quantity'+numberItem,quantity);
   dataItem.set('subtotal'+numberItem,subtotal.toFixed(2));
   dataCart.total+= parseFloat(price);
   dataCart.totalItems+=1 ;
   $(".simpleCart_grandTotal").text(parseFloat(dataCart.total.toFixed(2)));
   $(".simpleCart_quantity").text(parseInt(dataCart.totalItems));
   
   var html='<table class="table "><tr><th>Id</th><th>Nombre</th><th>Descripción</th><th>Cantidad</th><th></th><th></th><th>Precio</th><th>Subtotal</th><th></th></tr>';
   for (let index = 1; index <= parseInt(dataCart.numberItem); index++) {
     html+="<tr>  ";
      if (dataItem.has('numberItem'+index)) {  // esto por si el item fue borrado
    html+="<tr>  ";
    html+= "<td>"+dataItem.get('numberItem'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('name'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('description'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('quantity'+index) +"</td>"; 
    html+= "<td>"+"<a href='javascript:;'   class='button-andres btn_add'>+</a>" +"</td>"; 
    html+= "<td>"+"<a href='javascript:;'  class='button-andres-menos btn_subtract'>-</a>" +"</td>"; 
    html+= "<td>"+dataItem.get('price'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('subtotal'+index) +"</td>"; 
    html+= "<td>"+'<a href="javascript:;" class="button-andres btn_delete">Borrar</a> '+"</td>"; 
    html+="</tr>";
  }
     }
     html+='</table>';
   $(".simpleCart_table_body").html(html);
   
   
  });


  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

          dataItem.append('totalItems',dataCart.totalItems);
          dataItem.append('numberItems',dataCart.numberItem);
          dataItem.append('total',dataCart.total);
          dataItem.append('rebate',dataCart.rebate);

        $.ajax({
            url: '{{ route('CartRegister') }}',
            type: 'POST',
            data:dataItem,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            success: function(data) {
              if($("#id_login").val()==0){
                $("#modal_login").modal("show");


              }else{
              $("#modal_payment").modal("hide");
              // $("#modalPayment2").modal("show");
              $("#shop_section").hide();
              $("#checkout_section").show();
              }

            }
        });
// ########################FIN SUMAR UNA UNIDAD EN LA TABLA CARRITO###############################

// ########################RESTAR UNA UNIDAD EN LA TABLA CARRITO###############################
$(document).on('click','.btn_subtract',function () {
     numberItem = $(this).parents("tr").find("td").eq(0).html(); 
     quantity= $(this).parents("tr").find("td").eq(3).html();
     price = $(this).parents("tr").find("td").eq(6).html();
     subtotal = $(this).parents("tr").find("td").eq(7).html();
     quantity=parseInt(quantity) - 1;
     subtotal= parseFloat(price) * parseFloat(quantity);
     
   dataItem.set('quantity'+numberItem,quantity);
   dataItem.set('subtotal'+numberItem,subtotal.toFixed(2));
   dataCart.total-= parseFloat(price);
   dataCart.totalItems-=1 ;
   $(".simpleCart_grandTotal").text(parseFloat(dataCart.total.toFixed(2)));
   $(".simpleCart_quantity").text(parseInt(dataCart.totalItems));
   
   var html='<table class="table "><tr><th>Id</th><th>Nombre</th><th>Descripción</th><th>Cantidad</th><th></th><th></th><th>Precio</th><th>Subtotal</th><th></th></tr>';
   for (let index = 1; index <= parseInt(dataCart.numberItem); index++) {
    if (dataItem.has('numberItem'+index)) {  // esto por si el item fue borrado
    html+="<tr>  ";
    html+= "<td>"+dataItem.get('numberItem'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('name'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('description'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('quantity'+index) +"</td>"; 
    html+= "<td>"+"<a href='javascript:;'   class='button-andres  btn_add'>+</a>" +"</td>"; 
    html+= "<td>"+"<a href='javascript:;'  class='button-andres-menos btn_subtract'>-</a>" +"</td>"; 
    html+= "<td>"+dataItem.get('price'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('subtotal'+index) +"</td>"; 
    html+= "<td>"+'<a href="javascript:;" class="button-andres btn_delete">Borrar</a> '+"</td>"; 
    html+="</tr>";
  }
     }
     html+='</table>';
   $(".simpleCart_table_body").html(html);
   
   $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

          dataItem.append('totalItems',dataCart.totalItems);
          dataItem.append('numberItems',dataCart.numberItem);
          dataItem.append('total',dataCart.total);
          dataItem.append('rebate',dataCart.rebate);

        $.ajax({
            url: '{{ route('CartRegister') }}',
            type: 'POST',
            data:dataItem,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            success: function(data) {
              if($("#id_login").val()==0){
                $("#modal_login").modal("show");


              }else{
              $("#modal_payment").modal("hide");
              // $("#modalPayment2").modal("show");
              $("#shop_section").hide();
              $("#checkout_section").show();
              }

            }
        });
   
  });
// ########################RESTAR  UNA UNIDAD EN LA TABLA CARRITO###############################


// ########################BORRAR UNA UNIDAD EN LA TABLA CARRITO###############################
$(document).on('click','.btn_delete',function () {
     numberItem = $(this).parents("tr").find("td").eq(0).html(); 
     quantity= $(this).parents("tr").find("td").eq(3).html();
     price = $(this).parents("tr").find("td").eq(6).html();
     subtotal = $(this).parents("tr").find("td").eq(7).html();

      dataItem.delete('idproduct'+numberItem);
      dataItem.delete('numberItem'+numberItem);
      dataItem.delete('name'+numberItem);
      dataItem.delete('description'+numberItem);
      dataItem.delete('quantity'+numberItem);
      dataItem.delete('price'+numberItem);
      dataItem.delete('subtotal'+numberItem);

   dataCart.total-= parseFloat(subtotal);
   dataCart.totalItems-=quantity ;
  // dataCart.numberItem -=1;
   $(".simpleCart_grandTotal").text(parseFloat(dataCart.total.toFixed(2)));
   $(".simpleCart_quantity").text(parseInt(dataCart.totalItems));
   
   var html='<table class="table "><tr><th>Id</th><th>Nombre</th><th>Descripción</th><th>Cantidad</th><th></th><th></th><th>Precio</th><th>Subtotal</th><th></th></tr>';
   for (let index = 1; index <= parseInt(dataCart.numberItem); index++) {
    if (dataItem.has('numberItem'+index)) {  // esto por si el item fue borrado
    html+="<tr>  ";
    html+= "<td>"+dataItem.get('numberItem'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('name'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('description'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('quantity'+index) +"</td>"; 
    html+= "<td>"+"<a href='javascript:;'   class='button-andres  btn_add'>+</a>" +"</td>"; 
    html+= "<td>"+"<a href='javascript:;'  class='button-andres-menos btn_subtract'>-</a>" +"</td>"; 
    html+= "<td>"+dataItem.get('price'+index) +"</td>"; 
    html+= "<td>"+dataItem.get('subtotal'+index) +"</td>"; 
    html+= "<td>"+'<a href="javascript:;" class="button-andres btn_delete">Borrar</a> '+"</td>"; 
    html+="</tr>";
  }
     }
     html+='</table>';
   $(".simpleCart_table_body").html(html);
   
   
  });

  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

          dataItem.append('totalItems',dataCart.totalItems);
          dataItem.append('numberItems',dataCart.numberItem);
          dataItem.append('total',dataCart.total);
          dataItem.append('rebate',dataCart.rebate);

        $.ajax({
            url: '{{ route('CartRegister') }}',
            type: 'POST',
            data:dataItem,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            success: function(data) {
              if($("#id_login").val()==0){
                $("#modal_login").modal("show");


              }else{
              $("#modal_payment").modal("hide");
              // $("#modalPayment2").modal("show");
              $("#shop_section").hide();
              $("#checkout_section").show();
              }

            }
        });
// ########################BORRAR UNA UNIDAD EN LA TABLA CARRITO###############################



});
  
</script>

</body>
</html>