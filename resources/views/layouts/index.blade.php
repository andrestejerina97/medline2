<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Title Tag  -->
    <title>{{ config('app.name', 'MedlineApp') }}</title>
	<!-- Favicon -->
	<link rel="icon" href="{{asset('img/ICONO.ico')}}" type="image/ico">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800;900&display=swap" rel="stylesheet"> 	
	<!-- StyleSheet -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{asset('public/css/bootstrap.css')}}">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('public/css/magnific-popup.min.css')}}">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('public/css/font-awesome.css')}}">
	<!-- Fancybox -->
	<link rel="stylesheet" href="{{asset('public/css/jquery.fancybox.min.css')}}">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="{{asset('public/css/themify-icons.css')}}">
	<!-- Jquery Ui -->
    <link rel="stylesheet" href="{{asset('public/css/jquery-ui.css')}}">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('public/css/niceselect.css')}}">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('public/css/animate.css')}}">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="{{asset('public/css/flex-slider.min.css')}}">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="{{asset('public/css/owl-carousel.css')}}">
	<!-- Slicknav -->
    <link rel="stylesheet" href="{{asset('public/css/slicknav.min.css')}}">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="{{asset('public/css/reset.css')}}">
	<link rel="stylesheet" href="{{asset('public/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/responsive.css')}}">

</head>
<body class="js">
	
    @include('layouts.preloader')
		@include('public.header.index')
		
		<!-- Breadcrumbs -->
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="bread-inner">
							<ul class="bread-list">
								<li><a href="{{route('Medicines')}}">Inicio<i class="ti-arrow-right"></i></a></li>
								<li class="active"><a href="blog-single.html">Tienda</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumbs -->
        
        <main >
            @yield('content')
        </main>
        

	
		
	@include('public.footer.footer')
	
	
    <!-- Jquery -->
  
    <script src="{{asset('js/public/jquery.min.js')}}"></script>
    <script src="{{asset('js/public/jquery-migrate-3.0.0.js')}}"></script>
	<script src="{{asset('js/public/jquery-ui.min.js')}}"></script>
	<!-- Popper JS -->
	<script src="{{asset('js/public/popper.min.js')}}"></script>
	<!-- Bootstrap JS -->
	<script src="{{asset('js/public/bootstrap.min.js')}}"></script>
	<!-- Color JS -->
	<!-- Slicknav JS -->
	<script src="{{asset('js/public/slicknav.min.js')}}"></script>
	<!-- Owl Carousel JS -->
	<script src="{{asset('js/public/owl-carousel.js')}}"></script>
	<!-- Magnific Popup JS -->
	<script src="{{asset('js/public/magnific-popup.js')}}"></script>
	<!-- Fancybox JS -->
	<script src="{{asset('js/public/facnybox.min.js')}}"></script>
	<!-- Waypoints JS -->
	<script src="{{asset('js/public/waypoints.min.js')}}"></script>
	<!-- Countdown JS -->
	<script src="{{asset('js/public/finalcountdown.min.js')}}"></script>
	<!-- Nice Select JS -->
	<script src="{{asset('js/public/nicesellect.js')}}"></script>
	<!-- Ytplayer JS -->
	<script src="{{asset('js/public/ytplayer.min.js')}}"></script>
	<!-- Flex Slider JS -->
	<script src="{{asset('js/public/flex-slider.js')}}"></script>
	<!-- ScrollUp JS -->
	<script src="{{asset('js/public/scrollup.js')}}"></script>
	<!-- Onepage Nav JS -->
	<script src="{{asset('js/public/onepage-nav.min.js')}}"></script>
	<!-- Easing JS -->
	<script src="{{asset('js/public/easing.js')}}"></script>
	<!-- Active JS -->
	<script src="{{asset('js/public/active.js')}}"></script>
	
	@yield('scripts')
</body>
</html>