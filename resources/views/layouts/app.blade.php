<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Title Tag  -->
    <title>{{ config('app.name', 'MedlineApp') }}</title>
	<!-- Favicon -->
	<link rel="icon" href="{{asset('img/ICONO.ico')}}" type="image/ico">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600;800;900&display=swap" rel="stylesheet"> 	<link rel="stylesheet" href="{{asset('public/css/bootstrap.css')}}">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('public/css/magnific-popup.min.css')}}">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('public/css/font-awesome.css')}}">
	<!-- Fancybox -->
	<!-- Themify Icons -->
    <link rel="stylesheet" href="{{asset('public/css/themify-icons.css')}}">
	<!-- Jquery Ui -->
	<!-- Nice Select CSS -->
	<link rel="stylesheet" href="{{asset('public/css/slicknav.min.css')}}">
	<link rel="stylesheet" href="{{asset('public/css/niceselect.css')}}">

	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="{{asset('public/css/reset.css')}}">
	<link rel="stylesheet" href="{{asset('public/style.css')}}">
	<link rel="stylesheet" href="{{asset('public/css/responsive.css')}}">

@yield('css')

	
</head>
<body class="js">
	
	@include('layouts.preloader')
	
	@if(Auth::check())
	 @switch(Auth::user()->role_id)
		 @case(1)
		 @include('public.header.admin')

			 @break
		 @case(2)
		 @include('public.header.customer')

			 @break
		@case(3)
		@include('public.header.medical')

		@break
		 @default
		 @include('public.header.customer')

	 @endswitch

	@else
	@include('public.header.customer')

  @endif
		
		<!-- Breadcrumbs -->
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="bread-inner">
							

							<ul class=" ml-5 bread-list">
							<a id="btn_header_logout"  href="javascript:;"   onclick="event.preventDefault();document.getElementById('logout-form').submit();"   >Cerrar sesión</a>
							<a id="btn_header_login"    href="{{route('login')}}">Iniciar sesión</a>
							</ul>
						</div>
						<div >
  
						  </div> 
						  <div >
  
						  </div>
					</div>
					
				</div>
				
			</div>
		</div>
		<!-- End Breadcrumbs -->
        
        <main >
            @yield('content')
        </main>
        

	
		
	@include('public.footer.footer')
	
	
    <!-- Jquery -->
  
    <script src="{{asset('js/public/jquery.min.js')}}"></script>
    <script src="{{asset('js/public/jquery-migrate-3.0.0.js')}}"></script>
	<!-- Popper JS -->
	<script src="{{asset('js/public/popper.min.js')}}"></script>
	<!-- Bootstrap JS -->
	<script src="{{asset('js/public/bootstrap.min.js')}}"></script>
	<!-- Color JS -->
	<!-- Slicknav JS -->
	<script src="{{asset('js/public/slicknav.min.js')}}"></script>
	<!-- Owl Carousel JS -->
	<script src="{{asset('js/public/owl-carousel.js')}}"></script>
	<!-- Magnific Popup JS -->
	<script src="{{asset('js/public/magnific-popup.js')}}"></script>
	<!-- Fancybox JS -->
	<script src="{{asset('js/public/facnybox.min.js')}}"></script>
	<!-- Waypoints JS -->
	<script src="{{asset('js/public/waypoints.min.js')}}"></script>
	<!-- Countdown JS -->
	<script src="{{asset('js/public/finalcountdown.min.js')}}"></script>
	<!-- Nice Select JS -->
	<script src="{{asset('js/public/nicesellect.js')}}"></script>
	<!-- Ytplayer JS -->
	<script src="{{asset('js/public/ytplayer.min.js')}}"></script>
	<!-- Flex Slider JS -->
	<script src="{{asset('js/public/flex-slider.js')}}"></script>
	<!-- ScrollUp JS -->
	<script src="{{asset('js/public/scrollup.js')}}"></script>
	<!-- Onepage Nav JS -->

	<!-- Active JS -->
	<script src="{{asset('js/public/active.js')}}"></script>
	<script type="text/javascript">

@if(Auth::check())
 var username= '{{Auth::user()->name}}';
  $("#btn_header_logout").css('display','block');
  $("#btn_header_login").css('display','none');


  @else
  //$("#login_error").show();
  //$("#login_error").fadeOut(10000);
  $("#message_home").show();

  $("#message_home p").text("Bienvenido!Te extrañabamos");
  $("#btn_header_logout").css('display','none');
  $("#btn_header_login").css('display','block');


  @endif
  $(function () {
    $(document).on('click','#btn_register',function () {
     location.href= '{{route("register")}} ';
    });
  })
	</script>
	@yield('scripts')
</body>
</html>