<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <a class="navbar-brand logo_h" href="{{route('index')}}"><img src="{{asset('img/logo.png')}}" alt="">MEDLINE APP - LOGIN</a>

                <div  id="message_login_form"></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('loginForm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="login_email" class="col-md-4 col-form-label text-md-right">{{ __('Dirección email') }}</label>

                            <div class="col-md-6">
                                <input id="login_email" type="email" class="form-control{{ $errors->has('login_email') ? ' is-invalid' : '' }}" name="login_email" value="{{ old('login_email') }}" required autofocus>

                                @if ($errors->has('login_email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('login_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="login_password" class="col-md-4 col-form-label text-md-right">{{ __('password') }}</label>

                            <div class="col-md-6">
                                <input id="login_password" type="password" class="form-control{{ $errors->has('login_password') ? ' is-invalid' : '' }}" name="login_password" required>

                                @if ($errors->has('login_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('login_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Recuerdame') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" id="btn_login" class="btn ">
                                    {{ __('Iniciar sesión') }}
                                </button>
                                <button type="button" id="btn_registering" class="btn">
                                    {{ __('Registrarse') }}
                                </button>
                            </div>
                            <div class="col-md-8 offset-md-4">
                                
                                @if (Route::has('login_password.request'))
                                    <a class=" btn-link" href="{{ route('login_password.request') }}">
                                        {{ __('Olvidaste tu contraseña?') }}
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-8 offset-md-6">
                                
                                @if (Route::has('login_password.request'))
                                    <a id="btn_forget" class=" btn-link" href="{{ route('login_password.request') }}">
                                        {{ __('Regístrate') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>