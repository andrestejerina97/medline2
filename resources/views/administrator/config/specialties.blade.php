<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Administrador</title>
<link rel="icon" href="{{asset('img/ICONO.ico')}}" type="image/png">

  <link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{asset('vendors/fontawesome/css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('vendors/linericon/style.css')}}">
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.theme.default.min.css')}}" >
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/flat-icon/font/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('vendors/nice-select/nice-select.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>
<body class="bg-shape">
  <div class="loader"></div>
@include('administrator.header.header')

  <!--================Hero Banner Area Start =================-->
   <!--==================Form Area -->
   <section class="bg-gray section-padding hero-banner  magic-ball ">
    <div class="section-intro text-center pb-90px">
             <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">
              <h2>Nueva Especialidad</h2>
              @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
             <form method="POST"  action="{{route('SaveSpecialties')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md">
                            <label for="">Nombre de la Especialidad:</label>
                            <input type="text" name="name" id="name" tabindex="1" class="form-control" 
                            placeholder="Ingrese nombre completo" required>
                        </div>
                      </div>
                    
                  
                    <div class="frow justify-content-center">
                        <button type="submit" class="button" 
                            data-dismiss="modal">Guardar</button>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
            </div>
        </div>
</section>
<!--End form area -->
<!--================Table specialties =================-->
<section class="bg-gray magic-ball magic-ball-testimonial pb-xl-5">
    <div class="container">
      <div class=" text-center pb-90px">
        <h2>Listado de Especialidades</h2>
        
      </div>
        <div class="testimonial__item">
          <div class="row">
            <div class="col-md-9 col-lg-12">
              <div class="testimonial__content mt-4 mt-sm-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nombre</th>
                        
                      <th></th>
                      <th></th>
                    </tr>
                    <tbody>
                      @foreach($specialties as $specialty)
                      <tr>
                      <td>{{$specialty->id}}</td>
                      <td>{{$specialty->name}}</td>
     
                      <td>
  
                          <button id_med="{{$specialty->id}}" type="button " id="btn_update" class="btn btn-success btn_update"><i class="ti-pencil-alt"></i></button>  
                      </td>
                      <td>
                        <form method="post" action="{{route('DeleteSpecialties')}}" >
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" id="id" value="{{$specialty->id}}">
                            <button  type="submit" class="btn btn-success"><i class="ti-trash"></i></button>
                          </form>
                        </td>
                      </tr>
                      @endForeach
                    </tbody>
                  </thead>
                    </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================End Table specialties =================-->
  
   
   
  <!--================Hero Banner Area End =================-->


<!--MODAL UPDATE -->
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdateLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalUpdateLabel">Ya estás a un paso!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
   
        <form method="POST" action="{{route('UpdateSpecialties')}}" accept-charset="UTF-8" enctype="multipart/form-data">
          <div class="row">
              <div class="form-group col-md">
                <label for="">Nombre de la categoría:</label>
                <input type="text" name="nameU" id="nameU" tabindex="1" class="form-control" 
                  placeholder="ingrese nombre completo" required>
              </div>
            </div>
          
          
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

           <input type="hidden" name="idU" id="idU">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
      
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
        <button type="submit" id="btn_update" class="btn btn-primary">Guardar cambios</button>
      </div>
    </form>
    </div>
  </div>
</div>
</div>


<!--MODAL UPDATE -->
  @include('administrator.footer.footer')
  




<script src="{{asset('vendors/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendors/nice-select/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/mail-script.js')}}"></script>
<script src="{{asset('js/skrollr.min.js')}}"></script>

<script type="text/javascript">

$(".btn_update").click(function() {
  $(this).parents("tr").find("td").each(function(){

              id = $(this).parents("tr").find("td").eq(0).html();
              name= $(this).parents("tr").find("td").eq(1).html();
	           
	            $('#idU').val(id);
	            $('#nameU').val(name);
	          

});
$("#modalUpdate").modal("show");
});
</script>
</body>
</html>