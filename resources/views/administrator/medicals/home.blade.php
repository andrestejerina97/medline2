<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Administrador</title>
    <link rel="icon" href="{{asset('img/ICONO.ico')}}" type="image/png">

    <link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('vendors/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/themify-icons/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/linericon/style.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/flat-icon/font/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/nice-select/nice-select.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>

<body class="bg-shape">
    <div class="loader"></div>
    @include('administrator.header.header')

    <!--================Hero Banner Area Start =================-->

    <!--==================Form Area -->
    <section class="bg-gray  hero-banner  magic-ball ">
        <div class="section-intro text-center pb-90px">
            <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">
                <h2>Alta de Médicos</h2>
               @isset($messages)
                <div class="alert alert-success">
                    <ul>
                        <li>{{ $messages }}</li>
                    </ul>
                </div>
                @endisset
                <hr>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @isset($messages)
                <div class="alert alert-info">
                    <ul>
                        <li><span>{{ $messages }}</span></li>
                    </ul>
                </div>
                @endisset
                                   
            
                <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">

                <form method="post"  action="{{route('SearchUser')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="form-row">
                        
                        <div class="form-group col-md-12 col-lg-12">
                            <label for="">Atención para registrar un médico debe estar registrado como usuario previamente:</label>
                        <div class="input-group col-12">
                            <input type="mail" name="emailSearch" id="emailSearch" tabindex="1" class="form-control" 
                            placeholder="ingrese mail de usuario" required>
                            <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">

                            <button type="submit" class="btn btn-primary ">Buscar</button>
                        </div>
                        </div>
                    </div>
                </form>
            </div>   
            </div>
        </div>
    </section>
    <!--End form area -->


  <!--================Table medicals with search=================-->
  <section class="bg-gray  magic-ball magic-ball-testimonial pb-xl-5">
    <div class="container">
        <div class=" text-center pb-90px">
            <h2>Listado de Médicos registrados</h2>
        </div>
       
        <div class="testimonial__item">
            <div class="row">
                <div class="col-md-9 col-lg-12">

                        <form method="post"  action="{{route('SearchMedicalTable')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                            <div class="form-row">
                                
                                <div class="form-group col-md-12 col-lg-12">
                                    <label for=""></label>
                                <div class="input-group col-12">
                                    <input type="mail" name="nameMedical" id="nameMedical" tabindex="1" class="form-control" 
                                    placeholder="ingrese nombre del médico a buscar" required>
                                    <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
            
                                    <button type="submit" class="btn btn-primary ">Buscar</button>
                                </div>
                                </div>
                            </div>
                        </form>
                    
                    <div class="testimonial__content mt-4 mt-sm-0">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Edad</th>
                                    <th>Celular</th>
                                    <th>Especialidad</th>
                                    <th>Clínica</th>
                                    <th>Max de pacientes</th>
                                    <th>Consultorio N°</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            <tbody>
                                @foreach($medicals as $medical)
                                <tr>
                                    <td>{{$medical->id}}</td>
                                    <td>{{$medical->name}}</td>
                                    <td>{{$medical->age}}</td>
                                    <td>{{$medical->cellphone}}</td>
                                    <td>{{$medical->nameSpecialty}}</td>
                                    <td>{{$medical->nameClinic}}</td>
                                    <td>{{$medical->patients_per_day}}</td>
                                    <td>{{$medical->consulting_room}}</td>
                                    <td>
                                        <a href="{{route('UpdateMedicalsView',['id_medical'=>$medical->id])}}" 
                                            class="btn btn-success btn_update"><i
                                                class="ti-pencil-alt"></i></a>
                                            </td>
                                    <td>
                                        <form method="post" action="{{route('DeleteMedicals')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="id" id="id" value="{{$medical->id}}">
                                            <button type="submit" class="btn btn-success"><i
                                                    class="ti-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endForeach
                            </tbody>
                            </thead>
                        </table>
                        <!--section pagination -->
                        <ul class="pagination ">
          <li ><a class="last_page btn btn-primary" href="{{$medicals->previousPageUrl() }}"><i class="fa fa-chevron-left"></i></a></li>
          <li ><a class="last_page btn " href="javascript:;">{{$medicals->currentPage()}}</a></li>
          <li><a class="next_page btn btn-primary" href="{{$medicals->nextPageUrl() }}"><i class="fa fa-chevron-right"></i></a></li>
        </ul>
                        <!--/ End Sectión pagination -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!--================End Table medicals =================-->

 

    <!--MODAL UPDATE -->
    <div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdateLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalUpdateLabel">Ya estás a un paso!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form method="POST" action="{{route('UpdateMedicals')}}" accept-charset="UTF-8"
                        enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-md">
                                <label for="">Nombre del Medicamento:</label>
                                <input type="text" name="nameU" id="nameU" tabindex="1" class="form-control"
                                    placeholder="ingrese nombre completo" required>
                            </div>

                            <div class="form-group col-md">
                                <label for="">Casa comercial:</label>
                                <input type="text" name="descriptionU" tabindex="2" id="descriptionU"
                                    class="form-control" placeholder="ingrese una descripción para mostrar al público">

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md">
                                <label for="">Precio de compra(opcional):</label>

                                <input type="text" name="purchase_priceU" tabindex="3" id="purchase_priceU"
                                    class="form-control" placeholder="Precio compra">
                            </div>
                            <div class="form-group col-md">
                                <label class="" for="">Precio de venta:</label>
                                <input type="text" tabindex="4" id="priceU" name="priceU" class="form-control"
                                    placeholder="Precio normal" required>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group col-md">
                                <label class="" for="">Precio de venta oferta:</label>
                                <input type="text" tabindex="" id="offer_priceU" name="offer_priceU"
                                    class="form-control" placeholder="Precio oferta" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md">
                                <label for="">Stock de producto(opcional):</label>
                                <input type="tel" tabindex="5" name="stockU" id="stockU" class="form-control" value="1"
                                    placeholder="Cantidad de unidades disponibles" required>
                            </div>
                            <div class="form-group col-md">
                                <label for="">Codigo de producto:</label>
                                <input type="tel" tabindex="6" name="codeU" id="codeU" class="form-control"
                                    placeholder="Codigo que identificará el producto">
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="input-group  col-md ">
                                <label class="form-label" for="">Categoría :&nbsp;</label>
                                <select class="form-control " id="medicalU" name="medicalU">
                                    @foreach ($medicals as $medical)
                                    <option value="{{$medical->id}}">{{$medical->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md">
                            <label for="">Imagen de venta(obligatorio):</label>
                            <input type="file" class="form-control" name="fileU">
                        </div>

                        <input type="hidden" name="idU" id="idU">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
                            <button type="submit" id="btn_update" class="btn btn-primary">Guardar cambios</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <script src="{{asset('vendors/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendors/nice-select/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('js/mail-script.js')}}"></script>
    <script src="{{asset('js/skrollr.min.js')}}"></script>

    <script type="text/javascript">

    </script>
</body>

</html>
