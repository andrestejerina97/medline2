<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Administrador</title>
    <link rel="icon" href="{{asset('img/ICONO.ico')}}" type="image/png">

    <link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('vendors/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/themify-icons/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/linericon/style.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/flat-icon/font/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/nice-select/nice-select.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>

<body class="bg-shape">
    <div class="loader"></div>
    @include('administrator.header.header')

    <!--================Hero Banner Area Start =================-->

    <!--==================Form Area -->
    <section class="bg-gray section-padding hero-banner  magic-ball ">
        <div class="section-intro text-center pb-90px">
            <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">
                <h2>Alta de Médicos</h2>
                <hr>
                <h4>Datos personales</h4>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
     @if ($count>0)
@foreach ($users as $user)
    

                <form method="POST" action="{{route('SaveMedicals')}}" accept-charset="UTF-8"
                    enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md">
                            <label for="">Nombre completo:</label>
                        <input type="text" name="name" id="name" value="{{$user->name}}" tabindex="1" class="form-control"
                                placeholder="ingrese nombre completo" required>
                        </div>
                        <div class="form-group col-md">
                            <label for="">Cédula:</label>
                            <input type="tel" tabindex="2" name="ci" id="ci" value="{{$user->cedula}}" class="form-control"
                            pattern="{0,20}[0-9]" placeholder="cédula de identidad" title="Cédula de identidad" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md">
                            <label for="">Teléfono propio</label>
                            <input type="tel" tabindex="3" name="phone" id="phone" value="{{$user->phone}}" class="form-control"
                                placeholder="Teléfono" pattern="{0,20}[0-9]" title="Teléfono" required>
                        </div>
                        <div class="form-group col-md">
                            <label for="">Teléfono laboral</label>
                            <input type="tel" tabindex="4" name="work_phone" id="work_phone" class="form-control"
                                placeholder="Teléfono laboral" pattern="{0,20}[0-9]" title="Teléfono laboral" >
                        </div>
                        <div class="form-group col-md">
                            <label for="">Celular</label>
                            <input type="tel" tabindex="5"  name="cellphone" pattern="{0,20}[0-9]" id="cellphone" class="form-control"
                                placeholder="Celular" title="Celular" >
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md">
                            <label for="">Fecha de nacimiento:</label>
                            <input type="date" tabindex="6" name="birthdate" id="birthdate" class="form-control"
                                placeholder="Fecha de nacimiento" title="Fecha de nacimiento" required>
                        </div>
                        <div class="form-group col-md">
                            <label for="">Edad:</label>
                            <input type="tel" tabindex="7" name="age" id="age" class="form-control" placeholder="Edad"
                                pattern="[0-9]{1,4}" title="edad solo en número" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md">
                            <label for="">Dirección:</label>
                            <input type="text" tabindex="8" name="home" id="home" value="{{$user->home}}" class="form-control"
                                placeholder="Dirección" title="Dirección" required>
                        </div>
                        <div class="form-group col-md">
                            <label for="">Correo electrónico:</label>
                        <input type="mail" tabindex="9" name="mail" value="{{$user->email}}" id="mail" class="form-control"
                                placeholder="Correo electrónico" title="Correo electrónico" required>
                        </div>
                    </div>
<hr>
                <h4>Datos Profesionales</h4>
                <div class="form-row">
                  <div class="form-group col-md">
                      <label class="form-label" for="">Clínica :&nbsp;</label>
                      <select class="form-control " id="clinics" name="clinics">
                        @foreach ($clinics as $clinic)
                        <option  value="{{$clinic->id}}">{{$clinic->name}}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="form-group col-md">
                      <label for="">Consultorio:</label>
                      <input type="tel" tabindex="10" name="consulting_room" id="consulting_room" class="form-control"
                          placeholder="Consultorio" title="Consultorio" required>
                  </div>
              </div>
              
              <div class="form-row">
                <div class="form-group col-md">
                    <label class="form-label" for="">Especialidad :&nbsp;</label>
                    <select class="form-control " id="specialties" name="specialties">
                      @foreach ($specialties as $specialty)
                      <option  value="{{$specialty->id}}">{{$specialty->name}}</option>
                      @endforeach
                    </select>
                </div>
                <div class="form-group col-md">
                    <label for="">Horario de atención:</label>
                    <input type="tel" tabindex="11" name="attention_hours" id="attention_hours" class="form-control"
                        placeholder="Horario de atención" title="Horario de atención" required>
                </div>
            </div>        
            <div class="form-row">
              <div class="form-group col-md">
                  <label for="">Pacientes por día:</label>
                  <input type="tel" tabindex="12" name="patients_per_day" id="patients_per_day" class="form-control"
                      placeholder="Pacientes por día" pattern="{0,20}[0-9]" title="Pacientes por día" required>
              </div>
              <div class="form-group col-md">
                  <label for="">Precio de consulta:</label>
                  <input type="mail" tabindex="13" pattern="{0,20}[0-9]" name="price_inquire" id="price_inquire" class="form-control"
                      placeholder="Precio por consulta" title="Precio por consulta" required>
              </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md">
              <label class="form-label" for="">Paquetes :&nbsp;</label>
              <select class="form-control " id="packeges" name="packeges">
                @foreach ($packeges as $packege)
                <option  value="{{$packege->id}}">{{$packege->name}}</option>
                @endforeach
              </select>
          </div>
            <div class="form-group col-md">
                <label for="">Foto de perfil:</label>
                <input type="file" tabindex="14" name="photo" id="photo" class="form-control"
                    placeholder="Foto de perfil" title="Foto de perfil">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md">
                <label for="">¿Destacado?:</label>
                <select class="form-control " id="destake" name="destake">
                  <option  value="0" selected>NO</option>
                  <option  value="1">SI</option>
                </select>
          </div>

        </div>

                    <div class="frow justify-content-center">
                        <button type="submit" class="button" data-dismiss="modal">Guardar</button>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id_user" value="{{$user->id}}">

                </form>
                @endforeach
                @else
                <div class="alert alert-danger">
                    <ul>
                        <li><span>El email no se encuentra, verifique que se haya escrito bien y vuelva a buscar <a href="{{route('Medicals')}}">Verificar</a></span> </li>
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </section>
    <!--End form area -->

    <!--================Hero Banner Area End =================-->


 

    <!--MODAL UPDATE -->





    <script src="{{asset('vendors/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendors/nice-select/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('js/mail-script.js')}}"></script>
    <script src="{{asset('js/skrollr.min.js')}}"></script>

    <script type="text/javascript">

    </script>
</body>

</html>
