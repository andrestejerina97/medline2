<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Medicamentos</title>
<link rel="icon" href="{{asset('img/ICONO.png')}}" type="image/png">

  <link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{asset('vendors/fontawesome/css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('vendors/linericon/style.css')}}">
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.theme.default.min.css')}}" >
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/flat-icon/font/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('vendors/nice-select/nice-select.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body class="bg-shape">
@include('administrator.header.header')

  <!--================Hero Banner Area Start =================-->

   <!--==================Form Area -->
   <section class="bg-gray hero-banner section-padding magic-ball ">
       <div class="container">
        <div class=" text-center pb-90px">
            <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">
             <h2>Items solicitados:</h2>
            </div>
            <div class="testimonial__item">
                <div class="row">
                  <div class="col-md-9 col-lg-12">
                    <div class="testimonial__content mt-4 mt-sm-0">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Nombre de artículo</th>
                            <th>Descripción</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Descuento</th>
                            <th>Subtotal</th>
                            

                            
                          </tr>
                          <tbody>
                            @foreach($medicineRequestItems as $medicineRequestItem)
                            <tr>
                            <td>{{$medicineRequestItem->name}}</td>
                            <td>{{$medicineRequestItem->description}}</td>
                            <td>{{$medicineRequestItem->quantity}}</td>
                            <td>{{$medicineRequestItem->offer_price}}</td>
                            <td>{{$medicineRequestItem->rebate}}</td>
                            <td>{{$medicineRequestItem->subtotal}}</td>
                            </tr>
                            @endForeach
                          </tbody>
                        </thead>
                          </table>
                         
                    </div>
                  <a href="{{route('MedicineSended',['numberTruck'=> $numberTruck])}}" class="btn btn-info">Marcar como enviado</a>

                </div>
                </div>
               
              
              </div>
   </div>
       </div>


       
    </section>
   <!--==================Form Area =================-->

 

<script src="{{asset('vendors/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendors/nice-select/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/mail-script.js')}}"></script>
<script src="{{asset('js/skrollr.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>