<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Medicamentos</title>
<link rel="icon" href="{{asset('img/ICONO.png')}}" type="image/png">

  <link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{asset('vendors/fontawesome/css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('vendors/linericon/style.css')}}">
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.theme.default.min.css')}}" >
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/flat-icon/font/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('vendors/nice-select/nice-select.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body class="bg-shape">
@include('administrator.header.header')

  <!--================Hero Banner Area Start =================-->

   <!--==================Form Area -->
   <section class="bg-gray hero-banner section-padding magic-ball ">
       <div class="container">
        <div class=" text-center pb-90px">
            <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">
             <h2>Solicitudes de Farmacia:</h2>
            </div>
            <div class="testimonial__item">
                <div class="row">
                  <div class="col-md-9 col-lg-12">
                    <div class="testimonial__content mt-4 mt-sm-0">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Cliente</th>
                            <th>Cédula</th>
                            <th>Dirección</th>
                            <th>Teléfono</th>
                            <th>Provincia</th>
                            <th>Código Postal</th>
                            <th>Total</th>
                            <th></th>

                            
                          </tr>
                          <tbody>
                            @foreach($medicinesRequest as $medicineRequest)
                            <tr>
                            <td>{{$medicineRequest->name_customer}}</td>
                            <td>{{$medicineRequest->cedula}}</td>

                            <td>{{$medicineRequest->home}}</td>
                            <td>{{$medicineRequest->phone}}</td>
                            <td>{{$medicineRequest->state_province}}</td>
                            <td>{{$medicineRequest->code_postal}}</td>
                            <td>{{$medicineRequest->total}}</td>

                          <td>
                            
                                <input type="hidden" name="id" id="id" value="{{$medicineRequest->id}}">
                                <a  href="{{route('truckList',['numberTruck'=>$medicineRequest->id])}}" id="btn_truck" class="btn btn-success btn_update"><i class="ti-truck"></i></button>  
                              
                            </td>
                            </tr>
                            @endForeach
                          </tbody>
                        </thead>
                          </table>
                    </div>
                  </div>
                </div>
              </div>
   </div>
       </div>


       
    </section>
   <!--==================Form Area =================-->



  <script src="{{asset('vendors/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendors/nice-select/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/mail-script.js')}}"></script>
<script src="{{asset('js/skrollr.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>