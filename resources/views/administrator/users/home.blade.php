<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Portal de administración</title>
<link rel="icon" href="{{asset('img/ICONO.png')}}" type="image/png">

  <link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{asset('vendors/fontawesome/css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('vendors/linericon/style.css')}}">
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.theme.default.min.css')}}" >
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/flat-icon/font/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('vendors/nice-select/nice-select.css')}}">

<link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body class="bg-shape">
@include('administrator.header.header')

  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">
      <div class="col-lg-12 col-md-8 mb-4 mb-md-0 mx-auto">
        <h2>Usuarios del sistema</h2>
      </div>
    
      <div class="testimonial__content mt-4 mt-sm-0">
        <form method="post"  action="{{route('SearchUserTable')}}" accept-charset="UTF-8" enctype="multipart/form-data">
          <div class="form-row">
              
              <div class="form-group col-md-12 col-lg-12">
                  <label for=""></label>
              <div class="input-group col-12">
                  <input type="text" name="searchUser" id="searchUser" tabindex="1" class="form-control" 
                  placeholder="ingrese nombre del usuario a buscar" required>
                  <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">

                  <button type="submit" class="btn btn-primary ">Buscar</button>
              </div>
              </div>
          </div>
      </form>
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Fecha creación</th>
                    <th>Telefono</th>
                    <th>Rol</th>
  

                </tr>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{{$user->nameRole}}</td>

                </tr>
                @endForeach
            </tbody>
            </thead>
        </table>
        <!--section pagination -->
        <ul class="pagination ">
<li ><a class="last_page btn btn-primary" href="{{$users->previousPageUrl() }}"><i class="fa fa-chevron-left"></i></a></li>
<li ><a class="last_page btn " href="javascript:;">{{$users->currentPage()}}</a></li>
<li><a class="next_page btn btn-primary" href="{{$users->nextPageUrl() }}"><i class="fa fa-chevron-right"></i></a></li>
</ul>
</div>
<div class="card mt-3">
  <div class="card-body">
  <span>TOTAL DE USUARIOS EN EL SISTEMA: @isset($total_users){{$total_users}}@endisset</span>
  <hr>
  <span>TOTAL DE CLIENTES REGISTRADOS: @isset($total_customer){{$total_customer}}@endisset</span>
  <hr>
  <span>TOTAL DE MÉDICOS REGISTRADOS: @isset($total_medical){{$total_medical}}@endisset</span>

  </div>
</div>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->




  




<script src="{{asset('vendors/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendors/nice-select/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/mail-script.js')}}"></script>
<script src="{{asset('js/skrollr.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>