    <div class="row">
        <div class="col-lg-8  col-md-8 col-8 mx-auto">
            <div class="form-group">
                <label for="">Fecha de cita: <span>*</span></label>
              <input type="date" name="date_quote" id="date_quote" class="form-control" 
               required>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 mx-auto  col-8">
            <div class="form-group">
                <label for="">Hora de la cita: <span>*</span></label>
              <input type="time" name="time_quote" id="time_quote" class="form-control" 
              placeholder="hh:mm" required>
            </div>
        </div>
        <div class="col-lg-8  col-md-8 col-8 mx-auto">
          <div class="form-group">
              <label for="">Motivo de consulta:</label>
            <input type="text" name="reason" id="reason" class="form-control" 
             required>
          </div>
        </div>
          <div class="col-lg-8  col-md-8 col-8 mx-auto">
            <div class="form-group">
                <label for="">Enfermedad actual: </label>
              <input type="text" name="current_illness" id="current_illness" class="form-control" 
               >
            </div>
          </div>
      

  </div>

<!--/ End Form -->

