<div class="card mx-auto col-lg-8 col-md-8 col-xs-10 col-10">
    
     <div class="card-body ">
         <div class="col-lg-12 col-md-12 col-12">
           <div class="form-group">
               <label for="">Evaluación de la consulta:<span>*</span></label>
             <textarea type="text" value="" name="evaluation" id="evaluation" class="form-control">
                </textarea>
           </div>
       </div>
       <div class="col-lg-12 col-md-12 col-12">
         <div class="form-group">
             <label for="">Adjuntos: <span></span></label>
           <input type="file" name="files[]" id="file" class="form-control" multiple>
         </div>
       </div>
       <div class="col-lg-12 col-md-12 col-12">
           <div class="form-group">
               <label for="">Observaciones: <span></span></label>
             <textarea type="text" autocomplete=off autofocus
             name="observation" id="observation" class="form-control">
                </textarea>
           </div>
       </div>
      
     </div>
   </div>
