<div class="row">
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Nombre completo: <span>*</span></label>
          <input type="text" name="name" id="name" class="form-control" 
          placeholder="Nombre completo" 
          @isset($user->name)
             value="{{$user->name}}" 
          @endisset           
          >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Dirección: <span></span></label>
          <input type="text" name="home" id="home" class="form-control" 
          placeholder="Dirección completa" 
          @isset($user->home)
          value="{{$user->home}}" 
         @endisset 
         required>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Edad:</label>
            <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" autocomplete="off" name="age" id="age" class="form-control"
                placeholder="Edad" 
                @isset($user->age)
                value="{{$user->age}}" 
               @endisset >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Fecha de nacimiento:</label>
            <input type="date" onkeypress="return event.charCode >= 48 && event.charCode <= 57" autocomplete="off" name="birthdate" id="birthdate" class="form-control"
                placeholder="Fecha de nacimiento" 
                @isset($user->birthdate)
                value="{{$user->birthdate}}" 
               @endisset >
        </div>
    </div>
        <div class="col-lg-6 col-md-6 col-12">
            <div class="form-group">
            <label for="">Teléfono propio</label>
            <input type="tel" tabindex="3" name="phone" id="phone" class="form-control"
                placeholder="Teléfono" 
                @isset($user->phone)
                value="{{$user->phone}}" 
               @endisset  pattern="{0,20}[0-9]" title="Teléfono" disabled>
            </div>

        </div>

        <div class="col-lg-6 col-md-6 col-12">
            <div class="form-group">

            <label for="">Celular:</label>
            <input type="tel" tabindex="5"  name="cellphone" pattern="{0,20}[0-9]" id="cellphone" class="form-control"
                placeholder="Celular"  @isset($user->cellphone)
                value="{{$user->cellphone}}" 
               @endisset  title="Celular" >
            </div>

            </div>
            <div class="col-lg-6 col-md-6 col-12">
                <div class="form-group">
    
                <label for="">Tipo de sangre:</label>
                <input type="tel" tabindex="5"  name="type_blood" id="type_blood" class="form-control"
                    placeholder="Celular"  @isset($user->type_blood)
                    value="{{$user->type_blood}}" 
                   @endisset >
                </div>
    
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-group">
        
                    <label for="">Ocupación:</label>
                    <input type="tel" tabindex="5"  name="occupation" id="occupation" class="form-control"
                         @isset($user->occupation)
                        value="{{$user->occupation}}" 
                       @endisset >
                    </div>
        
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="form-group">
            
                        <label for="">Sexo:</label>
                        <input type="tel" tabindex="5"  name="sex" id="sex" class="form-control"
                             @isset($user->sex)
                            value="{{$user->sex}}" 
                           @endisset >
                        </div>
            
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                
                            <label for="">País:</label>
                            <input type="tel" tabindex="5"  name="country" id="country" class="form-control"
                                  @isset($user->country)
                                value="{{$user->country}}" 
                               @endisset >
                            </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                    
                                <label for="">Provincia/Cantón/parroquia:</label>
                                <input type="tel" tabindex="5"  name="state_province" id="state_province" class="form-control"
                                    placeholder="Estado civil"  @isset($user->state_province)
                                    value="{{$user->state_province}}" 
                                   @endisset >
                                </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="form-group">
                        
                                    <label for="">Estado civil:</label>
                                    <input type="tel" tabindex="5"  name="civil_status" id="civil_status" class="form-control"
                                        placeholder="Estado civil"  @isset($user->civil_status)
                                        value="{{$user->civil_status}}" 
                                       @endisset >
                                    </div>
                                    </div>
        <div class="col-lg-6 col-md-6 col-12">
            <div class="form-group">
            <label for="">Cambiar Foto de perfil:</label>
            <input type="file"  tabindex="6" name="photo" id="photo" class="form-control"
                placeholder="Foto de perfil" title="Foto de perfil">
            </div>
            <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="patient_id" value="{{$user->id}}">
            <button type="submit" id="btn_update_patient" class="btn button-andres">Actualizar datos</button>

        </div>

    </div>