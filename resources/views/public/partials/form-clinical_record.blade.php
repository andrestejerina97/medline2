<div class="row">
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Talla: <span>*</span></label>
          <input type="text" name="height" id="height" class="form-control" 
          placeholder="" 
          @isset($clinical_record->height)
             value="{{$clinical_record->height}}" 
          @endisset           
          >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Peso: <span></span></label>
          <input type="text" name="weight" id="weight" class="form-control" 
          placeholder="" 
          @isset($clinical_record->weight)
          value="{{$clinical_record->weight}}" 
         @endisset 
         >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">IMC: <span>*</span></label>
          <input type="text" name="imc" id="imc" class="form-control" 
          placeholder="" 
          @isset($clinical_record->imc)
             value="{{$clinical_record->imc}}"
             
          @endisset           
          >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Temperatura: <span></span></label>
          <input type="text" name="temperature" id="temperature" class="form-control" 
          placeholder="" 
          @isset($clinical_record->temperature)
          value="{{$clinical_record->temperature}}"
          
         @endisset 
         >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Presión Sistólica: <span></span></label>
          <input type="text" name="systolic_pressure" id="systolic_pressure" class="form-control" 
          placeholder="" 
          @isset($clinical_record->systolic_pressure)
          value="{{$clinical_record->systolic_pressure}}"
          
         @endisset 
         >
        </div>
    </div> 
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Presión Diastólica: <span></span></label>
          <input type="text" name="diastolic_pressure" id="diastolic_pressure" class="form-control" 
          placeholder="" 
          @isset($clinical_record->diastolic_pressure)
          value="{{$clinical_record->diastolic_pressure}}"
          
         @endisset 
         >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Ritmo cardiaco: <span></span></label>
          <input type="text" name="heart_rate" id="heart_rate" class="form-control" 
          placeholder="" 
          @isset($clinical_record->heart_rate)
          value="{{$clinical_record->heart_rate}}"
          
         @endisset 
         >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Saturación de oxígeno: <span></span></label>
          <input type="text" name="oxygen_saturation" id="oxygen_saturation" class="form-control" 
          placeholder="" 
          @isset($clinical_record->oxygen_saturation)
          value="{{$clinical_record->oxygen_saturation}}"
          
         @endisset 
         >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Anamnesis: <span></span></label>
            <textarea name="anamnesis" id="anamnesis" class="form-control" rows="3">@isset($clinical_record->anamnesis){{$clinical_record->anamnesis}} @endisset </textarea>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Diagnóstico: <span></span></label>
            <textarea name="diagnosis" id="diagnosis" class="form-control" rows="3">@isset($clinical_record->diagnosis){{$clinical_record->diagnosis}} @endisset </textarea>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Observaciones: <span></span></label>
            <textarea name="observation" id="observation" class="form-control" rows="3">@isset($clinical_record->observation){{$clinical_record->observation}} @endisset </textarea>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Exploración: <span></span></label>
            <textarea 
            name="exploration" 
            id="exploration"
            class="form-control" 
             rows="3"
             >@isset($clinical_record->exploration){{$clinical_record->exploration}} @endisset </textarea>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Evolución: <span></span></label>
            <textarea 
            name="evolution" 
            id="evolution"
            class="form-control" 
             rows="3"
             >@isset($clinical_record->evolution){{$clinical_record->evolution}} @endisset </textarea>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Tratamiento: <span></span></label>
            <textarea 
            name="treatment" 
            id="treatment"
            class="form-control" 
             rows="3"
             >@isset($clinical_record->treatment){{$clinical_record->treatment}} @endisset </textarea>
        </div>
    </div>
</div>