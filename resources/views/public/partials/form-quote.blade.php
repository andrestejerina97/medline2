<div class="row">
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Motivo de consulta: <span>*</span></label>
          <input type="text" name="reason" id="reason" class="form-control" 
          placeholder="" 
          @isset($quote->reason)
             value="{{$quote->reason}}" 
          @endisset           
          >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Enfermedad actual: <span></span></label>
          <input type="text" name="current_illness" id="current_illness" class="form-control" 
          placeholder="" 
          @isset($quote->current_illness)
          value="{{$quote->current_illness}}" 
         @endisset 
         required>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Fecha de cita: <span>*</span></label>
          <input type="text" name="date_quote" id="date_quote" class="form-control" 
          placeholder="" 
          @isset($quote->date_quote)
             value="{{$quote->date_quote}}"
             disabled
          @endisset           
          >
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <div class="form-group">
            <label for="">Hora de cita: <span></span></label>
          <input type="text" name="time_quote" id="time_quote" class="form-control" 
          placeholder="" 
          @isset($quote->time_quote)
          value="{{$quote->time_quote}}"
          disabled
         @endisset 
         >
        </div>
    </div>
</div>