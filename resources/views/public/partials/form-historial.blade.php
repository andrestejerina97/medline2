<div  class="table-responsive" >
    <table id="card_clinical_history" class="table table-hover">
      <tr>
        <th>última atención</th>
        <th>Razón de consulta</th>
        <th>Diagnóstico</th>
        <th>Observaciones</th>
        <th></th>

    </tr>
    @foreach ($clinic_histories as $clinic_history)
    <tr>
      <td>{{$clinic_history->date_quote}}</td>
      <td>{{$clinic_history->reason}}</td>
      <td>{{$clinic_history->evaluation}}</td>
      <td>{{$clinic_history->observation}}</td>
    <td> <a onclick="filter_history(this);" href="javascript:;" data-historyC="{{$clinic_history->id}}" data-report="{{$clinic_history->id_medical_report}}" data-quote={{$clinic_history->id_medical_quote}} class="button-andres bg-success  text-light ">Ampliar</a>
      </td>

    </tr>
    @endforeach
    </table>
    </div>