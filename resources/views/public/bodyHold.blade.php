

@extends('layouts.public')

@section('content')


	<!-- Product Style -->
  <section class="product-area shop-sidebar shop section">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-12">
          <div class="shop-sidebar">
              <!-- Single Widget -->
              <div class="single-widget category">
                <h3 class="title">Categories</h3>
                <ul class="categor-list">
                  @foreach ($categories as $category)  
                  <li>
                    <a href="#">{{$category->name}}</a>
                </li>
                  @endforeach 
                  
                </ul>
              </div>
              <!--/ End Single Widget -->
              <!-- Shop By Price -->
                <div class="single-widget range">
                  <h3 class="title">Rango de precios</h3>
                  <div class="price-filter">
                    <div class="price-filter-inner">
                      <div id="slider-range"></div>
                        <div class="price_slider_amount">
                        <div class="label-input">
                          <span>Rango:</span><input type="text" id="amount" name="price" placeholder="Añade tu precio"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="check-box-list">
                    <li>
                      <label class="checkbox-inline" for="1"><input name="news" id="1" type="checkbox">$20 - $50<span class="count">(3)</span></label>
                    </li>
                    <li>
                      <label class="checkbox-inline" for="2"><input name="news" id="2" type="checkbox">$50 - $100<span class="count">(5)</span></label>
                    </li>
                    <li>
                      <label class="checkbox-inline" for="3"><input name="news" id="3" type="checkbox">$100 - $250<span class="count">(8)</span></label>
                    </li>
                  </ul>
                </div>
                <!--/ End Shop By Price -->
          </div>
        </div>
        <div class="col-lg-9 col-md-8 col-12">
          <div class="row">
            <div class="col-12">
              <!-- Shop Top -->
              <div class="shop-top">
                <div class="shop-shorter">
                  <div class="single-shorter">
                    <label>Mostrar :</label>
                    <select>
                      <option selected="selected">09</option>
                      <option>15</option>
                      <option>25</option>
                      <option>30</option>
                    </select>
                  </div>
                  <div class="single-shorter">
                    <label>Ordenado por :</label>
                    <select>
                      <option selected="selected">Nombre</option>
                      <option>Precio</option>
                      
                    </select>
                  </div>
                </div>
                <ul class="view-mode">
                  <li class="active"><a href="shop-grid.html"><i class="fa fa-th-large"></i></a></li>
                  <li><a href="shop-list.html"><i class="fa fa-th-list"></i></a></li>
                </ul>
              </div>
              <!--/ End Shop Top -->
            </div>
          </div>



         


<div class="row">
    @foreach ($medicines as $medicine)
    
    <div class="col-lg-4 col-md-6 col-12">
      <div class="single-product">
        <div class="product-img">
          <a href="product-details.html">
            <img class="default-img" src="{{ asset('storage/medicines/'.$medicine->resource)}}" alt="#">
            <img class="hover-img" src="https://via.placeholder.com/550x750" alt="#">
          </a>
          <h4  style="display:none" class="item_name" >{{$medicine->id}}</h4>
          <input style="display:none" value="{{$medicine->name}}"  class="item_nombre">
          <div class="button-head">
            <div class="product-action">
              <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#"><i class=" ti-eye"></i><span>Ver detalles</span></a>
            </div>
            <div class="product-action-2">
              <a title="Añadir al carrito" class="item_add payment" href="javascript:;">Añadir al carrito</a>
            </div>
          </div>
        </div>
        <div class="product-content">
          <h3><a href="product-details.html">{{$medicine->name}}</a></h3>
          <h3><a href="product-details.html">{{$medicine->description}}</a></h3>
          <div class="product-price">
            <span>${{$medicine->price}} </span>
            <span>${{$medicine->offer_price}} </span>
          </div>
        </div>
      </div>
    </div>

@endforeach
     
</div>


        </div>
      </div>
    </div>
  </section>
  <!--/ End Product Style 1  -->	

  
  <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
          </div>
          <div class="modal-body">
            <div class="row no-gutters">
              <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <!-- Product Slider -->
                  <div class="product-gallery">
                    <div class="quickview-slider-active">
                      <div class="single-slider">
                        <img src="https://via.placeholder.com/569x528" alt="#">
                      </div>
                      <div class="single-slider">
                        <img src="https://via.placeholder.com/569x528" alt="#">
                      </div>
                      <div class="single-slider">
                        <img src="https://via.placeholder.com/569x528" alt="#">
                      </div>
                      <div class="single-slider">
                        <img src="https://via.placeholder.com/569x528" alt="#">
                      </div>
                    </div>
                  </div>
                <!-- End Product slider -->
              </div>
              <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="quickview-content">
                  <h2>Flared Shift Dress</h2>
                  <div class="quickview-ratting-review">
                    <div class="quickview-ratting-wrap">
                      <div class="quickview-ratting">
                        <i class="yellow fa fa-star"></i>
                        <i class="yellow fa fa-star"></i>
                        <i class="yellow fa fa-star"></i>
                        <i class="yellow fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <a href="#"> (1 customer review)</a>
                    </div>
                    <div class="quickview-stock">
                      <span><i class="fa fa-check-circle-o"></i> in stock</span>
                    </div>
                  </div>
                  <h3>$29.00</h3>
                  <div class="quickview-peragraph">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo ipsum numquam.</p>
                  </div>
                  <div class="size">
                    <div class="row">
                      <div class="col-lg-6 col-12">
                        <h5 class="title">Size</h5>
                        <select>
                          <option selected="selected">s</option>
                          <option>m</option>
                          <option>l</option>
                          <option>xl</option>
                        </select>
                      </div>
                      <div class="col-lg-6 col-12">
                        <h5 class="title">Color</h5>
                        <select>
                          <option selected="selected">orange</option>
                          <option>purple</option>
                          <option>black</option>
                          <option>pink</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="quantity">
                    <!-- Input Order -->
                    <div class="input-group">
                      <div class="button minus">
                        <button type="button" class="btn btn-primary btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                          <i class="ti-minus"></i>
                        </button>
                      </div>
                      <input type="text" name="quant[1]" class="input-number"  data-min="1" data-max="1000" value="1">
                      <div class="button plus">
                        <button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="quant[1]">
                          <i class="ti-plus"></i>
                        </button>
                      </div>
                    </div>
                    <!--/ End Input Order -->
                  </div>
                  <div class="add-to-cart">
                    <a href="#" class="btn">Add to cart</a>
                    <a href="#" class="btn min"><i class="ti-heart"></i></a>
                    <a href="#" class="btn min"><i class="fa fa-compress"></i></a>
                  </div>
                  <div class="default-social">
                    <h4 class="share-now">Share:</h4>
                    <ul>
                      <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a class="youtube" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                      <li><a class="dribbble" href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal end -->
 
  <!--================Blog section Start =================-->
  <section class="bg-gray magic-ball-about">
    <div class="section-intro">
      <h2>MEDLINE PRODUCTOS</h2>
    </div>
    <div class="container">
      <div class="section-intro text-center ">
       <!-- <img class="section-intro-img" src="img/home/section-icon.png" alt=""> -->
        <form class="search-form" action="#">
          <div class="form-group">
            <div class="input-group">
              <input id="searchMedicines" name="searchMedicines" type="text" class="form-control" placeholder="Buscar mi medicamento">
              <div class="input-group-append">
                <span class="input-group-text"><i class="ti-search"></i></span>
              </div>
            </div>
          </div>
        </form>
              <!--aside categories -->
              <aside class="single_sidebar_widget tag_cloud_widget">
                  <h4 class="widget_title">Categorías</h4>
                  <ul class="list cat-list">
                    @foreach ($categories as $category)  
                    <li>
                      <a href="#">{{$category->name}}</a>
                  </li>
                    @endforeach 
                    
                  </ul>
                </aside>
              
                <a class="button " id="paymentFinalice" href="#"> <i class="ti-shopping-cart"></i> Finalizar (<span id="simpleCart_quantity" class="simpleCart_quantity"></span>) </a>
                <a href="javascript:;" class="simpleCart_empty ">Vaciar carrito </a>
        <!-- End aside categories -->
        <!--aside payment -->
      </div>
    </div>
          <!-- end aside payment -->
    </section>
    <!-- -------------------------------------------------------- -->       
           
<section class="">
  <div class="section-med ">

      @php
      $i = 0
      @endphp
@foreach ($medicines as $medicine)

@if ( ($i== 3) or $loop->first ) 

<div class="row">
  <div class="col-md-6 col-lg-4 mb-4 mb-lg-0 ">
    <div class="card-blog">
      <img class="card-img rounded-0" src="{{ asset('storage/medicines/'.$medicine->resource) }}" alt="">
      <div class="card-blog-body simpleCart_shelfItem">
        <div class="row">
          <h3 class="item_price">${{$medicine->price}} </h3>
          <h3 class="item_price crossedtext">${{$medicine->price}} </h3>
        </div>
        <a href="#">
          <h4  style="display:none" class="item_name" >{{$medicine->id}}</h4>
          <h4  >{{$medicine->name}}</h4>
          <h4 class="item_price ">{{$medicine->description}} </h4>
        </a>
        <input style="display:none" value="{{$medicine->name}}"  class="item_nombre">
        <input type="text" class="item_quantity form-control" value="1">
    
        <ul class="card-blog-info">
          <a class="item_add payment" href="javascript:;"><i class="ti-shopping-cart">Añadir</i></a>
        </ul>
      </div>
    </div>
    </div>

@else
<div class="col-md-6 col-lg-4 mb-4 mb-lg-0 ">
<div class="card-blog">
  <img class="card-img rounded-0" src="{{ asset('storage/medicines/'.$medicine->resource) }}" alt="">
  <div class="card-blog-body simpleCart_shelfItem">
    <div class="row">
      <h3 class="item_price">${{$medicine->price}} </h3>
      <h3 class="item_price crossedtext">${{$medicine->price}} </h3>
      

    </div>
    <a href="#">
      <h4  style="display:none" class="item_name" >{{$medicine->id}}</h4>
      <h4  >{{$medicine->name}}</h4>
      <h4 class="item_price ">{{$medicine->description}} </h4>
    </a>
    <input style="display:none" value="{{$medicine->name}}"  class="item_nombre">
    <input type="text" class="item_quantity form-control" value="1">

    <ul class="card-blog-info">
      <a class="item_add payment" href="javascript:;"><i class="ti-shopping-cart">Añadir</i></a>
    </ul>
  </div>
</div>
</div>

@if ( ($i==3) or ($loop->remaining==0))

</div>
@php
$i = 0
@endphp
@endif
    
@endif
@php
$i ++
@endphp
@endforeach
     

</div>
</section>
  <!--================Blog section End =================-->

  <input type="hidden" class=" item_token" id="_token" name="_token" value="{{ csrf_token() }}">
<!-- -->
<!-- Modal -->
<div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="modalPaymentLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalPaymentLabel">Listado de productos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
          <h4 class="widget_title">Detalles de la compra:</h4>
          <div class="simpleCart_items" >
          </div>
     
          -----------------------------<br/>
          Final Total: <span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span> <br />
        
          
         
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
        <button type="button"  id="btn_confirmar1" class="btn btn-primary ">Confirmar Compra</button>
      </div>
    </div>
  </div>
</div>
</div>
<!-- -->
<!-- Modal -->
<div class="modal fade" id="modalPayment2" tabindex="-1" role="dialog" aria-labelledby="modalPayment2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalPayment2Label">Ya estás a un paso!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <section class="bg-gray section-padding magic-ball ">
          <div class="section-intro text-center pb-20px">
                   <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">
                    <h2>Dinos como llegar a tí</h2>
                   <form method="POST" action="{{route('SaveMedicines')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                          <div class="row">
                              <div class="form-group col-md">
                                  <label for="">Escribe tu dirección:</label>
                                  <input type="text" name="home" id="home" class="form-control" 
                                  placeholder="Dirección completa" required>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md">
                                <label for="">Descripción </label>
                                  <input type="text" name="description" id="description" class="form-control"
                                      placeholder="Danos alguna referencia útil">
                                 
                              </div>
                            </div>
                          <div class="row">
                              <div class="form-group col-md">
                                  <label for="">Código postal:</label>
                                  <input type="text" name="code_postal" id="code_postal" class="form-control"
                                      placeholder="CP" required>
                              </div>
                            </div>

                      </form>
                  </div>
              </div>
      </section>
          
         
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
        <button type="button" id="btn_confirmar2" class="btn btn-primary simpleCart_checkout">Confirmar Compra</button>
      </div>
    </div>
  </div>
</div>
</div>
<!-- -->



@endsection




  <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
  <script src="vendors/nice-select/jquery.nice-select.min.js"></script>
  <script src="js/jquery.ajaxchimp.min.js"></script>
  <script src="js/mail-script.js"></script>
  <script src="js/skrollr.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/simpleCart.min.js"></script>
  <script type="text/javascript">

  $("#paymentFinalice").click(function() {
    $("#modalPayment").modal("show");
  })
  $("#btn_confirmar1").click(function() {
    $("#modalPayment1").modal("toggle");
    $("#modalPayment2").modal("show");
  })
  simpleCart.empty();
  simpleCart.bind("beforeCheckout", function(data) {
 data._token=$("#_token").val();
 data.home=$("#home").val();
 data.code_postal=$("#code_postal").val();
 data.description=$("#description").val();
  });
    simpleCart({
      cartColumns: [
        {attr: "nombre",label:'nombre'},
        
        {attr: "price",label:'precio',view: 'currency'},
        {attr:'quantity',label:'cant'},
	    	{ view: "decrement" , label: false , text: "-" } ,
        { view: "increment" , label: false , text: "+" } ,

 
      ],
      cartStyle: "table", 
      checkout: {
        type: "SendForm",
        url: 'SavePurchase',
        extra_data: {
        
        }
      }
    });

  </script>
</body>
</html>