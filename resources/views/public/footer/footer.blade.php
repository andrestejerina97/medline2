  <!-- Start Footer Area -->
  
  <footer class="footer mt-2" style="background-color: #144484;">

    <div class="copyright">
      <div class="container">
        <div class="inner">
          <div class="row">
            <div class="col-lg-12 col-12 ">
              <div class="text-center">
                <div class="links-footer mb-3">
                  <a class="pr-1" href=""><i class="fa fa-facebook "></i></a> 
                <a class="pr-1" href=""><i class="fa fa-instagram "></i></a> 
                 <a class="pr-1" href=""><i class="fa fa-twitter "></i> </a>
                 <a class="pr-1" href=""><i class="fa fa-phone "></i> </a>
                 <a class="pr-3" href=""><i class="fa fa-map-marker "></i> </a>
                </div>
                <p>Copyright © 2020 <a  target="_blank">MedlineApp S.A</a>  -  Todos los derechos reservados.</p>

            </div>
              </div>
            </div>
            <div class="col-lg-6 col-12">
              <div class="right">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- /End Footer Area -->
