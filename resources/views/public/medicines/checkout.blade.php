<!-- Start Checkout -->
<section id="checkout_section" class="shop checkout section" style="display:none">
        <div class="row"> 
            <div class="col-lg-8 col-12 ml-3">


                    <!-- Form -->
                    <form class="form" method="post" id="form_checkout"  accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label for="">Nombre completo: <span>*</span></label>
                                  <input type="text" name="name_customer" id="name_customer" class="form-control" 
                                  placeholder="Nombre completo" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label for="">Escribe tu dirección: <span>*</span></label>
                                  <input type="text" name="home" id="home" class="form-control" 
                                  placeholder="Dirección completa" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label for="">Código postal:</label>
                                    <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" autocomplete="off" name="code_postal" id="code_postal" class="form-control"
                                        placeholder="CP" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label>Número de telefono<span>*</span></label>
                                    <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" autocomplete="off" id="phone" name="phone" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label>País<span>*</span></label>
                                    <select name="country_name" id="country_name">
                                        <option value="Ecuador">Ecuador</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label>Ciudad<span>*</span></label>
                                    <select name="state_province" id="state_province">
                                        <option value="Cuenca" >Cuenca</option>
                                        <option value="Guayaquil">Guayaquil</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">

                                    <label for="">Descripción </label>
                                    <input type="text" name="description" id="description" class="form-control"
                                        placeholder="Danos alguna referencia útil ">
                                </div>
                            </div>

                          <!--  <div class="col-12">
                                <div class="form-group create-account">
                                    <input id="cbox" type="checkbox">
                                    <label>Crear una cuenta?</label>
                                </div>
                            </div> -->
                        </div>
                    </form>
                    <!--/ End Form -->
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="order-details">
                    <!-- Order Widget -->
                    <div class="single-widget">
                        <h2>COMPRA TOTAL</h2>
                        <div class="content">
                            <ul>
                                <li>Sub Total<span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span></li>
                                <li>(+) Delivery<span>$0.00</span></li>
                                <li class="last">Total<span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span></li>
                            </ul>
                        </div>
                    </div>
                    <!--/ End Order Widget -->
                    <!-- Order Widget -->
                    <div class="single-widget">
                        <h2>Pago</h2>
                        <div class="content">
                            <div class="checkbox">
                                <label class="checkbox-inline" for="3"><input name="news" id="3" type="checkbox"> Efectivo</label>
                       
                            </div>
                        </div>
                    </div>
                    <!--/ End Order Widget -->
                  
                    <!-- Button Widget -->
                    <div class="single-widget get-button">
                        <div  id="message_checkout"></div>

                        <div class="content">
                            <div class="button">
                                <button  id="btn_finally_payment"  class="btn ">Finalizar la compra</button>
                            </div>
                            
                            
                        </div>
                    </div>
                    <!--/ End Button Widget -->
                                      
                    <!-- Button Widget -->
                    <div class="single-widget get-button">
                        <div class="content">
                            <div class="button">
                                <a href="javascript:;" class="btn btn-info back_shop">Volver a tienda</a>
                            </div>
                            
                        </div>
                    </div>
                    <!--/ End Button Widget -->
                </div>
            </div>
        </div>
</section>
<!--/ End Checkout -->

