  <!-- Header -->
  <header class="header shop">
    <div class="middle-inner">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-12">
          <!-- Logo -->
         
            <a class="navbar-brand logo_h" href="{{route('index')}}"><img src="{{asset('img/logo.png')}}" alt="">MEDLINE APP</a>					

         
                    
          <!--/ End Logo -->
          <!-- Search Form -->
          <div class="search-top">
            <div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
            <!-- Search Form -->
            <div class="search-top">
              <form class="search-form" method="POST" action="{{route('SearchMedicines')}}">
                <input type="text" placeholder="Buscar Medicamento..." name="searchInput">
                <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
                <button value="search" type="submit"><i class="ti-search"></i></button>

              </form>
            </div>
            <!--/ End Search Form -->
          </div>
          <!--/ End Search Form -->
          <div class="mobile-nav"></div>
        </div>
      <!--  <div class="col-lg-8 col-md-7 col-12">
          <div class="search-bar-top">
            <div class="search-bar">
              <select>
                <option selected="selected">Categorías</option>
               
              </select>
              <form>
                <input name="search" placeholder="Buscar aquí..." type="search">
                <button class="btnn"><i class="ti-search"></i></button>
              </form>
            </div>
          </div>
        </div> -->
        <div class="col-lg-2 col-md-3 col-12">
          <div class="right-bar">
            <!-- Search Form -->
            <div class="sinlge-bar">
              <a href="#" class="single-icon"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
            </div>
 <!-- User item -->
          <div class="sinlge-bar shopping">   
              <a href="#" class="single-icon"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>  
              <div class="shopping-item">
                <div class="dropdown-cart-header">
                      @if(Auth::check())
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">

                        </form>
                        <a onclick="event.preventDefault();document.getElementById('logout-form').submit();"   >Cerrar sesión</a></li>

                      @else
                      <a href="{{route('login')}}">Iniciar sesión</a></li>

                      @endif
                  
                </div>
            </div>
          </div>
         <!--End User item -->
           <!-- Search Form -->
            <div class="sinlge-bar shopping">
              <a href="#" class="single-icon"><i class="ti-bag"></i> <span class="total-count"><span id="simpleCart_quantity" class="simpleCart_quantity"></span></span></a>
            
              <!-- Shopping Item -->
              <div class="shopping-item">
                <div class="dropdown-cart-header">
                  <span><span id="simpleCart_quantity" class="simpleCart_quantity"></span> Items</span>
                  <a href="javascript:;" data-toggle="modal" data-target="#modal_payment"  >Ver carrito</a>
                </div>
              
                <div class="bottom">
                  <div class="total">
                    <span>Total</span>
                    <span class="total-amount">$<span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span></span>
                  </div>
                  <a href="javascript:;"  class="btn animate btn_confirmar1">Checkout</a>
                </div>

              </div>
              <!--/ End Shopping Item -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Header Inner -->
  <div class="header-inner">
    <div class="container">
      <div class="cat-nav-head">
        <div class="row">
          <div class="col-12">
            <div class="menu-area">
              <!-- Main Menu -->
              <nav class="navbar navbar-expand-lg">
                <div class="navbar-collapse">	
                  <div class="nav-inner">	
                    <ul class="nav main-menu menu navbar-nav">
                      <li class="active"><a href="{{route('home')}}">Inicio</a></li>
                      <li><a href="{{route('Medicines')}}">Farmacia</a></li>												
                      <li><a href="{{route('MedicalsHome')}}">Médicos</a></li>
                      <li><a href="#">Mis pacientes<span></span> <i class="ti-angle-down"></i></a>
                        <ul class="dropdown">
                          <li>
                          <a href="{{route('Quotes')}}"  >Solicitudes Nuevas</a>
                          </li>
                          
                          <li> <a href="{{route('PatientsHome')}}"class="btn animate btn_confirmar1">Ver todos mis pacientes</a></li>
                        </ul>
                      </li>
                      <li><a href="{{route('PatientsHome')}}">Mis pacientes</a></li>
                      <li><a href="{{route('Medicals')}}">Contacto</a></li>
                      <li><a href="#">Mi carrito <span id="simpleCart_quantity" class="simpleCart_quantity badge badge-primary"></span> <i class="ti-angle-down"></i></a>
                        <ul class="dropdown">
                          <li>
                            <a href="javascript:;"  class="btn animate button-cart btn-primary simpleCart_view" data-toggle="modal" data-target="#modal_payment" >Ver carrito</a>
                          </li>
                          
                            <li> <a href="#" class="btn animate btn_confirmar1">Ir a Checkout</a></li>
                        </ul>
                      </li>
                      <li>
                       
                      </li>
                      
                      </ul>
                  </div>
                
                </div>
              </nav>
              <!--/ End Main Menu -->	
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--/ End Header Inner -->
</header>
<!--/ End Header -->

 