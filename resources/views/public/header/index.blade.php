  <!-- Header -->
  <header class="header shop">
    <div class="middle-inner row-mobile-nav" style="background-color: #6dc2e7">
      <div class="container">
        <div class="row " >
          <div class="col-lg-12 col-md-2 col-12">
            <a class="text-light nav-logo" href="{{route('index')}}"><img src="{{asset('img/logo.png')}}"  alt="">MEDLINE APP</a>					
            <div class="search-form-home text-lg-right">

              <form class="" method="POST" action="{{route('SearchMedicines')}}">
                <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
              <div class="div-search">
                <a class="pr-1" href=""><i class="fa fa-facebook "></i></a> 
              <a class="pr-1" href=""><i class="fa fa-instagram "></i></a> 
               <a class="pr-1" href=""><i class="fa fa-twitter "></i> </a>
               <a class="pr-1" href=""><i class="fa fa-phone "></i> </a>
               <a class="pr-3" href=""><i class="fa fa-map-marker "></i> </a>
                <button type="submit"><i class="fa fa-search icon-search"></i></button>
                <input type="search" class="input-search" placeholder="Buscar Medicamento..." name="searchInput">
              </div>
              </form>
          </div>
            
            <div class="mobile-nav pb-3" >    </div>

              <!--/ End Search Form -->
            <!--/ End Search Form -->
  
            </div>
          </div>
        </div>
  
      </div>

  <!-- Header Inner -->
  <div class="header-inner">
    <div class="container">
      <div class="cat-nav-head">
        <div class="row">
          <div class="col-12">
            <div class="menu-area">
              <!-- Main Menu -->
              <nav class="navbar navbar-expand-lg">
                <div class="navbar-collapse">	
                  <div class="nav-inner">	
                    <ul class="nav main-menu menu navbar-nav">
                      <li class="active"><a href="{{route('Medicines')}}">Inicio</a></li>
                      <li><a href="{{route('Medicines')}}">Farmacia</a></li>												
                      <li><a href="{{route('Medicines')}}">Citas médicas</a></li>
                      <li><a href="{{route('Medicals')}}">Contacto</a></li>
                      <li> 
                        @if(Auth::check())
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                        </form>
                        <a href="javascript:;" onclick="event.preventDefault();document.getElementById('logout-form').submit();"   >Cerrar sesión</a></li>

                      @else
                      <a href="{{route('login')}}">Iniciar sesión</a></li>

                      @endif
                     
                    </ul>
                  </div>
                
                </div>
              </nav>
              <!--/ End Main Menu -->	
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--/ End Header Inner -->
</header>
<!--/ End Header -->

 