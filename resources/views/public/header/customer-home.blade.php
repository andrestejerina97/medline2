  <!-- Header -->
  <header class="header shop">
      <div class="middle-inner row-mobile-nav" style="background-color: #6dc2e7">
        <div class="container">
          <div class="row " >
            <div class="col-lg-12 col-md-2 col-12">
              <a class="text-light nav-logo" href="{{route('index')}}"><img src="{{asset('img/logo.png')}}"  alt="">MEDLINE APP</a>					
              <div class="search-form-home text-lg-right">

                <form class="" method="POST" action="{{route('SearchMedicines')}}">
                  <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class="div-search">
                  <a class="pr-1" href=""><i class="fa fa-facebook "></i></a> 
                <a class="pr-1" href=""><i class="fa fa-instagram "></i></a> 
                 <a class="pr-1" href=""><i class="fa fa-twitter "></i> </a>
                 <a class="pr-1" href=""><i class="fa fa-phone "></i> </a>
                 <a class="pr-3" href=""><i class="fa fa-map-marker "></i> </a>
                  <button type="submit"><i class="fa fa-search icon-search"></i></button>
                  <input type="search" class="input-search" placeholder="Buscar Medicamento..." name="searchInput">
                </div>
                </form>
            </div>
              
              <div class="mobile-nav pb-3" >    </div>

                <!--/ End Search Form -->
              <!--/ End Search Form -->
    
              </div>
            </div>
          </div>
    
        </div>

  <!-- Header Inner -->
  <div class="header-inner" >
    <div class="container-fluid">
      <div class="cat-nav-head">
        <div class="row">
          <div class="col-lg-4 mt-4">
            <a class="text-light nav-logo" href="{{route('index')}}"><img src="{{asset('img/logo.png')}}"  alt="">MEDLINE APP</a>					
          </div>
          <div class="col-8 col-lg-8 mt-4">
             
              <div class="search-form-home text-lg-right">

                <form class="" method="POST" action="{{route('SearchMedicines')}}">
                  <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class=" div-search">
                  <a class="pr-1" href=""><i class="fa fa-facebook "></i></a> 
                <a class="pr-1" href=""><i class="fa fa-instagram "></i></a> 
                 <a class="pr-1" href=""><i class="fa fa-twitter "></i> </a>
                 <a class="pr-1" href=""><i class="fa fa-phone "></i> </a>
                 <a class="pr-3" href=""><i class="fa fa-map-marker "></i> </a>
                  <button type="submit"><i class="fa fa-search icon-search"></i></button>
                  <input type="search" class="input-search" placeholder="Buscar Medicamento..." name="searchInput">
                </div>
                </form>
            </div>
 
        </div>
      </div>
        <div class="row">
          <div class="col-12 col-lg-12">
       
            <div class="menu-area">
              <!-- Main Menu -->

              <nav class="navbar navbar-expand-lg">

                <div class="navbar-collapse">	

                  <div class="nav-inner">

                      <ul class="nav main-menu menu navbar-nav">
                        
                        <li class=""><a href="{{route('home')}}">Inicio</a></li>
                      <li><a href="{{route('Medicines')}}">Farmacia</a></li>												
                      <li><a href="{{route('MedicalsHome')}}">Médicos</a></li>                    
                      <li><a href="#">Mi carrito <span id="simpleCart_quantity" class="simpleCart_quantity badge badge-primary"></span> <i class="ti-angle-down"></i></a>
                        <ul class="dropdown">
                          <li>
                            <a href="javascript:;"  class="btn animate button-cart btn-primary simpleCart_view" data-toggle="modal" data-target="#modal_payment" >Ver carrito</a>
                          </li>
                          
                            <li> <a href="#" class="btn animate btn_confirmar1">Ir a Checkout</a></li>
                        </ul>
                      </li>
                      <li>
                        <div class="dropdown-cart-header">
                          @if(Auth::check())
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
                            </form>
                            <a onclick="event.preventDefault();document.getElementById('logout-form').submit();"  href="javascript:;"  >Cerrar sesión <i class="fa fa-close"></i></a>
                
                          @else
                          <a  href="{{route('login')}}">Iniciar sesión <i class="fa fa-user"></i></a>
                
                          @endif
                      
                    </div>
                      </li>
                      <li><a href="{{route("register")}}">Registrarse</a></li>                    
                      <li><a href="{{route('MedicalsHome')}}">Contacto</a></li>                    

                      
                      </ul>


                      
                  </div>
                
                </div>
              </nav>
              <!--/ End Main Menu -->	
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--/ End Header Inner -->
</header>
<!--/ End Header -->

 