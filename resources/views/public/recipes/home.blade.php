


@extends('layouts.public')

@section('content')
             
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
<input type="hidden" id="id_login" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" >

	<!-- Recipe section -->
  <section id="recipe_section" class="product-area shop-sidebar shop section" >
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2>Nueva receta</h2>
                </div>
            </div>
        </div>
<!--Card Quotes Medical -->
<div class="row mx-auto">

      <!--############CAR#############-->
      <div class="card col-lg-6 col-md-6 col-xs-12 col-12  ">
        <div class="card-header row">
            <div class="col-lg-3  ">
              <a href="">                  <img src="{{asset('img/logo.png')}}" alt="">
              </a>
            </div>
        @foreach ($medicals as $medical)
        <div class="col-9 col-lg-9 ">
        <h5 class="text-right">{{$medical->name}}</h5> 
        <p class="text-right">{{$medical->nameSpecialty}}</p>
        <div class="text-left">
          <span >{{"Consultorio: ".$medical->nameClinic.","}}</span>
          <span >{{$medical->homeClinic}}</span>
          <span class="">{{"Citas: ".$medical->cellphone."/"}}</span>
          <span class="">{{$medical->work_phone}}</span>
          </div>
        </div>
      @endforeach
    </div>
         
        <div class="card-body ">
          <h5 class="widget_title text-center">Receta Médica</h5>
          <div class="col-lg-12 col-md-12 col-xs-12">
             
            <div class="row mt-2">
                <label for="">Enfermedad:</label>
            </div>
        <div class="row ">
            <div class="col-lg-6 input-group">
                <input type="text" class="form-control" name="name_disease" id="name_disease" placeholder="buscar nombre">
                <button id="btn_search_diseases" type="button" class="button-andres">Buscar</button>
            </div>
            <div class="col-lg-6">
        <div id="div_select_disease"></div>
    </div>
    </div>
                          
                <div class="row mt-2">
                    <label for="">Productos disponibles:</label>
                </div>
            <div class="row ">
                <div class="col-lg-6 input-group">
                    <input type="text" class="form-control" name="name_medicine" id="name_medicine" placeholder="buscar por nombre">
                    <button id="btn_search_medicine" type="button" class="button-andres">Buscar</button>
                </div>
                <div class="col-lg-6">
            <div id="div_select_medicine"></div>
        </div>
        </div>
          <div class="row mt-2">
                    <label for="">Cantidad: <span></span></label>
                  <input type="number" 
                  value="1" name="quantity" placeholder="Cantidad" id="quantity" class="form-control" > 
                </div>
            <div class="row mt-2">
              <button type="button" class="btn btn-dark text-center" id="btn_add_recipe" disabled >Agregar otro </button>
            </div>
            <!--Datos de la consulta -->
              <!--Fin Datos de la consulta -->
              <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
          </div>

        </div>
      </div>
      <!--############END CARD#############-->
 <!--############CAR#############-->
 <div class="card col-lg-6 col-md-6 col-xs-12 col-12 ">
  <div class="card-header row">
    <div class="col-lg-3">
      <a href=""> <img src="{{asset('img/logo.png')}}" alt="">
      </a>
    </div>
@foreach ($medicals as $medical)
<div class="col-9 col-lg-9">
<h5 class="text-right">{{$medical->name}}</h5> 
<p class="text-right">{{$medical->nameSpecialty}}</p>
<div class="text-left">
<span >{{"Consultorio: ".$medical->nameClinic.","}}</span>
<span >{{$medical->homeClinic}}</span>
  <span >{{"Citas: ".$medical->cellphone."/"}}</span>
  <span >{{$medical->work_phone}}</span>
  </div>
</div>
@endforeach
    </div>
   <div class="card-body ">
     <h5 class="widget_title text-center">Indicaciones</h5>
     <div class=" mt-3 col-lg-12 col-12 col-md-12 col-xs-12">
        <div class="row">
            <label for="">Medicamento:</label>
             <div id="div_select_medicine_indication" class="form-group">
                 <select class="form-control " id="select_medicine_indication" name="select_medicine_indication">
                  <option value="">No hay medicamento seleccionado</option>
                </select>
             </div>
         </div>
         <div class="row">
         <div class="col-lg-3 col-3 col-xs-3">
          <label for="">Consumir:</label>
          </div>
          <div class="col-lg-4 col-3 col-xs-4">
            <input type="number" 
            value="1" name="consume_quantity" placeholder="Cantidad" id="consume_quantity" class="form-control " > 
          </div>
          <div class="col-lg-4 col-6 col-xs-6">
            <select class="form-control " name="pack_recipe" id="pack_recipe">
              <option value="1">comprimido</option>
              <option value="2">Tabletas</option>

            </select>
            </div>
    </div>
        <div class="row">
         <div class="col-lg-3 col-xs-3 col-3">
          <label for="">Cada:</label>
          </div>
          <div class="col-lg-4 col-xs-3 col-3">
            <input type="number" 
            value="1" name="consume_frecuency" placeholder="consume_frecuency" id="consume_frecuency" class="form-control " > 
          </div>
          <div class="col-lg-4 col-xs-4 col-6">
            <select class="form-control " name="time_medicine" id="time_medicine" multiple>
              <option value="1">Horas</option>
            </select>
            </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-3">
       <label for="">Por:</label>
       </div>
       <div class="col-lg-4 col-3">
         <input type="number" 
         value="1" name="days_quantity" placeholder="Cantidad" id="days_quantity" class="form-control " > 
       </div>
       <div class="col-lg-4 col-6">
         <select class="form-control " name="time_quantity_days" id="time_quantity_days">
           <option value="1">Días</option>
         </select>
         </div>
 </div>
 <div class="row mt-2">
  <textarea name="observation" id="observation" rows="2">Observaciones</textarea>
  </div>
  <div class="row mt-2">
    <button type="button" id="btn_add_indication" class="btn btn-dark text-center " >Agregar otro </button>
  </div>

       <!--Datos de la consulta -->

         <!--Fin Datos de la consulta -->
         <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
     </div>

   </div>
 </div>
 <!--############END CARD#############-->
    
    <div class="mx-auto mt-3">
        <div id="message_home"></div>
        <div id="message_indication"></div>
        <div id="message_error"></div>

        <button type="button" id="btn_new_recipe" class="btn bg-succes text-center " >Generar receta</button>
        <form method="post" action="{{route('PfdSendRecipe')}}">
          <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
          <input type="hidden"  id="id_medical_quote" name="id_medical_quote" value="{{ $quotes}}">

      <button type="submit" id="btn_send_pdf" class="btn bg-succes text-center " style="display: none">Enviar receta</button>
    
    </form>
    </div>
<!--END card Quotes Medical -->
</div>
</div>
</section>

@endsection

@section('scripts')
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script type="text/javascript">

$(function(){

$(document).on('click','#btn_search_medicine',function(e){
e.preventDefault();
let name_medicine= document.getElementById('name_medicine').value;

if(name_medicine==''){
let au='<div class="alert alert-danger msj" role="alert"><p>CUIDADO! Debe ingresar algún nombre o indicio  válido</p> </div>';
$("#message_error").html(au);
return false;
}else{
    
}
//document.getElementById("btn_search_medicine").disabled = true;
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

         

         $.ajax({
            url: '{{ route('SearchMedicineForRecipe') }}',
            type: 'POST',
            data:'name_medicine='+name_medicine,
            dataType:'json',
            success: function(response) {
                let options='<select class="form-control " onchange="fill_select_medicine_indication();" id="select_medicine" name="select_medicine">'; 
               for (let iterator in response.medicines) {
                options+="<option value='"+response.medicines[iterator].id+"'>"+response.medicines[iterator].name+', '+response.medicines[iterator].description +"</option>";
               }
               options+='</select>';
               $("#div_select_medicine").html(options);
               $("#div_select_medicine").css('display','block');
               document.getElementById("btn_search_medicine").disabled = false;
               document.getElementById("btn_add_recipe").disabled = false;

               //combo para las indicaciones
               let id_medicine= document.getElementById('select_medicine').value;
               let select_medicine_indication= document.getElementById('select_medicine_indication');
               let combo = document.getElementById("select_medicine");
               let selected = combo.options[combo.selectedIndex].text;
               optionsM+="<option value='"+id_medicine+"'>"+selected+"</option>";
                combo='<select class="form-control " id="select_medicine_indication" name="select_medicine_indication">'+optionsM+"</select>";
               $("#div_select_medicine_indication").html(combo);
               document.getElementById("btn_add_indication").disabled = false;
               document.getElementById("btn_add_recipe").disabled = false;
                document.getElementById("btn_new_recipe").disabled = false;
            }
        });
});
// Search diseases 
$(document).on('click','#btn_search_diseases',function(e){
let name_disease= document.getElementById('name_disease').value;

e.preventDefault();
if(name_disease==''){
let au='<div class="alert alert-danger msj" role="alert"><p>CUIDADO! Debe ingresar alguna enfermedad válida</p> </div>';
$("#message_error").html(au);
return false;
}else{

}
document.getElementById("btn_search_diseases").disabled = true;
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

         

         $.ajax({
            url: '{{ route('SearchDiseasesForRecipe') }}',
            type: 'POST',
            data:'name_disease='+name_disease,
            dataType:'json',
            success: function(response) {
                let options='<select class="form-control "  id="select_disease" name="select_disease">'; 
               for (let iterator in response.diseases) {
                options+="<option value='"+response.diseases[iterator].CIE10+"'>"+response.diseases[iterator].descripcion_CIE +"</option>";
               }
               options+='</select>';
               $("#div_select_disease").html(options);
               $("#div_select_disease").css('display','block');
               document.getElementById("btn_search_diseases").disabled = false;
               document.getElementById("btn_add_recipe").disabled = false;

               
            }
        });
});
//end search diseases
//add indication
$(document).on('click','#btn_add_indication',function(){
  document.getElementById("btn_add_indication").disabled = true;

let id_medicine= document.getElementById('select_medicine').value;
let consume_quantity= document.getElementById('consume_quantity').value;
let pack_recipe= document.getElementById('pack_recipe').value;
let consume_frecuency= document.getElementById('consume_frecuency').value;
let time_medicine= document.getElementById('time_medicine').value;
let observation= document.getElementById('observation').value;
let days_quantity= document.getElementById('days_quantity').value;
let time_quantity_days=document.getElementById('time_quantity_days').value;
recipes.append('id_medicine_indication'+count_indication,id_medicine);
recipes.append('consume_quantity'+count_indication,consume_quantity);
recipes.append('pack_recipe'+count_indication,pack_recipe);
recipes.append('consume_frecuency'+count_indication,consume_frecuency);
recipes.append('consume_frecuency_text'+count_indication,time_medicine);
recipes.append('observation'+count_indication,observation);
recipes.append('quantity_days'+count_indication,days_quantity);
recipes.append('quantity_days_text'+count_indication,time_quantity_days);

document.getElementById('observation').value="";
let au='<div class="alert alert-success msj" role="alert"><p>Indicación agregada! '+count_indication+" Items en total"+'</p> </div>';
$("#message_indication").html(au);
count_indication++;
document.getElementById("btn_add_indication").disabled = false;

}); 
//end add indication
//add recipte
$(document).on('click','#btn_add_recipe',function(){
  
let id_medicine= document.getElementById('select_medicine').value;
let quantity= document.getElementById('quantity').value;
let id_disease= document.getElementById('select_disease').value;
document.getElementById("btn_add_recipe").disabled = true;

if (id_medicine =='' || id_disease=="") {
let au='<div class="alert alert-danger msj" role="alert"><p>CUIDADO! Debe ingresar alguna enfermedad o producto válido</p> </div>';
$("#message_error").html(au);
return false;
}else{

}

recipes.append('id_medicine'+count_medicine,id_medicine);
recipes.append('quantity'+count_medicine,quantity);
recipes.append('id_disease'+count_medicine,id_disease);


$("#div_select_medicine").css('display','none');
document.getElementById('name_medicine').value="";
let au='<div class="alert alert-success msj" role="alert"><p>Receta agregada! '+count_medicine+" Items en total"+'</p> </div>';
$("#message_home").html(au);
count_medicine++;
document.getElementById("btn_add_recipe").disabled = true;
document.getElementById("btn_new_recipe").disabled = false;
document.getElementById("btn_search_disease").disabled = false;

});

$(document).on('click','#btn_new_recipe',function(){
  document.getElementById("btn_new_recipe").disabled = true;

  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

let id_medicine= document.getElementById('select_medicine').value;
let quantity= document.getElementById('quantity').value;
let id_disease= document.getElementById('select_disease').value;
if (id_medicine =='' || id_disease=="") {
let au='<div class="alert alert-danger msj" role="alert"><p>CUIDADO! Debe ingresar alguna enfermedad o producto válido</p> </div>';
$("#message_error").html(au);
return false;
}
recipes.append('id_medicine'+count_medicine,id_medicine);
recipes.append('quantity'+count_medicine,quantity);
recipes.append('id_disease'+count_medicine,id_disease);

let consume_quantity= document.getElementById('consume_quantity').value;
let pack_recipe= document.getElementById('pack_recipe').value;
let consume_frecuency= document.getElementById('consume_frecuency').value;
let time_medicine= document.getElementById('time_medicine').value;
let observation= document.getElementById('observation').value;
let days_quantity= document.getElementById('days_quantity').value;
let time_quantity_days=document.getElementById('time_quantity_days').value;
let id_medical_quote=document.getElementById('id_medical_quote').value;
recipes.append('id_medicine_indication'+count_indication,id_medicine);
recipes.append('consume_quantity'+count_indication,consume_quantity);
recipes.append('pack_recipe'+count_indication,pack_recipe);
recipes.append('consume_frecuency'+count_indication,consume_frecuency);
recipes.append('consume_frecuency_text'+count_indication,time_medicine);
recipes.append('observation'+count_indication,observation);
recipes.append('quantity_days'+count_indication,days_quantity);
recipes.append('quantity_days_text'+count_indication,time_quantity_days);


recipes.append('count_medicines',parseInt(count_medicine)); // se elimina 1 porque no se agrega el último producto
recipes.append('count_indication',parseInt(count_indication));// se elimina 1 porque no se agrega la última indicación
recipes.append('id_medical_quote',id_medical_quote); // se elimina 1 porque no se agrega el último producto


         $.ajax({
            url: '{{ route('SaveRecipe') }}',
            type: 'POST',
            data: recipes,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            success: function(response) {
                if(response.message==1){
                  swal("Excelente!", "Receta creada con éxito!", "success");
                    document.getElementById("btn_send_pdf").style.display = "block"; 
                    document.getElementById("btn_new_recipe").style.display = "none";
                    let au='<div class="alert alert-success msj" role="alert"><p>Receta generada con éxito</p> </div>';
                    $("#message_error").html(au);
                    document.getElementById('id_medical_quote').value=response.id_medical_quote;


                }
            }   
       
      });
    });
      //end add recipte


}); 

var recipes= new FormData();
var count_indication=1;
var optionsM="";
var count_medicine=1;
function fill_select_medicine_indication(){
 
 //combo para las indicaciones
           let id_medicine= document.getElementById('select_medicine').value;
           let select_medicine_indication= document.getElementById('select_medicine_indication');
           let combo = document.getElementById("select_medicine");
           let selected = combo.options[combo.selectedIndex].text;
            optionsM+="<option value='"+id_medicine+"'>"+selected+"</option>";
            combo='<select class="form-control " id="select_medicine_indication" name="select_medicine_indication" >'+optionsM+"</select>";
           $("#div_select_medicine_indication").html(combo);
}
  </script>   
@endsection