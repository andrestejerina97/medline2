        <html lang="es">
      <head>
          <!-- Meta Tag -->
          <meta charset="utf-8">
          <title>{{ config('app.name', 'MedlineApp') }}</title>

          <link rel="stylesheet" href="{{asset('public/css/bootstrap.css')}}">

      </head>
      <body class="js" style="background-image: url({{asset('img/imagepdf.png')}});  background-repeat: no-repeat; background-position: center;  opacity: 0.5; ">
 <table style="position: fixed; top: -68px; width:100% height: 100%">
  <tr >
  <td style="width:49%;"> 
          
      <!--############CAR#############-->
         <div  class=""  style="color:darkslategrey; ">
          <div class="" style="position: relative; top:50px">
            <img class="text-left" src="{{asset('img/iconpdf.png')}}" alt="">
          </div>
            @foreach ($medicals as $medical)
            <h5  class="text-right ">{{$medical->name}}</h5> 
            <p class="text-right ">{{$medical->nameSpecialty}}</p>
            <div class="text-right">
              <span style="font-size: 70%">{{$medical->nameClinic.","}}</span>
              <span style="font-size: 70%">{{$medical->homeClinic}}</span>
              <span style="font-size: 70%">{{"Cel: ".$medical->cellphone."/"}}</span>
              <span style="font-size: 70%">{{$medical->work_phone}}</span>
              <p style="font-size: 70%">Fecha:{{$fecha}}</p>
              </div>
          @endforeach
    </div>
    <div class="" style=" width: 100%;" >
      @foreach ($information_aditionals as $information_aditional)
      <div class="text-center ml-2">
        <span style="font-size: 70%">CI: {{$information_aditional->cedula}}</span> &nbsp;&nbsp;
          <span style="font-size: 70%">Nombre: {{$information_aditional->name_patient.' '.$information_aditional->lastname_patient}}
          </span>

      </div>
      @endforeach
    </div>
       </td> 
       <td style="padding-left: 20px;">
    <div  class=""  style="color:darkslategrey; ">
     <div class="" style="position: relative; top:50px">
       <img class="ml-2" src="{{asset('img/iconpdf.png')}}" alt="">
     </div>
       @foreach ($medicals as $medical)
       <h5  class="text-right ">{{$medical->name}}</h5> 
       <p class="text-right ">{{$medical->nameSpecialty}}</p>
       <div class="text-right">
         <span style="font-size: 70%">{{$medical->nameClinic.","}}</span>
         <span style="font-size: 70%">{{$medical->homeClinic}}</span>
         <span style="font-size: 70%">{{"Cel: ".$medical->cellphone."/"}}</span>
         <span style="font-size: 70%">{{$medical->work_phone}}</span>
         <p style="font-size: 70%">Fecha:{{$fecha}}</p>
         </div>
     @endforeach
</div>
<div class="" style=" width: 100%;" >
 @foreach ($information_aditionals as $information_aditional)
 <div class="text-center ml-2">
   <span style="font-size: 70%">CI: {{$information_aditional->cedula}}</span> &nbsp;&nbsp;
     <span style="font-size: 70%">Nombre: {{$information_aditional->name_patient.' '.$information_aditional->lastname_patient}}
     </span>

 </div>
 @endforeach
</div>     
</td></tr>
<tr>
  <td style="width:49%;">
          <h5 class="text-center">RECETA MÉDICA</h5>

            @foreach ($recipes as $recipe)
            <div class="text-left ml-2">

              <span style="font-size: 70%">{{'-'.$recipe->name.', '.$recipe->description.' '.$recipe->quantity}} </span>
         </div>

              @endforeach

        
        </div>
      </td>
      <!--############END CARD#############-->
      <td style="width:49%;">

        <h5 class="text-center">INDICACIONES</h5>

        @foreach ($indications as $indication)
        <div class="text-left ml-2">
          <span style="font-size: 70%">{{$indication->name." ".$indication->description.', tomar '.$indication->consume_quantity." cada ". $indication->consume_frecuency
          ." durante ".$indication->quantity_days}}
          </span>
      </div>
      @endforeach
      </td> </tr>
      <tr><td style="width: 49%"></td><td style="width: 49%;padding-left:10px;">
      <h5 class="text-center mt-2"style=" font-size: 90%">DIAGNOSTICO Y OBSERVACIONES</h5>

      <div style="margin-top:2px; border: 1px solid;">
        @foreach ($recipes as $recipe)
          <span style="font-size: 80%">{{$recipe->id_disease." "}}
          </span>
      @endforeach
      @foreach ($indications as $indication)
      <small style="font-size: 70%">{{$indication->observation.". "}}
      </small>
  @endforeach
      </div>

  
  
      </td></tr>

</table>  

<div style="position: fixed;
bottom: 13px;
">
<table style="width:100%">
  <tr>
    <td>
        <hr style="border: 1px solid color:darkslategrey; width:70%; margin-left:100px">
        <div style="text-align:right">
         @foreach ($medicals as $medical)
     <span style="text-align: right">{{$medical->name}}</span>
         @endforeach
      
           </div></td>
    <td> 
      <hr style="border: 1px solid color:darkslategrey; width:70%; margin-left:100px">
      <div style="text-align:right">
         @foreach ($medicals as $medical)
     <span style="text-align: right">{{$medical->name}}</span>
         @endforeach
  
           </div></td>
  </tr>
</table>
</div>
</body>
</html>