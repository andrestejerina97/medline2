
@extends('layouts.public')

@section('content')
             

	<!-- Recipe section -->
  <section id="section_my_recipes" class="product-area shop-sidebar shop section " >
    <div class="container">
      <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2>Mis recetas</h2>
                </div>
            </div>
       
<!--Card Quotes Medical -->
<div class="table-responsive col-lg-12">
  <table class="table table-hover">
    <thead>
    <tr>
      <th>Id</th>
      <th>Paciente</th>
      <th>Cédula</th>
      <th>Fecha de creación</th>
      <th></th>
      <th></th>

    </tr>
  </thead>
  <tbody>
    @foreach ($recipes as $recipe)
      <tr>
      <td>{{$recipe->id}}</td>
      <td>{{$recipe->name_patient.' '. $recipe->lastname_patient}}</td>
      <td>{{$recipe->cedula}}</td>
      <td>{{$recipe->created_at}}</td>
      <td>
      <form method="post" action="{{route('RecipeDowloadPdf')}}">
      <input type="hidden" id="id_information"value="{{$recipe->id}}" name="id_information">
          <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
      <button type="submit"   id="btn_send_pdf" class="button-andres-table text-light" >Descargar</button>
    </form>
     </td>
      <td><form> <a href="{{route('RecipeDelete',['id_recipe'=>$recipe->id])}}" class="button-andres bg-danger text-light">Eliminar</a></form></td>
      </tr>
    @endforeach
  </tbody>
  </table>
</div>
<!--END card Quotes Medical -->
</div>
</div>
</section>

@endsection
