

@extends('layouts.public')

@section('content')

                        
                        
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
<input type="hidden" id="id_login" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" >

	<!-- Product Style -->
  <section id="shop_section"class="product-area shop-sidebar shop section">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-12">
          <div class="shop-sidebar">
              <!-- Single Widget -->
              <div class="single-widget specialty">
                <h3 class="title">Especialidades</h3>
                <ul class="categor-list">
                  @foreach ($specialties as $specialty)  
                  <li>
                    <a href="{{route('MedicalsForspecialty',['specialty'=>$specialty->id])}}">{{$specialty->name}}</a>
                </li>
                  @endforeach 
                  
                </ul>
              </div>

              								<!-- Single Widget -->
								<div class="single-widget recent-post">
									<h3 class="title">Listado Productos</h3>
                <ul class="view-mode ">
                  <li ><a class="last_page" href="javascript:;"><i class="fa fa-chevron-left"></i></a></li>
                  <li ><a class="last_page" href="javascript:;"></a></li>
                  <li><a class="next_page" href="javascript:;"><i class="fa fa-chevron-right"></i></a></li>
                </ul>
								</div>
								<!--/ End Single Widget -->

                <div class="single-widget specialty">
                  <h3 class="title">Mi carrito <i class="ti-shopping-cart"></i></h3>
              <!-- Shopping Item -->
              <div class="shopping-item">
                <div class="dropdown-cart-header">
                  <span><span id="simpleCart_quantity" class="simpleCart_quantity"></span> Items</span>
                </div>
              
                <div class="bottom">
                  <div class="total">
                    <span>Total</span>
                    <span class="total-amount">$<span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span></span>
                  </div>
                  <div class="row">
                    <button  class="button-cart btn-primary simpleCart_view" data-toggle="modal" data-target="#modal_payment"  >Ver carrito</button>
                  </div>

                </div>
              </div>
              <!--/ End Shopping Item -->
                </div>
          </div>
        </div>

<div class="col-lg-9 col-md-8 col-12">
  <!-- Start notifications wiget-->
  <div class="">          
    <div  id="message_home" >
  </div>

  @if ($errors->any())
  <div class="alert alert-danger alert-dismissible fade show">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>

          @endforeach
      </ul>
  </div>
@endif
</div>


<!--End notification wiget-->
<div class="col-12">
  <div class="row">
            
    <div class="col-12">
        <div class="section-title">
            <h2>Médicos destacados</h2>
        </div>
    </div>
</div>
  <div class="owl-medical popular-slider">
      @isset($medicals)
      @foreach ($medicals as $medical)
    
          <div class="product-img">
              
              <a>
                  <img class="default-img" src="{{asset('storage/public/medicals/'.$medical->file)}}"
                      alt="#">

                </a>
                <div class="ml-5">
                  <h4>{{$medical->name}}</h4>
                  <h5>{{$medical->nameSpecialty}}</h5>
                  <form action="">
                    <button type="submit" class="btn">Ver</button>
                  </form>

                 </div>        
          </div>
          @endforeach

          @endisset
       </div>
</div>
        </div>
        
      </div>
 
    </div>
  
  <!--/ End Product Style 1  -->	

  
  <!-- Modal -->
    <div class="modal fade" id="modal_payment" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
          </div>
          <div class="modal-body">
            <div class="row no-gutters">           
              <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
                <div class="quickview-content">
                  <h4 class="widget_title">Mi carrito de compra:</h4>

                  <div class="simpleCart_items table-responsive" >

                   <!-- <table class="table table-hover">
                      <tr>
                      <th>Id</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Cantidad</th>
                      <th>Precio</th>
                      <th>Subtotal</th>
                    </tr>
                </table>
                  -->
                      <div class="simpleCart_table_body"></div>

                   

                  </div>
                  -----------------------------<br/>
                  Final Total: <span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span> <br />
                  <div class="modal-footer">
                   <button type="button" class="btn " data-dismiss="modal">cerrar</button>
                   <button type="button"  class="btn  btn_confirmar1 ">Confirmar Compra</button>
                   </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal end -->
 
      <!-- Modal -->
      <div class="modal fade" id="modal_register" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <div class="modal-body">
              <div class="row no-gutters">           
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
                  <div class="quickview-content">
                    <h4 class="widget_title">Formulario de registro</h4>
  
                    @include('auth.formRegister')

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal end -->
  
  <!-- Modal -->
  <div class="modal fade" id="modal_login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
        </div>
        <div class="modal-body">
          <div class="row no-gutters">           
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
              <div class="quickview-content ">
                <div id="login_error" style="display: none" class="alert alert-danger alert-dismissible fade show">
                  
              </div>
                <h4 class="widget_title">Ups! parece que no iniciaste sesión</h4>

                @include('auth.formLogin')
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal end -->

  <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">

 



</section>
<!--############SECTION CHECKOUT#############-->
<!--############END SECTION CHECKOUT#############-->
@endsection

@section('scripts')
<script type="text/javascript">



 $(function(){


 })

  </script>   
@endsection