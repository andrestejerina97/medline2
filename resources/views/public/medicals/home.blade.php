@extends('layouts.public')

@section('content')



<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<input type="hidden" id="id_login" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}">

<!-- Product Style -->
<section id="shop_section" class="product-area shop-sidebar shop section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-12">
                <div class="shop-sidebar">
                    <!-- Single Widget -->
                    <div class="single-widget-category specialty">
                        <h3 class="title">Especialidades</h3>
                        <ul class="categor-list">
                            @foreach ($specialties as $specialty)
                            <li>
                                <a
                                    href="{{route('MedicalsForspecialty',['specialty'=>$specialty->id])}}">{{$specialty->name}}</a>
                            </li>
                            @endforeach

                        </ul>
                    </div>

                    <!-- Single Widget -->
                    <div class="single-widget recent-post">
                        <h3 class="title">Listado Médicos</h3>
                        <ul class="view-mode ">
                            <li><a class="last_page" href="{{$medicals->previousPageUrl() }}"><i
                                        class="fa fa-chevron-left"></i></a></li>
                            <li><a class="last_page" href="javascript:;">{{$medicals->currentPage()}}</a></li>
                            <li><a class="next_page" href="{{$medicals->nextPageUrl() }}"><i
                                        class="fa fa-chevron-right"></i></a></li>
                        </ul>
                    </div>
                    <!--/ End Single Widget -->

   
                </div>
            </div>

            <div class="col-lg-9 col-md-8 col-12">
                <!-- Start notifications wiget-->


                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>

                        @endforeach
                    </ul>
                </div>
                @endif



                <!--End notification wiget-->

                <div class="row">

                    <div class="col-12">
                        <div class="section-title">
                            <h2>Médicos destacados</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @isset($medicals)

                    @foreach ($medicals as $medical)

                    <div class="col-lg-4 col-md-6 col-xs-6 col-6">

                            <div class="product-img ">
                                <a>
                                    @if($medical->photo != null)
                                    <img class="default-img-medical" loading="lazy" src="{{asset('storage/public/medicals/'.$medical->photo)}}"
                                        alt="Imagen No disponible" >     
                                    @else
                                    <img class="default-img-medical" loading="lazy" src="{{asset('img/medical_not_found.png')}}"
                                    alt="Imagen No disponible" >
                                    @endif
                                   
                                </a>
                            </div>
                        <div class="mt-3">
                            <p class="text-center ">{{$medical->name}}</p>
                        </div>
                        <div class="mt-3">
                            <h5 class="text-center">{{$medical->nameSpecialty}}</h5>
                        </div>
                        <div class="mt-3">
                            <form class="text-center" action="{{route('NewQuote',['medical'=>$medical->id])}}">
                                <button type="submit" id="btn_newQuote" class="btn">Ver</button>
                            </form>
                        </div>
                    </div>
                    @endforeach

                    @endisset


                </div>
            </div>

        </div>

    </div>

    <!--/ End Product Style 1  -->


    <!-- Modal -->
    <div class="modal fade" id="modal_payment" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
                            aria-hidden="true"></span></button>
                </div>
                <div class="modal-body">
                    <div class="row no-gutters">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
                            <div class="quickview-content">
                                <h4 class="widget_title">Mi carrito de compra:</h4>

                                <div class="simpleCart_items table-responsive">

                                    <!-- <table class="table table-hover">
                      <tr>
                      <th>Id</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Cantidad</th>
                      <th>Precio</th>
                      <th>Subtotal</th>
                    </tr>
                </table>
                  -->
                                    <div class="simpleCart_table_body"></div>



                                </div>
                                -----------------------------<br />
                                Final Total: <span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span>
                                <br />
                                <div class="modal-footer">
                                    <button type="button" class="btn " data-dismiss="modal">cerrar</button>
                                    <button type="button" class="btn  btn_confirmar1 ">Confirmar Compra</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal end -->

    <!-- Modal -->
    <div class="modal fade" id="modal_register" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
                            aria-hidden="true"></span></button>
                </div>
                <div class="modal-body">
                    <div class="row no-gutters">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
                            <div class="quickview-content">
                                <h4 class="widget_title">Formulario de registro</h4>

                                @include('auth.formRegister')

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Modal end -->

    <!-- Modal -->
    <div class="modal fade" id="modal_login" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
                            aria-hidden="true"></span></button>
                </div>
                <div class="modal-body">
                    <div class="row no-gutters">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
                            <div class="quickview-content ">
                                <div id="login_error" style="display: none"
                                    class="alert alert-danger alert-dismissible fade show">

                                </div>
                                <h4 class="widget_title">Ups! parece que no iniciaste sesión</h4>

                                @include('auth.formLogin')
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Modal end -->

    <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">





</section>
<!--############SECTION CHECKOUT#############-->
<!--############END SECTION CHECKOUT#############-->
@endsection

@section('scripts')
<script type="text/javascript">




</script>
@endsection
