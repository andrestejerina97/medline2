@extends('layouts.public')

@section('css')
<link href='{{asset('fullcalendar/fullcalendar.min.css')}}' rel='stylesheet' />
<link href='{{asset('fullcalendar/fullcalendar.print.css')}}' rel='stylesheet' media='print' />
@endsection
@section('content')


<div class="row">
	<div class="col-md-6 col-lg-8 mx-auto col-10">
		<div class="card">
  		<div class="card-header" id="top">
      	<h4 class="title" id="encabezados">Calendario de Citas</h4>
  		</div>
  		<div class="card-content table-responsive" >
				<div id="calendar"></div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')


<script src='{{asset('fullcalendar/lib/jquery-ui.custom.min.js')}}'></script>
<script src='{{asset('fullcalendar/lib/cupertino/jquery-ui.min.css')}}'></script>

<script src='{{asset('fullcalendar/gcal.js')}}'></script>
<script src='{{asset('fullcalendar/lib/moment.min.js')}}'></script>
<script src='{{asset('fullcalendar/fullcalendar.min.js')}}'></script>
<script src='{{asset('fullcalendar/lang/es.js')}}'></script>
<script type="text/javascript">

$(document).ready(function() {

$('#calendar').fullCalendar({
    lang: 'es',
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
    },
    defaultDate: '{{ date('Y-m-d')}}',
    editable: false,
    minTime:"08:00",
    hiddenDays:[0],
    eventLimit: true, // allow "more" link when too many events
    events: [
      @foreach($quotesGeneral as $quoteGeneral) 
                {
                  
                  title: '{{$quoteGeneral->nameUser}}',
                  url: '{{route("FilterQuote",["quote"=>$quoteGeneral->clinical_history_id])}}',
                  start: '{{$quoteGeneral->date_quote."T".$quoteGeneral->time_quote}}',
                },
      @endforeach
            ] ,
});

});


  </script>   
@endsection