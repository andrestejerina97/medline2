@extends('layouts.public')

@section('content')
             
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
<input type="hidden" id="id_login" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" >

	<!-- Product Style -->
  <section id="request_section" class="product-area shop-sidebar shop section mb-5">
    <div class="container">
        <div class="row "> 
          <div class="col-lg-12 col-md-12  col-sm-12 col-12 ">
            <div id="div_message_home">
            </div>  
          </div>          
         <!--Table request for day -->
        <div class="col-lg-12 col-md-12  col-sm-12 col-12 ">
            <h4 class="widget_title">Solicitudes pendientes :</h4>

            <div class="table-responsive" >

              <table class="table table-hover">
                <tr>
                <th>Número de cita</th>
                <th>Paciente</th>
                <th>Día</th>
                <th>Hora</th>
                <th>Motivo</th>
                <th>Estado</th>
                <th></th>
              </tr>
          @foreach ($quotesToday as $quote)
              <tr>
              <td>{{$quote->id}}</td>
                <td>{{$quote->nameUser}}</td>
                <td>{{$quote->date_quote}}</td>
                <td>{{$quote->time_quote}}</td>
                <td>{{$quote->reason}}</td>

                <td>Pendiente</td>
              <td>
                <form> <a href="{{route('FilterQuote',['id_quote'=>$quote->id])}}" class="button-andres text-light">Ver</a></form></td>
              <td>
              <a  data-id="{{$quote->id}}" href="javascript:;" onclick="accept(this);" class="button-andres text-light bg-success btn_accept_quote">Aceptar</a>
              </td>
            </tr>
          @endforeach
        </table>
        </div>
        <ul class="pagination view-mode ">
          <li ><a class="last_page button " href="{{$quotesToday->previousPageUrl() }}"><i class="fa fa-chevron-left"></i></a></li>
          <li ><a class="last_page button " href="javascript:;">{{$quotesToday->currentPage()}}</a></li>
          <li><a class="next_page button" href="{{$quotesToday->nextPageUrl() }}"><i class="fa fa-chevron-right"></i></a></li>
        </ul>
      </div>
      <!--end Table request for day -->
    </div>  
    </div>
</section>
<!--############SECTION CHECKOUT#############-->
<!--############END SECTION CHECKOUT#############-->
@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}"></script>

<script type="text/javascript">

function accept(e) {
  
  swal({
  title: "Deseas aceptar esta solicitud?",
  text: "Una vez aceptada se le notificará al usuario y no podrás volver atrás",
  icon: "info",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        let data = 'id_quote='+e.dataset.id ;
        $(e).attr('disabled','true');     

         $.ajax({
            url: '{{route('AcceptRequest')}}',
            type: 'POST',
            data: data,
            dataType:'json',
            beforeSend:function(){
              swal({
                title: "Procesando la solicitud",
                text: "Por favor espere...",
                icon: "info",
                buttons: false,
              });
            },
            success: function(response) {
              if (response.result ==1) {
                $(e).parents("tr").remove()       

                swal("Excelente!", "La solicitud fue aceptada! Se le notificará al usuario que aceptaste su petición y encontrarás esta solicitud en TU CALENDARIO PERSONAL", "success");
                let au='<div class="alert alert-success msj" role="alert"><p>Solicitud aceptada con éxito! </p> </div>';
                $("#div_message_home").html(au);
                $.ajaxSetup({
                 headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                 });
                 
                $.ajax({
                  type: "POST",
                  url: '{{route('SendAcceptedMail')}}',
                  data: 'id_user='+response.id_user,
                  dataType: 'json',
                  success: function (data) {
                    console.log(data);
                  }
                 });

              }else{
                swal("Ups!", "Hubo un error en tu solicitud por favor vuelvo a intentarlo", "error");

              }

            },
            error:function(data){
              swal("Ups!", "hubo un error al procesar tu petición", "danger");

            },
        });
  } else {
  }
});


  
}
 


</script>   
@endsection