

@extends('layouts.public')

@section('content')                        
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
<input type="hidden" id="id_login" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" >
<!-- Start Checkout -->
<section id="checkout_section" class="product-area shop-sidebar shop section">
    <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title">
              <h2>Presentación</h2>
               
            </div>
          </div>

            <div class="col-lg-6 col-xs-10 col-md-8 mx-auto">
            @isset($medicals)
            @foreach ($medicals as $medical)
          
                <div class="product-img text-center"> 
                    <a>
                        <img class="default-img" src="{{asset('storage/public/medicals/'.$medical->photo)}}"
                            alt="#">
                      </a>
                      <hr>
                      <div  class="mt-4">
                        <h4 class="text-center">{{$medical->name}}</h4>

                    </div>
                    <div  class="mt-3">
                        <h5 class="text-center">{{$medical->nameSpecialty}}</h5>
                    </div>
                       <div class="text-center mt-3">
                        <h4>Atención: {{$medical->attention_hours}}</h4>
                        <h4>Consultorio: {{$medical->consulting_room}}</h4>
                        <b>{{$medical->nameClinic.' ,'.$medical->homeClinic}}</b>

                       </div>        
                </div>
                @endforeach
      
                @endisset
              </div>
                <!-- Form -->
                <div class="col-12">
                  <div class="section-title mt-5">
                    <h2>Solicitar de turno</h2>
                     
                  </div>
                </div>
                <div class=" mx-auto">
            <form class="form" method="post" id="form_new_quote"   accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-8  col-md-8 col-8 mx-auto">
                            <div class="form-group">
                                <label for="">Fecha de cita: <span>*</span></label>
                              <input type="date" name="date_quote" id="date_quote" class="form-control" 
                               required>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 mx-auto  col-8">
                            <div class="form-group">
                                <label for="">Hora de la cita: <span>*</span></label>
                              <input type="time" name="time_quote" id="time_quote" class="form-control" 
                              placeholder="hh:mm" required>
                            </div>
                        </div>
                        <div class="col-lg-8  col-md-8 col-8 mx-auto">
                          <div class="form-group">
                              <label for="">Motivo de consulta:</label>
                            <input type="text" name="reason" id="reason" class="form-control" 
                             required>
                          </div>
                        </div>
                          <div class="col-lg-8  col-md-8 col-8 mx-auto">
                            <div class="form-group">
                                <label for="">Enfermedad actual: </label>
                              <input type="text" name="current_illness" id="current_illness" class="form-control" 
                               >
                            </div>
                          </div>
                        <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="id_medical" name="id_medical" value="{{$id_medical}}">

                  </div>
                  <div class="row ">
                  <div class="col-lg-8 col-md-8 col-8 mx-auto">
                    <div id="div_message_home"></div>

                  <button  type="submit"  id="btn_save_quote"  class="btn ">Enviar solicitud</button>
              </div>
              <div class="col-lg-8 col-md-8 col-8 mt-3 mx-auto">
                <button type="button" onclick="history.go(-1); return false;" class="btn">Volver a sección anterior</button>                                     
</div>
            </div>
                <!--/ End Form -->
 
      </form>
    </div>
      </div>
</div>
</section>
<!--/ End Checkout -->

@endsection

@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}"></script>

<script type="text/javascript">



 $(function(){


  $("#form_new_quote").submit(function(e) {
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        document.getElementById("btn_save_quote").disabled = true;
        
        let data = $(this).serialize();
        e.preventDefault();

         $.ajax({
            url: '{{route('SaveQuote')}}',
            type: 'POST',
            data: data,
            dataType:'json',
            success: function(response) {
              if (response.result ==1) {
                swal("Excelente!", "Tu solicitud se ha enviado con éxito!,se te notificará cuando el profesional acepte tu solicitud", "success");
                let au='<div class="alert alert-success msj" role="alert"><p>Solicitud enviada! </p> </div>';
                $("#div_message_home").html(au);
                $.ajaxSetup({
                 headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                 });
                 
                $.ajax({
                  type: "POST",
                  url: '{{route('SendNewMail')}}',
                  data: 'id_medical='+response.id_medical,
                  dataType: 'json',
                  success: function (data) {
                    console.log(data);
                  }
                 });
              }else{
                swal("Ups!", "Hubo un error en tu solicitud por favor vuelvo a intentarlo", "error");

              }
              document.getElementById("btn_save_quote").disabled = false;

            }
        });
});
 

   
 })

  </script>   
@endsection