

@extends('layouts.public')

@section('content')

                        
                        
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
<input type="hidden" id="id_login" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" >

	<!-- Product Style -->
  <section id="shop_section"class="product-area shop-sidebar shop section">
    <div class="container">
      <div class="row no-gutters">           
        <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
          <div class="quickview-content">
            <h4 class="widget_title">Nuevas solicitudes:</h4>

            <div class="table-responsive" >

              <table class="table table-hover">
                <tr>
                <th>Número de cita</th>
                <th>Paciente</th>
                <th>Día</th>
                <th>Hora</th>
                <th>Estado</th>
                
              </tr>
          
          
          @foreach ($quotes as $quote)
              <tr>
              <td>{{$quote->id}}</td>
                <td>{{$quote->name}}</td>
                <td>{{$quote->date_quote}}</td>
                <td>{{$quote->time_quote}}</td>
                @if ($quote->status==0)
                <td>{{$quote->status}}</td>
                @else
                    
                @endif
              </tr>

          @endforeach

        </table>
            <div class="modal-footer">
             <button type="button" class="btn " data-dismiss="modal">cerrar</button>
             <button type="button"  class="btn  btn_confirmar1 ">Confirmar Compra</button>
             </div>
          </div>
        </div>
      </div>
    </div>
  
  
 
      <!-- Modal -->
      <div class="modal fade" id="modal_register" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <div class="modal-body">
              <div class="row no-gutters">           
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
                  <div class="quickview-content">
                    <h4 class="widget_title">Formulario de registro</h4>
  
                    @include('auth.formRegister')

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal end -->
  
  <!-- Modal -->
  <div class="modal fade" id="modal_login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
        </div>
        <div class="modal-body">
          <div class="row no-gutters">           
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
              <div class="quickview-content ">
                <div id="login_error" style="display: none" class="alert alert-danger alert-dismissible fade show">
                  
              </div>
                <h4 class="widget_title">Ups! parece que no iniciaste sesión</h4>

                @include('auth.formLogin')
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal end -->

  <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">

 



</section>
<!--############SECTION CHECKOUT#############-->
<!--############END SECTION CHECKOUT#############-->
@endsection

@section('scripts')
<script type="text/javascript">



 $(function(){


 })

  </script>   
@endsection