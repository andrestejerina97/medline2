


@extends('layouts.public')

@section('css')
<link rel="stylesheet" href="{{asset('public/css/owl-carousel.css')}}">
  <style>
    .card-header-medical{
      border-width: 0;
  outline: none;
  border-radius: 2px;
  box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
  transition: background-color .3s;
    }

    .button-andres{
      border-width: 0;
      outline: none;
      border-radius: 2px;
      box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
      transition: background-color .3s;
      font-size: 110% !important;
      margin-left: 10px;
      padding: 5px;
    }
  </style>
@endsection
@section('content')
             
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
<input type="hidden" id="id_login" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" >

	<!-- Product Style -->
  <section id="shop_section" class="product-area shop-sidebar shop section">
    <div class="container">
      <div class="text-center">
        <h3>Datos de consulta e historial clínico</h3>

      </div>
      <hr>
        <div class="row">
          <div class="card">
            <div class="card-header">
              <ul class="categor-list row">
                <li>
                    <a  class="button-andres  text-light active" data-section="patient" href="javascript:;" onclick="change_section(this);">Datos Personales</a>
                </li>
                <li>
                    <a  class="button-andres   text-light" data-section="clinical" href="javascript:;" onclick="change_section(this);">Datos Clínicos</a>
                </li>
                <li>
                  <a  class="button-andres   text-light" data-section="historial" href="javascript:;" onclick="change_section(this);">Historial Clínico</a>
                </li>
              <li>
                <a type="submit" class="button-andres  text-light" data-section="report" href="javascript:;" onclick="change_section(this);">Archivos Adjuntos</a>
              </li>
           
                </ul>
            </div>
            <div id="card_section" class="card-body">
              <section class="section_class" id="section_patient">
                <div class="row">
                  @foreach ($users as $user)

                  <div class="col-lg-2 col-md-2 col-xs-4 col-4">
                    <br><br>  
                    <div class="product-img text-center">
                      <a>
                          <img class="default-img" alt="Foto No disponible" src="{{asset('storage/public/patients/'.$user->photo)}}"
                              alt="#">
                              <span>Foto de perfil</span>
                        </a>     
                  </div>
                  </div>
                  <div class="col-lg-10 col-md-10 col-xs-8 col-8">
                    <h4 >Datos Personales : </h4>
                    <hr>
                    <form id="form_patient" onsubmit="update_patient(event,this);"  enctype="multipart/form-data" >
                      @include('public.partials.form-patient')
                    </form>

                  </div>
                  @endforeach

                </div>
              </section>
  
  
              
            <!--end card data quote-->
            <!--card data clinical -->
            <section class="section_class" style="display:none;" id="section_historial">
          <!-- card quote data-->
          <div id="card_clinical_history"  class="card mt-5">
            <div class="card-header">
                <h4 > Historial de consultas : </h4>
                </div>
                <div class="card-body">
                  @include('public.partials.form-historial')
          </div>
          </div>
          <!--End card quote data-->
            </section>
            <!--end data clinical -->

  <!-- card files atach-->
  <section class="section_class" style="display:none" id="section_report">
    <h4 >Archivos adjuntos : </h4>
  <hr>
    <div class="owl-carousel popular-slider">
      @foreach ($report_files as $report_file)
      @include('public.partials.form-atachment')
              @endforeach
  
         </div>
  </section>
  <!--End card files atach-->
  <section class="section_class" style="display:none;" id="section_clinical">
    <!-- card quote data-->
    <div id="card_clinical_history"  class="card mt-5">
      <div class="card-header">
          <h4 >Datos clínicos : </h4>
          </div>
          <div class="card-body">
            <form id="form_clinical_record" action="">
              @csrf
              @isset($clinical_records)
              @foreach ($clinical_records as $clinical_record)                  
            @include('public.partials.form-clinical_record')
            @endforeach
            @else
            @include('public.partials.form-clinical_record')
            @endisset

            <div class="text-right">
              <a>*Los datos aquí consignados corresponden a los últimos datos clínicos cargados para el actual paciente
                Pueden ser actualizados solo desde una nueva cita médica
              </a>

            </div>
          </form>

     </div>
    </div>
    <!--End card quote data-->
      </section>
      <!--end data clinical -->
      <section class="section_class" style="display:none" id="section_evaluation">
        <h4 >Evaluación de consulta : </h4>
      <hr>
      <form id="form_evaluation" method="POST" enctype="multipart/form-data">       
        @csrf
        @include('public.partials.form-evaluation')
        <br>
        <div class="text-right">
          <a href="javascript:;" onclick="save_evaluation()" class="button-andres text-center" >Guardar Evaluación</a>
        </div>
        </form>
 
      </section>
      <section class="section_class" style="display:none" id="section_new_quote">
        <form class="form" method="post" id="form_new_quote"   accept-charset="UTF-8" enctype="multipart/form-data">
          @csrf
        @include('public.partials.form-new-quote')
        <div class="text-right">
          <a href="javascript:;" onclick="save_quote()" class="button-andres text-center" >Guardar Cita </a>
        </div>
        </form>
      </section>

              </div>
              <div class="card-footer">
                <!--<a href="javascript:;" onclick="change_section(this)" data-section="new_quote" class="button-andres bg-primary  text-light"  >+ Nueva cita</a> -->
               
              </div>

<!--END card Quotes Medical -->
</div>
</div>
</div>
<br>
</section>

 <!-- Modal -->
 <div class="modal fade" id="modal_details_for_quote" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
      </div>
      <div class="modal-body">
        <div class="row no-gutters">           
          <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
            <div class="quickview-content">
              <h4 class="widget_title">Resumen recetas :</h4>
                <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                    <th>Fecha de Creación</th>
                    <th>Medicina</th>
                    <th>Cantidad</th>
                  </tr>
                  <tbody class="body_table_recipes">

                  </tbody>
                </table>
                </div>
                <hr>
                <h4 class="widget_title"> Indicaciones :</h4>

                <div class="table-responsive mt-4">
                  <table class="table table-hover">
                      <tr>
                      <th>Medicina</th>
                      <th>Cantidad</th>
                      <th>Frecuencia de consumo</th>
                      <th>Durante</th>
                      <th>Observaciones</th>

                    </tr>
                    <tbody class="body_table_indications">
  
                    </tbody>
                  </table>
                  </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal end -->
@endsection

@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}"></script>

@if(session('messages'))
<script type="text/javascript">
swal("Muy bien!",'{{session('messages')}}', "success");  
</script>   
@endif

<script type="text/javascript">
  function filter_history (element){
    let id_clinic_history = $(element).data("historyC");
    let id_medical_quote = $(element).data("quote") ;//document.getElementById('electriccars');
    let id_medical_report = $(element).data("report") ;//document.getElementById('electriccars');
    let token=$("#_token").val();
    let data= new FormData();
    data.append('id_clinic_history',id_medical_report);
    data.append('id_medical_quote',id_medical_quote);
    data.append('id_medical_report',id_medical_report);_token
    data.append('_token',token);

    $.ajax({
            url: '{{ route('FilterHistory') }}',
            type: 'POST',
            data:data,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            dataType: 'json',
            success: function(data) {
              if (data.recipes.length) {
                let lista = "";              
              for(var k in  data.recipes) {
                lista += "<tr>";              
                lista += "<td>"+data.recipes[k].created_at+"</td>";              
                lista += "<td>"+data.recipes[k].nameMedicine+' ,'+data.recipes[k].principle+"</td>";              
                lista += "<td>"+data.recipes[k].quantity+"</td>";
                lista += "</tr>";              
                }
              $(".body_table_recipes").html(lista);
              }
              if (data.indications.length) {
                lista = "";              
              for(var k in  data.indications) {
                lista += "<tr>";              
                  lista += "<td>"+data.recipes[k].nameMedicine+' ,'+data.recipes[k].principle+"</td>";              
                lista += "<td>"+data.indications[k].consume_quantity+"</td>";              
                lista += "<td>"+data.indications[k].consume_frecuency+"</td>";
                lista += "<td>"+data.indications[k].quantity_days+"</td>";
                lista += "<td>"+data.indications[k].observation+"</td>";

                lista += "</tr>";              
                }
                $(".body_table_indications").html(lista);

              }
              $("#modal_details_for_quote").modal("show");
               
            } ,error: function(data){a

              let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';
                      $("#message").html(au);
                      //$("#message_login_form .msj").fadeOut(2000);

          },
        });
  }
   $('.popular-slider').owlCarousel({
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 400,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        merge: true,
        navText: ['<i class=" ti-arrow-left"></i>','<i class=" ti-arrow-right"></i>'],
        lazyLoad:true,
        dots: false,
        responsive: {
            0: {
                items: 1,
            },
            300: {
                items: 1,
            },
            480: {
                items: 1,
            },
            768: {
                items: 1,
            },
            1170: {
                items: 1,
            },
        }
    });

    function update_patient(e,form) {
      e.preventDefault();
      let data=new FormData(form);
      document.getElementById('btn_update_patient').disabled=true;
      $.ajax({
            url: '{{ route('UpdatePatient') }}',
            type: 'POST',
            data:data,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            dataType: 'json',
            beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    closeOnClickOutside: false,
                    buttons: false,
                    
                  });
                },
            success:function(data) {
              if (data.result.length) {
                swal({
                  text:'Actualización de datos exitosa',
                  icon:"success",
                }).then((value) => {
                  location.reload();
                }); 

                let au='<div class="alert alert-success msj" role="alert">Actualización exitosa</div>';
                      $("#message").html(au);
              }else{
                swal("Ups!",'Hubo un error y no se pudo actualizar los datos', "warning");  
                let au='<div class="alert alert-danger msj" role="alert">Formato de imagen invalido</div>';
                      $("#message").html(au);
           

              }
              document.getElementById('btn_update_patient').disabled=false;

            },error: function(data){
              document.getElementById('btn_update_patient').disabled=false;
              swal("Ups!",'Hubo un error y no se pudo actualizar los datos', "warning");  

              let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';
                      $("#message").html(au);
                      //$("#message_login_form .msj").fadeOut(2000);

          },
        });

    }
    function change_section(a) {
      $(".section_class").css("display","none");
      let section_id=$(a).data("section");

      $("#section_"+section_id).css("display","block");

    }
    function save_clinical_record(a){
            var formdata = new FormData($("#form_clinical_record")[0]);
            url='{{route('ClinicalHistory.save.clinical_record',$clinical_history_id)}}';
            $.ajax({
                url         : url,
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    closeOnClickOutside: false,
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                  
                    if (data.result != null) {
                      swal("Excelente!","Datos clínicos actualizados con éxito","success");
                  
                    }

                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },

            });
    
          }
          function save_evaluation(){
            var formdata = new FormData($("#form_evaluation")[0]);
            url='{{route('ClinicalHistory.save.evaluation',$clinical_history_id)}}';
            $.ajax({
                url         : url,
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    closeOnClickOutside: false,
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal("Excelente!","Datos clínicos actualizados con éxito","success");
                  
                    }else{
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");

                    }

                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },

            });
    
          }
          function save_quote(){
            var formdata = new FormData($("#form_evaluation")[0]);
            url='{{route('SaveQuoteResource',$clinical_history_id)}}';
            $.ajax({
                url         : url,
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    closeOnClickOutside: false,
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal("Excelente!","Cita agendada con éxito","success");
                  
                    }else{
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");

                    }

                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },

            });
    
          }
          function finalize_quote(){
            var formdata = new FormData($("#form_finalize")[0]);
            url='{{route('FinalizeQuote',$clinical_history_id)}}';
            $.ajax({
                url         : url,
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    closeOnClickOutside: false,
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                      text:'La cita fue finalizada correctamente y será redirigido al historial del paciente',
                      icon:"success",
                    }).then((value)=> {
                        location.href="{{route('FilterPatient',$id_patient)}}";  
                      });
                    }else{
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");

                    }

                  },
                  error:function(data,message,res){
                    swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },

            });
    
          }
</script>
@endsection
