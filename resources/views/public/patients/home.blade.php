

@extends('layouts.public')

@section('content')

                        
                        
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
<input type="hidden" id="id_login" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" >

	<!-- Product Style -->
  <section id="shop_section"class="product-area shop-sidebar shop section mb-3">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-12">
          <div class="shop-sidebar">
              

              								<!-- Single Widget -->
								<div class="single-widget recent-post">
									<h3 class="title">Listado de Pacientes</h3>
                <ul class="view-mode ">
                <li ><a class="last_page" href="{{$patients->previousPageUrl() }}"><i class="fa fa-chevron-left"></i></a></li>
                  <li ><a class="last_page" href="javascript:;">{{$patients->currentPage()}}</a></li>
                  <li><a class="next_page" href="{{$patients->nextPageUrl() }}"><i class="fa fa-chevron-right"></i></a></li>
                </ul>
								</div>
								<!--/ End Single Widget -->

               
          </div>
        </div>

<div class="col-lg-9 col-md-8 col-12">
  <!-- Start notifications wiget-->
  <div class="">          
    <div  id="message_home" >
  </div>

  @if ($errors->any())
  <div class="alert alert-danger alert-dismissible fade show">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>

          @endforeach
      </ul>
  </div>
@endif
</div>


<!--End notification wiget-->
<div class="col-12">
            
    <div class="col-12">
        <div class="section-title">
            <h2>Mis pacientes</h2>
        </div>
</div>
<div class="row">
      @isset($patients)
      @foreach ($patients as $patient)
      <div class="col-lg-4 col-md-6 col-xs-6 col-6 ">

          <div class="product-img ">

              <a>
                @if($patient->photo != null)
                <img class="default-img-medical " loading="lazy" src="{{asset('storage/public/users/'.$patient->photo)}}"
                    alt="Imagen No disponible" >     
                @else
                <img class="default-img-medical" loading="lazy" src="{{asset('img/medical_not_found.png')}}"
                alt="Imagen No disponible" >
                @endif
      
                </a>
                      <h4 class="text-center">{{$patient->name}}</h4>
                      <h5 class="text-center">{{$patient->home}}</h5>
                      <h5 class="text-center">Cel: {{$patient->phone}}</h5>

                    <form class="text-center" action="{{route('FilterPatient',['patient'=>$patient->id])}}">
                        <button type="submit" class="btn">Ver</button>
                      </form>
          </div>
        </div>
          @endforeach

          @endisset
        </div>
</div>
        </div>
        
      </div>
 
    </div>
  
  <!--/ End Product Style 1  -->	

  
  
 
      <!-- Modal -->
      <div class="modal fade" id="modal_register" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <div class="modal-body">
              <div class="row no-gutters">           
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
                  <div class="quickview-content">
                    <h4 class="widget_title">Formulario de registro</h4>
  
                    @include('auth.formRegister')

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal end -->
  
  <!-- Modal -->
  <div class="modal fade" id="modal_login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
        </div>
        <div class="modal-body">
          <div class="row no-gutters">           
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 col-xs-12">
              <div class="quickview-content ">
                <div id="login_error" style="display: none" class="alert alert-danger alert-dismissible fade show">
                  
              </div>
                <h4 class="widget_title">Ups! parece que no iniciaste sesión</h4>

                @include('auth.formLogin')
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal end -->

  <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">

 



</section>
<!--############SECTION CHECKOUT#############-->
<!--############END SECTION CHECKOUT#############-->
@endsection

@section('scripts')
<script type="text/javascript">



 $(function(){


 })

  </script>   
@endsection