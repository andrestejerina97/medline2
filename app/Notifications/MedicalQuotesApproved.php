<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MedicalQuotesApproved extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $subject = sprintf('%s: Solicitud de cita médica.', config('app.name'));
        $greeting = sprintf('Hola, %s!', $notifiable->name);
 
        return (new MailMessage)
        ->subject($subject)
        ->greeting($greeting)
        ->line('Te informamos que tu solicitud fue aprobada por el profesional solicitado')
        ->line('No olvides la fecha y hora de tu cita.')
        ->line('Por cualquier duda o consulta el equipo Medline está a tu disposición')
        ->from('info@medlineapp.com', 'Medlineapp')
        ->salutation('Saludos!');

    
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
