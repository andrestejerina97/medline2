<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medical_reports extends Model
{
    protected $table = 'medical_reports';
    protected $fillable = ['evaluation','observation'];
   protected $guarded = ['id'];
}
