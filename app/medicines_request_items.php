<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicines_request_items extends Model
{
    protected $table = 'medicines_request_items';
    protected $fillable = ['id_request','id_medicines','quantity','rebate','aggregate','subtotal'];
   protected $guarded = ['id'];

   public static function MedicinesRequestItems($numberTruck)
{
    $listRequest= medicines_request_items::join('medicines',function($join){
        $join->on('medicines.id','=','medicines_request_items.id_medicines');})
        ->where('medicines_request_items.id_request','=',$numberTruck)
        ->get();


        return $listRequest;
}
}
