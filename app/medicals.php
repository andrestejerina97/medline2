<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicals extends Model
{
    protected $table = 'medicals';
    protected $fillable = ['name','age','phone','cellphone','home','mail','ci','birthdate','id_clinic','id_specialty','patients_per_day','work_phone','price_inquire','id_packeges','consulting_room','attention_hours','photo','destake','active','id_user'];
    protected $guarded = ['id'];
}
