<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicines_stock extends Model
{
    protected $table = 'medicines_stock';
    protected $fillable = ['id_medicine','stock'];
  
}
