<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class report_files extends Model
{
    protected $table = 'report_files';
    protected $fillable = ['id_report','photo'];
   protected $guarded = ['id_report'];
   public $incrementing = false;
   public $timestamps = false;
}
