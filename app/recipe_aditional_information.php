<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recipe_aditional_information extends Model
{
    protected $table = 'recipe_aditional_informations';
    protected $fillable = ['name_patient','lastname_patient','cedula','email'];
    protected $guarded = ['id'];

}
