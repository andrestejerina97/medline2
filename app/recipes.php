<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recipes extends Model
{
    protected $table = 'recipes';
    protected $fillable = ["id_disease,id_medical_quote,id_medicine,quantity,id_aditional_information"];
    protected $guarded = ['id'];
}
