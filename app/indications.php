<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class indications extends Model
{
    protected $table = 'indications';
    protected $fillable = ['id_medicine','consume_quantity','consume_frecuency','observation','id_aditional_information'];
    protected $guarded = ['id'];
}
