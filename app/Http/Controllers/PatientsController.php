<?php

namespace App\Http\Controllers;

use App\clinic_histories;
use App\clinical_record;
use App\medicals;
use App\medicals_quotes;
use App\patients;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PatientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function home()
    {
        $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
        if (count($medical_id)>0) {
            $medical_id=$medical_id[0];
        }else{
            $medical_id=0;
        }
        $patients=clinic_histories::join('patients','clinic_histories.id_user','=','patients.id')
            ->where('clinic_histories.id_medical','=',$medical_id)
            ->select('patients.id','patients.name','patients.phone','patients.home')
            ->distinct()
            ->simplePaginate(15);// medicals::all();
        
        return view('public.patients.home')
        ->with('patients',$patients);
       }

       public function filter($id_patient)
       {
        $users= patients::join('users','users.id','patients.id_user')
        ->where('patients.id','=',$id_patient)
        ->select('patients.*')
        ->get();

            $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
            if (count($medical_id)>0) {
                $medical_id=$medical_id[0];
            }else{
                $medical_id=0;
            }

        $clinic_histories=   $clinic_histories= clinic_histories::join('medical_reports','medical_reports.id','=','clinic_histories.id_medical_report')
        ->join('medicals_quotes','medicals_quotes.id','=','clinic_histories.id_medical_quote')
        ->select('medicals_quotes.date_quote','medicals_quotes.time_quote','medicals_quotes.reason','medicals_quotes.current_illness','medicals_quotes.status','medical_reports.evaluation','medical_reports.observation','clinic_histories.id','clinic_histories.id_medical_quote','clinic_histories.id_medical_report')
        ->where('clinic_histories.id_user','=',$id_patient)
        ->where('clinic_histories.id_medical','=',$medical_id)
        ->where('medicals_quotes.status','>',1)
        ->orderBy('clinic_histories.created_at','desc')
        ->simplePaginate(15);

        $clinical_records=clinical_record::join('clinic_histories','clinic_histories.clinical_records_id','clinical_records.id')
        ->select('clinical_records.*')
        ->where('clinic_histories.id_user',$id_patient)
        ->orderBy('clinical_records.id','DESC')->limit(1)->get();
        
        
        $report_files= clinic_histories::join('report_files','report_files.id_report','=','clinic_histories.id_medical_report')
        ->where('clinic_histories.id_user','=',$id_patient)
        ->where('clinic_histories.id_medical','=',$medical_id)
        ->select('report_files.photo')
        ->get();

            return view('public.patients.filterPatient')
            ->with('report_files',$report_files)
            ->with('users',$users)
            ->with('clinical_records',$clinical_records)
            ->with('clinic_histories',$clinic_histories);      
         }

         public function update(Request $request)
         {
            $error="";
            $id_user=$request->input('patient_id');
            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
               // $allowedfileExtension=['jpg','png','jpeg'];
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check=1;//in_array($extension,$allowedfileExtension);
                    if($check){
                      
                        $namephoto=$id_user."-".$filename;
                        $path = $file->storeAs('public/patients',$namephoto);
                        $user=patients::find($id_user);
                        $user->photo=$namephoto;
                        $user->update();
                        
                    }
                }else{
                   $namephoto="";
                   $error="Formato de imagen incorrecta,foto no actualizada";
                }
                  
                
            $patients=DB::table('patients')->where('id','=',$id_user)
            ->update($request->except('_token','photo','patient_id'));

                return response()->json(['result'=>$id_user]);

         }
}
