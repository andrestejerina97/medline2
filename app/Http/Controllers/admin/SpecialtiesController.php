<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\specialties;

use Illuminate\Support\Facades\Storage;

class SpecialtiesController extends Controller
{
    public function home()
    {
        $specialties=specialties::all();
        return view('administrator.config.specialties')->with('specialties',$specialties);
    }
    public function save(Request $request)
    {
        $specialty=new specialties();
      
       // $currentUser = Auth::user();
        $specialty->name= $request->input('name');
      $specialty->save();
      $specialties=specialties::all();

      return redirect('/admin/Specialties')->with('specialties',$specialties);

    }
    public function delete(Request $request)
    {
        $specialty= specialties:: find($request->input('id'));
        $specialty->delete();

        $specialties=specialties::all();
        return redirect('/admin/Specialties')->with('specialties',$specialties);
    }
    public function update(Request $request)
    {
        $specialty= specialties:: find($request->input('idU'));
        $specialty->name= $request->input('nameU');
        $specialty->update();


        $specialties=specialties::all();
        return redirect('/admin/Specialties')->with('specialties',$specialties);
    }
}
