<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\medicines;
use App\medicines_request;
use Illuminate\Support\Facades\Storage;
use App\medicine_categories;
use App\medicines_stock;
use Illuminate\Validation\Validator;

class CategoriesController extends Controller
{
    public function home()
    {
        $categories=medicine_categories::all();
        return view('administrator.config.categories')->with('categories',$categories);
    }
    public function save(Request $request)
    {
        $category=new medicine_categories();
      
       // $currentUser = Auth::user();
        $category->name= $request->input('name');
      $category->save();
      $categories=medicine_categories::all();

      return redirect('/admin/Categories')->with('categories',$categories);

    }
    public function delete(Request $request)
    {
        $category= medicine_categories:: find($request->input('id'));
        $category->delete();

        $categories=medicine_categories::all();
        return redirect('/admin/Categories')->with('categories',$categories);
    }
    public function update(Request $request)
    {
        $category= medicine_categories:: find($request->input('idU'));
        $category->name= $request->input('nameU');
        $category->update();


        $categories=medicine_categories::all();
        return redirect('/admin/Categories')->with('categories',$categories);
    }
}
