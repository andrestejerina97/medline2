<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\packeges;


class PackegesController extends Controller
{
    
        public function home()
        {
            $packeges=packeges::all();
            return view('administrator.config.packeges')->with('packeges',$packeges);
        }
        public function save(Request $request)
        {
            $packege=new packeges();
          
           // $currentUser = Auth::user();
            $packege->name= $request->input('name');
          $packege->save();
          $packeges=packeges::all();
    
          return redirect('/admin/Packeges')->with('packeges',$packeges);
    
        }
        public function delete(Request $request)
        {
            $packege= packeges:: find($request->input('id'));
            $packege->delete();
    
            $packeges=packeges::all();
            return redirect('/admin/Packeges')->with('packeges',$packeges);
        }
        public function update(Request $request)
        {
            $packege= packeges:: find($request->input('idU'));
            $packege->name= $request->input('nameU');
            $packege->update();
    
    
            $packeges=packeges::all();
            return redirect('/admin/Packeges')->with('packeges',$packeges);
        }
}
