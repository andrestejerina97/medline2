<?php

namespace App\Http\Controllers\admin;

use App\clinics;
use App\Http\Controllers\Controller;
use App\medicals;
use App\packeges;
use App\specialties;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class UsersController extends Controller
{
    public function index(){

        return view('administrator.users.home');
    }
public function search(Request $request)
{
    $medicals=medicals::all();
    $clinics=clinics::All();
    $specialties=specialties::All();
    $packeges=packeges::all();

    $users=User::where('email','=',$request->input('emailSearch'))
        ->get();// medicals::all();
    
    //DB::select('select * from users where email like (?)',[$request->input('emailSearch')]);
    
    return view('administrator.medicals.newMedical')
    ->with('count',$users->count())
    ->with('users',$users)
    ->with('packeges',$packeges)
    ->with('specialties',$specialties)
    ->with('clinics',$clinics)
    ->with('medicals',$medicals);

}

public function home()
{
$users= User::join('roles',function($join){$join->on('roles.id','=','users.role_id');})
->select('users.name','users.email','users.created_at','users.phone','roles.type as nameRole')
->orderby('users.id','asc')
->simplePaginate(4);

$total_users=User::all()->count();
$total_customer=User::where('users.role_id','=',2)->get()->count();
$total_medical=User::where('users.role_id','=',3)->get()->count();
return view('administrator.users.home')
->with('users',$users)
->with('total_users',$total_users)
->with('total_customer',$total_customer)
->with('total_medical',$total_medical);
}

public function SearchUserTable(Request $request)
{
    $users= User::join('roles',function($join){$join->on('roles.id','=','users.role_id');})
    ->select('users.name','users.email','users.created_at','users.phone','roles.type as nameRole')
    ->where('users.name','like','%'.$request->input('searchUser').'%')
    ->orderby('users.id','asc')
    ->simplePaginate(10);
    $total_users=User::all()->count();
    $total_customer=User::where('users.role_id','=',2)->get()->count();
    $total_medical=User::where('users.role_id','=',3)->get()->count();
    return view('administrator.users.home')
    ->with('users',$users)
    ->with('total_users',$total_users)
    ->with('total_customer',$total_customer)
    ->with('total_medical',$total_medical);
}
}
