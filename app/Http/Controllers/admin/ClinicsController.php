<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\clinics;

use Illuminate\Support\Facades\Storage;

class ClinicsController extends Controller
{
    public function home()
    {
        $clinics=clinics::all();
        return view('administrator.config.clinics')->with('clinics',$clinics);
    }
    public function save(Request $request)
    {
        $clinic=new clinics();
      
       // $currentUser = Auth::user();
        $clinic->name= $request->input('name');
        $clinic->home= $request->input('home');

      $clinic->save();
      $clinics=clinics::all();

      return redirect('/admin/Clinics')->with('clinics',$clinics);

    }
    public function delete(Request $request)
    {
        $clinic= clinics:: find($request->input('id'));
        $clinic->delete();

        $clinics=clinics::all();
        return redirect('/admin/Clinics')->with('clinics',$clinics);
    }
    public function update(Request $request)
    {
        $clinic= clinics:: find($request->input('idU'));
        $clinic->name= $request->input('nameU');
        $clinic->home= $request->input('homeU');

        $clinic->update();


        $clinics=clinics::all();
        return redirect('/admin/Clinics')->with('clinics',$clinics);
    }
}
