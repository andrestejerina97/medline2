<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\carousel;

use Illuminate\Support\Facades\Storage;



class CarouselController extends Controller
{
    public function index(Request $request)
    {
        $carousels=carousel::all();
        return view('administrator.config.carousel')
        ->with('carousels',$carousels);
    }

    public function save(Request $request)
 {   
     
     $file = $request->file('file');
     $carousel= new carousel();
     $carousel->name=$request->input('name') ;
     $carousel->description=$request->input('description');
     $carousel->file= '';
     $carousel->save();
     $carousel->file= "Carrousel-".$carousel->id.".". $file->getClientOriginalExtension();
     $path = $file->storeAs('public/carousels',$carousel->file); 
     $path=Storage::url('public/carousels/'.$carousel->file);
     $carousel->update();
    //indicamos que queremos guardar un nuevo archivo en el disco local
    //Storage::disk('local')->put($nombre,  \File::get($file));
    
    $carousels=carousel::all();
    return redirect('admin/Carousel')
    ->with('carousels',$carousels);

 }

    public function update(Request $request)
    {
       $carousel= carousel:: find($request->input('idU'));
     
      // $currentUser = Auth::user();
      $carousel->name= $request->input('nameU');
      $carousel->description= $request->input('descriptionU');

      if ($request->hasFile('fileU')) {
       Storage::delete('public/carousels/'.$carousel->file); 
       $file = $request->file('fileU');
       $carousel->file="carousel-".$carousel->id.".". $file->getClientOriginalExtension();
       $path = $file->storeAs('public/carousels',$carousel->file);
   }
       
       $carousel->update();
    
       $carousels=carousel::all();
       return redirect('admin/Carousel')
       ->with('carousels',$carousels);
   
    }
    public function delete(Request $request)
    {
       $carousel= carousel:: find($request->input('id'));
       Storage::delete('public/carousels/'.$carousel->file);       
       $carousel->delete();

       $carousels=carousel::all();
       return redirect('admin/Carousel')
       ->with('carousels',$carousels);
    }
   
}
