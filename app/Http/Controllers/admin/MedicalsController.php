<?php

namespace App\Http\Controllers\admin;

use App\clinics;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\medicals;
use App\clinic;
use App\packeges;
use App\specialties;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MedicalsController extends Controller
{
    public function home()
    {
        $medicals=DB::table('medicals')
        ->join( 'specialties','medicals.id_specialty','=', 'specialties.id')
        ->join( 'clinics','medicals.id_clinic','=', 'clinics.id')
        ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic')
        ->simplePaginate(5);
       
      /*  $medicals=medicals::join('specialties',function($join){
            $join->on('medicals.id_specialty','=','specialties.id');})
            ->join('clinics',function($join){
                $join->on('medicals.id_clinic','=','clinics.id');}) 
                 ->simplePaginate(5);*/
        $clinics=clinics::All();
        $specialties=specialties::All();
        $packeges=packeges::all();
        return view('administrator.medicals.home')
        ->with('packeges',$packeges)
        ->with('specialties',$specialties)
        ->with('clinics',$clinics)
        ->with('medicals',$medicals);
    }
    public function save(Request $request)
    {
     $medical=new medicals();
      
       // $currentUser = Auth::user();
     $medical->name= $request->input('name');
     $medical->age= $request->input('age');
     $medical->phone=$request->input('phone');
     $medical->cellphone=$request->input('cellphone');
     $medical->phone= $request->input('phone');
     $medical->home=$request->input('home');
     $medical->mail=$request->input('mail');
     $medical->ci=$request->input('ci');
     $medical->birthdate=$request->input('birthdate');
     $medical->id_clinic=$request->input('clinics');
     $medical->id_specialty=$request->input('specialties');
     $medical->patients_per_day= $request->input('patients_per_day');
     $medical->work_phone=$request->input('price_inquire');
     $medical->id_packeges=$request->input('packeges');
     $medical->price_inquire=$request->input('price_inquire');
     $medical->attention_hours=$request->input('attention_hours');
     $medical->consulting_room=$request->input('consulting_room');
     $medical->destake=$request->input('destake');
     $medical->id_user=$request->input('id_user');
     $medical->active=1;

     $medical->photo='';

     $medical->save();
     $user= User:: find($request->input('id_user'));
     $user->role_id=3;
     $user->update();

     if ($request->hasFile('photo')) {
        $file = $request->file('photo');

        $medical->photo="medical-".$medical->id.".". $file->getClientOriginalExtension();

        $path = $file->storeAs('public/medicals',$medical->photo);
        $path=Storage::url('public/medicals/'.$medical->photo);
        $medical->update();

    }
 



    $messages="Médico agregado con éxito";

      $medicals=medicals::join('specialties',function($join){
        $join->on('medicals.id_specialty','=','specialties.id');})
        ->join('clinics',function($join){
            $join->on('medicals.id_clinic','=','clinics.id');})
        ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic')
        ->simplePaginate(3);
    $clinics=clinics::All();
    $specialties=specialties::All();
    $packeges=packeges::all();

    return redirect('/admin/Medicals')
    ->with('packeges',$packeges)
    ->with('specialties',$specialties)
    ->with('clinics',$clinics)
    ->with('messages',$messages)
    ->with('medicals',$medicals);

    }
    public function delete(Request $request)
    {
        $medical= medicals:: find($request->input('id'));
        $medical->delete();

        $medicals=medicals::all();
        return redirect('/admin/Medicals')->with('medicals',$medicals);
    }
    public function update(Request $request)
    {
        $medical= medicals::find($request->input('id_medical'));
          // $currentUser = Auth::user();
        $medical->name= $request->input('name');
        $medical->age= $request->input('age');
        $medical->phone=$request->input('phone');
        $medical->cellphone=$request->input('cellphone');
        $medical->phone= $request->input('phone');
        $medical->home=$request->input('home');
        $medical->mail=$request->input('mail');
        $medical->ci=$request->input('ci');
        $medical->birthdate=$request->input('birthdate');
        $medical->id_clinic=$request->input('clinics');
        $medical->id_specialty=$request->input('specialties');
        $medical->patients_per_day= $request->input('patients_per_day');
        $medical->work_phone=$request->input('price_inquire');
        $medical->id_packeges=$request->input('packeges');
        $medical->price_inquire=$request->input('price_inquire');
        $medical->attention_hours=$request->input('attention_hours');
        $medical->consulting_room=$request->input('consulting_room');
        $medical->destake=$request->input('destake');
        
        if ($request->hasFile('photo')) {
            Storage::delete('public/medicals/'.$medical->photo);
            $file = $request->file('photo');
            $medical->photo="medical-".$medical->id.".". $file->getClientOriginalExtension();
            $path = $file->storeAs('public/medicals/',$medical->photo);
            Storage::url('public/medicals/'.$medical->photo);
        }
        $medical->update();
   
        $messages="Médico Actualizado con éxito";

        $medicals=medicals::join('specialties',function($join){
          $join->on('medicals.id_specialty','=','specialties.id');})
          ->join('clinics',function($join){
              $join->on('medicals.id_clinic','=','clinics.id');})
          ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic')
          ->simplePaginate(3);
      $clinics=clinics::All();
      $specialties=specialties::All();
      $packeges=packeges::all();
  
      return redirect('/admin/Medicals')
      ->with('packeges',$packeges)
      ->with('specialties',$specialties)
      ->with('clinics',$clinics)
      ->with('messages',$messages)
      ->with('medicals',$medicals);
  
    }
    public function updateView($id_medical)
    {
        $medicals=medicals::join('specialties',function($join){
            $join->on('medicals.id_specialty','=','specialties.id');})
            ->join('clinics',function($join){
                $join->on('medicals.id_clinic','=','clinics.id');})
            ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic')
            ->where('medicals.id','=',$id_medical)
            ->get();

            $clinics=clinics::All();
            $specialties=specialties::All();
            $packeges=packeges::all();
            return view('administrator.medicals.update')
            ->with('packeges',$packeges)
            ->with('specialties',$specialties)
            ->with('clinics',$clinics)
            ->with('medicals',$medicals);

    }

    public function searchMedicalTable(Request $request)
    {
        $medicals=medicals::join('specialties',function($join){
            $join->on('medicals.id_specialty','=','specialties.id');})
            ->join('clinics',function($join){
                $join->on('medicals.id_clinic','=','clinics.id');})
            ->where('medicals.name','like','%'.$request->input("nameMedical").'%')
            ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic')
            ->simplePaginate(4);
            
        $clinics=clinics::All();
        $specialties=specialties::All();
        $packeges=packeges::all();
        return view('administrator.medicals.home')
        ->with('packeges',$packeges)
        ->with('specialties',$specialties)
        ->with('clinics',$clinics)
        ->with('medicals',$medicals);        
    }
}
