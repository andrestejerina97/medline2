<?php

namespace App\Http\Controllers;

use App\clinic_histories;
use App\clinical_record;
use App\medicals_quotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\medicals;
use App\Notifications\MedicalQuotesApproved;
use App\packeges;
use App\specialties;
use App\Notifications\MedicalQuotesNew;
use App\Notifications\MedicalQuotesNewMedical;
use App\patients;
use App\report_files;
use App\User;
use Illuminate\Support\Facades\Auth;
class MedicalsQuotesController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        
    return view('public.medicines.home');
}
public function ProfileHome(){
    $title="Mi perfil";

    return view('public.medicals.Profile',compact('title'));
}

Public function Update(Request $request){
    


}
public function home()
{
    //$medical=medicals::where('id_user','=',Auth::user()->id);
    $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
if (count($medical_id)>0) {
    $medical_id=$medical_id[0];
}else{
    $medical_id=0;
    
}
    $quotesToday=medicals_quotes::join('medicals',function($join){
        $join->on('medicals.id','=','medicals_quotes.id_medical');})
        ->join('users',function($join){
            $join->on('users.id','=','medicals_quotes.id_user');})
        ->where('medicals_quotes.status','= ',0)
        ->where('medicals_quotes.id_medical','=',$medical_id)
        ->select('medicals_quotes.*','users.name as nameUser')
        ->simplePaginate(6);
  
        return view('public.quotes.request')
        ->with('quotesToday',$quotesToday);

}

public function filter($clinical_history_id)
{
    $clinical_history= clinic_histories::find($clinical_history_id);

    $id_patient=$clinical_history->id_user;
    $users= patients::join('users','users.id','patients.id_user')
        ->where('patients.id','=',$clinical_history->id_user)
        ->select('patients.*')
        ->get();
    $clinical_records=clinical_record::join('clinic_histories','clinic_histories.clinical_records_id','clinical_records.id')
    ->select('clinical_records.*')
    ->where('clinic_histories.id_user',$id_patient)
    ->where('clinic_histories.id','=',$clinical_history_id)
    ->limit(1)->get();

    $clinic_histories=clinic_histories::join('medical_reports','medical_reports.id','=','clinic_histories.id_medical_report')
    ->join('medicals_quotes','medicals_quotes.id','=','clinic_histories.id_medical_quote')
    ->select('medicals_quotes.date_quote','medicals_quotes.time_quote','medicals_quotes.reason','medicals_quotes.current_illness','medicals_quotes.status','medical_reports.evaluation','medical_reports.observation','clinic_histories.id','clinic_histories.id_medical_quote','clinic_histories.id_medical_report')
    ->where('clinic_histories.id_user','=',$clinical_history->id_user)
    ->where('clinic_histories.id_medical','=',$clinical_history->id_medical)
    ->where('medicals_quotes.status','>',1)
    ->orderBy('clinic_histories.created_at','desc')
    ->simplePaginate(10);
        
    $report_files= report_files::join('clinic_histories','report_files.id_report','=','clinic_histories.id_medical_report')
    ->where('clinic_histories.id_user','=',$clinical_history->id_user)
    ->where('clinic_histories.id_medical','=',$clinical_history->id_medical)
    ->select('report_files.photo')
    ->get();
    $quotes=medicals_quotes::where('id','=',$clinical_history->id_medical_quote)->get();

        return view('public.quotes.filterQuote')
        ->with('quotes',$quotes)
        ->with('report_files',$report_files)
        ->with('users',$users)
        ->with('clinical_records',$clinical_records)
        ->with('clinical_history_id',$clinical_history_id)
        ->with('id_patient',$id_patient)
        ->with('clinic_histories',$clinic_histories);


}
public function newQuote($id_medical)
{
    $medicals=DB::table('medicals')
    ->join('specialties','medicals.id_specialty','=','specialties.id')
    ->join('clinics','medicals.id_clinic','=','clinics.id')
    ->where('medicals.id','=',$id_medical)
    ->select('medicals.*','specialties.name as nameSpecialty','clinics.home as homeClinic','clinics.name as nameClinic')
    ->get();

    /*$medicals=medicals::join('specialties',function($join){
        $join->on('medicals.id_specialty','=','specialties.id');})
        ->where('medicals.id','=',$id_medical)
        ->select('medicals.*','specialties.name as nameSpecialty')
        ->get();*/
    $specialties=specialties::All();
    $packeges=packeges::all();
    return view('public.quotes.new')
    ->with('packeges',$packeges)
    ->with('id_medical',$id_medical)
    ->with('specialties',$specialties)
    ->with('medicals',$medicals);   

}
public function saveQuoteResource($clinical_history_id,Request $request)
{
    $clinical_history=clinic_histories::find($clinical_history_id);
    $patient=patients::find($clinical_history->id_user);
    $quote= new medicals_quotes();
    $quote ->id_medical= $clinical_history->id_medical;
    $quote ->id_user= $patient->id_user;
    $quote ->date_quote= $request->input('date_quote');
    $quote ->time_quote= $request->input('time_quote');
    $quote ->reason= $request->input('reason');
    $quote ->current_illness= $request->input('current_illness');
    $quote ->status= '1';
    $quote->save();

    //create item in clinical history
    $clinical_history= new clinic_histories();
    $clinical_history->id_medical=$quote->id_medical;
    $clinical_history->id_medical_quote=$quote->id;
    $clinical_history->id_user=$patient->id;
    $clinical_history->save();
    //end create item...
    return response()->json(['result'=>1]); 

}
public function save(Request $request){
    $quote= new medicals_quotes();
    $quote ->id_medical= $request->input('id_medical');
    $quote ->id_user= Auth::user()->id;
    $quote ->date_quote= $request->input('date_quote');
    $quote ->time_quote= $request->input('time_quote');
    $quote ->reason= $request->input('reason');
    $quote ->current_illness= $request->input('current_illness');

    $quote ->status= '0';
    $quote->save();
    
    return response()->json(['result'=>1,'id_medical'=>$request->input('id_medical')]);
}

public function accept(Request $request)
{   $id_quote=$request->input("id_quote");
    $quote= medicals_quotes::find($id_quote);
    $quote->status=1;
    $quote->Update();
    //create patient if not exists
   // $patient=patients::where('id','=',$quote->id_user)->select('id')->pluck();
    $id_user= clinic_histories::select('clinic_histories.id_user') // id_user is id of patient table 
    ->join('patients','patients.id','clinic_histories.id_user')
    ->where('patients.id_user','=',$quote->id_user)
    ->where('clinic_histories.id_medical','=',$quote->id_medical)
    ->pluck('clinic_histories.id_user');
    if (count($id_user)>0) {
        $id_patient=$id_user[0];
        $patient= patients::find($id_patient);
    }else{
        $patient= new patients();
        $patient->id_user =$quote->id_user;
        $patient->save();
        $id_patient=$patient->id;
    }
    //end create patient
    //create item in clinical history
    $clinical_history= new clinic_histories();
    $clinical_history->id_medical=$quote->id_medical;
    $clinical_history->id_medical_quote=$id_quote;
    $clinical_history->id_user=$id_patient;
    $clinical_history->save();
    //end create item...
    $toUser= User::find($patient->id_user);
   // $toUser->notify(new MedicalQuotesApproved($toUser));

    return response()->json(['result'=>1,'id_user'=>$quote->id_user]);

}
public function SendAcceptedMail(Request $request)
{
    $id_user=$request->input("id_user");
    $toUser= User::find($id_user);
    $toUser->notify(new MedicalQuotesApproved($toUser));
    return response()->json(['result'=>1]);
}
public function SendNewMail(Request $request)
{
    $medical_id= medicals::select('id_user')->where('id','=',$request->input('id_medical'))->pluck('id_user');
    if (count($medical_id)>0) {
        $medical_id=$medical_id[0];
    }else{
        $medical_id=0;
    }
    $toUser= User::find(Auth::user()->id);
    $toUser2=User::find($medical_id);
    $toUser->notify(new MedicalQuotesNew($toUser));
    $toUser2->notify(new MedicalQuotesNewMedical($toUser2));

          // send notification using the "Notification" facade
    return response()->json(['result'=>1]);
}

public function finalizeQuote($clinical_history_id)
{    
    $clinical_history=clinic_histories::find($clinical_history_id);
    $medical_quote= medicals_quotes::find($clinical_history->id_medical_quote);
    $medical_quote->status=2;
    $medical_quote->update();
    return response()->json(['result'=>1]);

}
}