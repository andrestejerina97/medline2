<?php

namespace App\Http\Controllers;

use App\clinic_histories;
use App\clinical_record;
use App\indications;
use App\medical_reports;
use App\medicals;
use App\medicals_quotes;
use App\recipes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClinicHistoriesController extends Controller
{
    public function home()
    {
    
    }
    public function filterByQuote(Request $request)
    {
     
    $id_medical_report=$request->input("id_medical_report");
    $id_clinic_history=$request->input("id_clinic_history");
    $id_quote=$request->input("id_medical_quote");
     $recipes= recipes::join('medicines','medicines.id','=','recipes.id_medicine')
     ->select('recipes.id','recipes.created_at','recipes.quantity','medicines.name as nameMedicine','medicines.active_principle as principle')
     ->where('recipes.id_medical_quote','=',$id_quote)
     ->get();
     $indications=indications::join('medicines','medicines.id','=','indications.id_medicine')
     ->select('indications.consume_quantity','indications.consume_frecuency','indications.quantity_days','indications.observation','medicines.name as nameMedicine','medicines.active_principle as principle')
     ->where('id_medical_quote','=',$id_quote)
     ->get();
     return response()->json(['recipes'=>$recipes,'indications'=>$indications]); 

}

public function saveClinicalRecord($id_clinic_history,Request $request)
{
    $data=$request->except('_token');
    $clinical_history=clinic_histories::find($id_clinic_history);

    if ($clinical_history->clinical_records_id >0) {
        $clinical_record=clinical_record::where('id','=',$clinical_history->clinical_records_id)->update($data);
        return response()->json(['result'=>$clinical_history->clinical_records_id]);

    }else{
        $clinical_record=clinical_record::create($data);
        clinic_histories::where('id','=',$id_clinic_history)->update([
            'clinical_records_id'=>$clinical_record->id,
        ]);
        return response()->json(['result'=>$clinical_record->id]);

    }

   
}

}
