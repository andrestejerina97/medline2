<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class categoriesController extends Controller
{
    public function home()
    {
        $categories=medicine_categories::all();
        return view('administrator.config.categories')->with('categories',$categories);
    }
    public function delete()
    {
        $categories=medicine_categories::all();
        return view('administrator.config.categories')->with('categories',$categories);
    }
    public function update()
    {
        $categories=medicine_categories::all();
        return view('administrator.config.categories')->with('categories',$categories);
    }
}
