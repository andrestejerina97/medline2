<?php

namespace App\Http\Controllers\Auth;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Darryldecode\Cart\Validators\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
class LoginAjaxController extends Controller
{
  public function login(Request $request)
  {
     $credentials= $this->validate(request(),[
        'email'=>'email|required|string',
        'password'=>'required|string',
      ]);
      if (Auth::attempt($credentials)) {
        return response()->json(['message'=>'exito']);
      }else{
        return response()->json(['message'=>'error']);
      }
  }
  protected function validator(array $data)
  {
      return Validator::make($data, [
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
          'home' =>  ['required', 'string', 'max:255'],
          'phone' =>  ['required', 'string', 'max:255'],
          'code_postal' =>  ['required', 'string', 'max:255'],

          ]);
  }

  public function register(Request $request)
  {
    $credentials= $this->validate(request(),[
      'name' => ['required', 'string', 'max:255'],
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      'home' =>  ['required', 'string', 'max:255'],
      'phone' =>  'regex:/[0-9]/',
      'cedula' =>  'regex:/[0-9]/',
      'password' =>  ['required', 'string', 'max:255'],
      'country_name' =>  [ 'max:255'],
      'state_province' =>  ['max:255'],
    ]);
       $credentials['cedula']=$request->input('cedula');
       $credentials['role_id']=2;
       $credentials['country']=$request->input('country_name');
       $credentials['password']=Hash::make($request->input('password'));
      $user= User::create($credentials);
       // $user=new  User();
       // $user->name=
        //auth()->login($user);

          return response()->json(['message'=>'exito']);

  }
  

}
