<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\URL;
use PHPUnit\Util\RegularExpression;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    protected function redirectTo()
    {
        return URL::previous();
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'cedula' =>  'regex:/[0-9]/',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'home' =>  ['required', 'string', 'max:255'],
            'phone' =>  'regex:/[0-9]/',

            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'home' => $data['home'],
            'code_postal' => $data['code_postal'],
            'phone' => $data['phone'],
            'role_id' => 2,
            'country' => $data['country_name'],
            'cedula' => $data['cedula'],
            'state_province' => $data['state_province'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
