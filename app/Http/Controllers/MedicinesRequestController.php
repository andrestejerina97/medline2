<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\medicines_request;
use App\medicines_request_items;
use Illuminate\Support\Facades\Auth;

class MedicinesRequestController extends Controller
{


    public function messages()
{
    return [
        'home.required' => 'Debe colocar una dirección válido!',
        'phone.required' => 'Debe colocar un número válido',
        'phone.min' =>'Debe colocar un número válido de números',
        'name_customer' =>'Debe colocar un nombre válido',
    ];
}
    public function SavePurchase(Request $request){

        $credentials= $this->validate(request(),[
            'name_customer' => ['required', 'string', 'max:255'],
            'home' =>  ['required', 'string'],
            'phone' =>  ['required'],
            'country' =>  [ 'max:255'],
            'state_province' =>  ['max:255'],
          ]);
      

        $total=0;
        $medicineRequest= new medicines_request();
        $medicineRequest->id_user=Auth::user()->id;
        $medicineRequest->home=$request->input('home');
        $medicineRequest->payment=1;
        $medicineRequest->arrival=12;
        $medicineRequest->name_customer=$request->input('name_customer');
        $medicineRequest->phone=$request->input('phone');
        $medicineRequest->country_name=$request->input('country_name');
        $medicineRequest->state_province=$request->input('state_province');
        $medicineRequest->description_home=$request->input('description');
        $medicineRequest->code_postal=$request->input('code_postal');
        $nameKey= "total";
        $medicineRequest->total= $request->input($nameKey);       
        $medicineRequest->save();
        $id_Request=$medicineRequest->id;

        for ($i=1; $i <= $request->input('numberItems') ; $i++) {
        $medicineRequest_item= new medicines_request_items(); 
        $medicineRequest_item->id_request=$id_Request;
        $nameKey='numberItem'.$i;
        if($request->has($nameKey)){ 
        $nameKey='idproduct'.$i;
        $medicineRequest_item->id_medicines=$request->input($nameKey);
        $nameKey= "quantity".$i;
        $medicineRequest_item->quantity=(int)$request->input($nameKey);
        $nameKey= "subtotal".$i;
        $medicineRequest_item->subtotal=(float)$request->input($nameKey); 
        $medicineRequest_item->rebate=0;
        $medicineRequest_item->aggregate=0;
        $medicineRequest_item->save();
    }
    }

       $resultado="Su pedido se registró con éxito";
       return response()->json(array('message'=>'exito')); 
     }

     public function CartRegister(Request $request)
     {  
        //datos generales del usuario id
        $this->DeleteCart($request);        
        $nameKey= "numberItems";
        $numberItems= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey= "total";
        $total= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey= "totalItems";
        $totalItems= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey= "rebate";
        $rebate= $request->session()->put($nameKey,$request->input($nameKey));
        // datos de los items del carrito
        for ($i=1; $i <= $request->input('numberItems') ; $i++) {
        $nameKey='numberItem'.$i;
        if($request->has($nameKey)){ 
        $nameKey='idproduct'.$i;
        $idItem= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey='numberItem'.$i;
        $numberItem= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey= "description".$i;
        $description= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey= "name".$i;
        $name= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey= "quantity".$i;
        $quantity= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey= "price".$i;
        $price= $request->session()->put($nameKey,$request->input($nameKey));
        $nameKey= "subtotal".$i;
        $subtotal= $request->session()->put($nameKey,$request->input($nameKey));
         }

    }
    // $request->session()->flush();
       $resultado="Su pedido se registró con éxito";
       return response()->json($request->session()->all()); 
       //return redirect('/Medicines')->with("resultado",$resultado);
     }
     public function DeleteCart(Request $request)
     {
        
        for ($i=1; $i <= $request->session()->get('numberItems') ; $i++) {
        $nameKey='numberItem'.$i;
        if($request->session()->has($nameKey)){ 
        $nameKey='idproduct'.$i;
        $idItem= $request->session()->forget($nameKey);
        $nameKey='numberItem'.$i;
        $numberItem= $request->session()->forget($nameKey);
        $nameKey= "description".$i;
        $description= $request->session()->forget($nameKey);
        $nameKey= "name".$i;
        $name= $request->session()->forget($nameKey);
        $nameKey= "quantity".$i;
        $quantity= $request->session()->forget($nameKey);
        $nameKey= "price".$i;
        $price= $request->session()->forget($nameKey);
        $nameKey= "subtotal".$i;
        $subtotal= $request->session()->forget($nameKey);
         }

    }
     }

}
