<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\medicines;
use App\medicine_categories;
use Illuminate\Support\Facades\DB;
class MedicinesController extends Controller
{
    public function index(){
        $medicines= DB::table('medicines')
        ->where('medicines.destake','=',1)
        ->simplePaginate(6);// medicines::all();
        $categories=medicine_categories::all();
    
        return view('public.medicines.home')
        ->with('medicines',$medicines)
        ->with('categories',$categories)
        ;
    }
    public function ProfileHome(){
        $title="Mi perfil";
    
        return view('public.medicals.Profile',compact('title'));
    }
    
    Public function Update(Request $request){
        
    
    
    }
    public function search(Request $request)
    {
        $medicines= medicines::where('name','like','%'.$request->input('searchInput').'%')
        ->simplePaginate(6);// medicines::all();
        $categories=medicine_categories::all();
    
        return view('public.medicines.home')
        ->with('medicines',$medicines)
        ->with('categories',$categories);
    }

}
