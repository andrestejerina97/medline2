<?php

namespace App\Http\Controllers;

use App\carousel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $carousels=carousel::all();
        $medicines= DB::table('medicines')
        ->where('medicines.destake','=',1)
        ->simplePaginate(12);// medicines::all();
        return view('home')
        ->with('medicines',$medicines)
        ->with('carousels',$carousels);
    }
}
