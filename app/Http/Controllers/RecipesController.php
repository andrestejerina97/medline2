<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\medicals;
use App\patients;
use App\User;
use App\clinic_histories;
use App\diseases;
use App\indications;
use App\medicals_quotes;
use App\medicine_categories;
use App\medicines;
use App\recipe_aditional_information;
use App\recipes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

//use Illuminate\Support\Facades\PDF;

class RecipesController extends Controller
{
   public function home($id_quote)
   {
    $quote= medicals_quotes::find($id_quote);
    $users= patients::join('users',function($join){
        $join->on('users.id','=','patients.id_user');})
        ->where('patients.id_user','=',$quote->id_user)
        ->get();
    $medical = medicals::join('specialties',function($join){
        $join->on('medicals.id_specialty','=','specialties.id');})
        ->join('clinics',function($join){
            $join->on('medicals.id_clinic','=','clinics.id');})
        ->where('medicals.id','=',$quote->id_medical)        
        ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic','clinics.home as homeClinic')
        ->get();// medicals::all();
    $clinic_histories= clinic_histories::join('medical_reports',function($join){
        $join->on('medical_reports.id','=','clinic_histories.id_medical_report');})
    ->where('clinic_histories.id_user','=',$quote->id_user)
    ->get();
    
    $report_files= clinic_histories::join('medical_reports',function($join){
        $join->on('medical_reports.id','=','clinic_histories.id_medical_report');})
        ->join('report_files',function($join){
            $join->on('medical_reports.id','=','report_files.id_report');})
        ->where('clinic_histories.id_user','=',$quote->id_user)
        ->where('report_files.photo')
        ->get();
    $categories= medicine_categories::all();


$quotes=$id_quote;

        return view('public.recipes.home')
        ->with('quotes',$quotes)
        ->with('categories',$categories)
        ->with('medicals',$medical)
        ->with('report_files',$report_files)
        ->with('users',$users)
        ->with('clinic_histories',$clinic_histories);
   }
   public function newRecipe()
   { 
    $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
if (count($medical_id)>0) {
    $medical_id=$medical_id[0];
}else{
    $medical_id=0;
    
}
    $medical = medicals::join('specialties',function($join){
        $join->on('medicals.id_specialty','=','specialties.id');})
        ->join('clinics',function($join){
            $join->on('medicals.id_clinic','=','clinics.id');})
        ->where('medicals.id','=',$medical_id)        
        ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic','clinics.home as homeClinic')
        ->get();// medicals::all();
   
    $categories= medicine_categories::all();
        return view('public.recipes.newRecipe')
        ->with('categories',$categories)
        ->with('medicals',$medical);
   }


   public function searchDiseases(Request $request)
   {
    $diseases= diseases::where('descripcion_CIE','like','%'.$request->input("name_disease").'%')
    ->take(15)->get();// medicines::all();

    return response()->json(['diseases'=>$diseases]);

   }
   public function searchMedicine(Request $request)
   {
    $medicines= medicines::where('name','like','%'.$request->input("name_medicine").'%')
    ->get();// medicines::all();

  return response()->json(['medicines'=>$medicines]);
;
   }

   public function save(Request $request)
   {
    $countRecipe=(int)$request->input('count_medicines');
    $id_medical_quote=(int)$request->input('id_medical_quote');

    for ($i=1; $i <=$countRecipe; $i++) {
        $recipe= new recipes();
        $nameKey="id_disease".$i;
        $recipe->id_disease= $request->input($nameKey); 
        $nameKey="id_medicine".$i;
        $recipe->id_medicine= $request->input($nameKey); 
        $nameKey="quantity".$i;
        $recipe->quantity= $request->input($nameKey);
        $recipe->id_medical_quote = $id_medical_quote ; 
 
        $recipe->save();       
    }
    for ($i=1; $i <= $request->input('count_indication') ; $i++) {
        $indication= new indications();
        $nameKey="id_medicine_indication".$i;
        $indication->id_medicine= $request->input($nameKey); 
       
        $nameKey="consume_quantity".$i;
        $indication->consume_quantity= $request->input($nameKey); 
       
        $nameKey="consume_frecuency_text".$i;
        $consume_frecuency_text=$request->input($nameKey);
        $nameKey="consume_frecuency".$i;
        $indication->consume_frecuency= $request->input($nameKey)." ".$consume_frecuency_text; 
   
        $nameKey="quantity_days_text".$i;
        $quantity_days_text= $request->input($nameKey); 
        $nameKey="quantity_days".$i;
        $indication->quantity_days= $request->input($nameKey)." ".$quantity_days_text; 
      
        $nameKey="observation".$i;
        $indication->observation= $request->input($nameKey); 
        $indication->id_medical_quote = $id_medical_quote ; 

        $indication->save();
    }
    return response()->json(['message'=>1,'id_medical_quote'=>$id_medical_quote]);

}
public function saveNewRecipe(Request $request)
{
    //informacion adicional
    $information_aditional= new recipe_aditional_information();
    $information_aditional->name_patient= $request->input("name_patient");
    $information_aditional->lastname_patient= $request->input("last_name_patient");
    $information_aditional->cedula= $request->input("cedula_patient");
    $information_aditional->email= $request->input("email_patient");
    $information_aditional->id_medical= $request->input("id_medical");

    $information_aditional->save();
    $countRecipe=(int)$request->input('count_medicines');
    //end informacion adicional
    for ($i=1; $i <=$countRecipe; $i++) {
        $recipe= new recipes();
        $nameKey="id_disease".$i;
        $recipe->id_disease= $request->input($nameKey); 
        $nameKey="id_medicine".$i;
        $recipe->id_medicine= $request->input($nameKey); 
        $nameKey="quantity".$i;
        $recipe->quantity= $request->input($nameKey); 
        $recipe->id_aditional_information=$information_aditional->id;
        $recipe->save();       
    }
    for ($i=1; $i <= $request->input('count_indication') ; $i++) {
        $indication= new indications();
        $nameKey="id_medicine_indication".$i;
        $indication->id_medicine= $request->input($nameKey); 
       
        $nameKey="consume_quantity".$i;
        $indication->consume_quantity= $request->input($nameKey); 
       
        $nameKey="consume_frecuency_text".$i;
        $consume_frecuency_text=$request->input($nameKey);
        $nameKey="consume_frecuency".$i;
        $indication->consume_frecuency= $request->input($nameKey)." ".$consume_frecuency_text; 
   
        $nameKey="quantity_days_text".$i;
        $quantity_days_text= $request->input($nameKey); 
        $nameKey="quantity_days".$i;
        $indication->quantity_days= $request->input($nameKey)." ".$quantity_days_text; 
      
        $nameKey="observation".$i;
        $indication->observation= $request->input($nameKey); 
  
        $indication->id_aditional_information= $information_aditional->id; 
        $indication->save();
    }

  //  $this->downloadPdf();

    return response()->json(['message'=>1,'id_information'=>$information_aditional->id]);


}

public function downloadPdf(Request $request)
{ 
    if ($request->has('id_information')) {
        $id_aditional_information=$request->input('id_information');
        
     $indications= indications::join('medicines','indications.id_medicine','=','medicines.id')
     ->where('indications.id_aditional_information','=',$id_aditional_information)
     ->get();
     
     $recipes= recipes::join('diseases','recipes.id_disease','=','diseases.CIE10')
    ->join('medicines','recipes.id_medicine','=','medicines.id')
     ->where('recipes.id_aditional_information','=',$id_aditional_information)
     ->get();

     $information_aditionals=recipe_aditional_information::where('id','=',$request->input('id_information'))->get();

    }else{
        $identity_value=$request->input('id_medical_quote');
        $id_aditional_information=$request->input('id_medical_quote');

        $identity_column_recipes='recipes.id_medical_quote';
        $indications= indications::join('medicines','indications.id_medicine','=','medicines.id')
        ->where('indications.id_medical_quote','=',$id_aditional_information)
        ->get();
        
        $recipes= recipes::join('diseases','recipes.id_disease','=','diseases.CIE10')
       ->join('medicines','recipes.id_medicine','=','medicines.id')
        ->where('recipes.id_medical_quote','=',$id_aditional_information)
        ->get();

        $information_aditionals=User::join('medicals_quotes','medicals_quotes.id_user','=','users.id')
        ->where('medicals_quotes.id','=',$identity_value)
        ->select('users.name as name_patient','users.cedula')
        ->get();

    }
    if( $identity_value =! ''){


     $pdf = App::make('dompdf.wrapper');

     $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
     if (count($medical_id)>0) {
         $medical_id=$medical_id[0];
     }else{
         $medical_id=0;
         
     }

     $medicals= medicals::join('specialties',function($join){
        $join->on('medicals.id_specialty','=','specialties.id');})
        ->join('clinics',function($join){
            $join->on('medicals.id_clinic','=','clinics.id');})
        ->where('medicals.id','=',$medical_id)        
        ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic','clinics.home as homeClinic')
        ->take(1)->get();// medicals::all();*/

    $fecha=date("Y-m-d H:i:s");

    $view =View::make('public.recipes.viewPdf', compact('medicals', 'recipes','information_aditionals','indications','fecha'))->render();


    $pdf = $pdf->loadHtml($view)->setPaper('a5','landscape')->setWarnings(false);    

    $info = ['info'=>'info'];

            $sending= recipe_aditional_information::find($request->input('id_information'));
  //  $data["email"]=$sending->email;
   // $data["client_name"]=$sending->name_patient." ".$sending->lastname_patient;
   // $data["subject"]="Envío de receta medline";

   /* Mail::send('public.recipes.mail', $data, function($message)use($data,$pdf) {
        $message->to($data["email"], $data["client_name"])
        ->subject($data["subject"])
        ->attachData($pdf->output(), "invoice.pdf");
        });*/
   return $pdf->stream('MiRecetaMedline'.$fecha.'.pdf');
}else{
    return response()->json(['message'=>0]);
}
}
public function showAll()
{
     $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
     if (count($medical_id)>0){
         $medical_id=$medical_id[0];
     }else{
         $medical_id=0;
     }
     $recipes= recipe_aditional_information::where('recipe_aditional_informations.id_medical','=',$medical_id)
     ->simplePaginate(15);
        return view('public.recipes.showAll')
        ->with('recipes',$recipes);
}

public function delete($id)
{
 $recipe_information= recipe_aditional_information::find($id);
 
 $indication=indications::where('id_aditional_information','=',$recipe_information->id)->delete();
 $recipe=recipes::where('id_aditional_information','=',$recipe_information->id)->delete();
 $recipe_information->delete();
 $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
 if (count($medical_id)>0){
     $medical_id=$medical_id[0];
 }else{
     $medical_id=0;
 }
 $recipes= recipe_aditional_information::where('recipe_aditional_informations.id_medical','=',$medical_id)
 ->simplePaginate(15);
    return redirect('/Recipes/show/all')
    ->with('recipes',$recipes);
}




public function sendPdf($id)
{ //$request->has('id_information')
    if ($id) {
        $id_aditional_information=$id;//$request->input('id_information');
        
     $indications= indications::join('medicines','indications.id_medicine','=','medicines.id')
     ->where('indications.id_aditional_information','=',$id_aditional_information)
     ->get();
     
     $recipes= recipes::join('diseases','recipes.id_disease','=','diseases.CIE10')
    ->join('medicines','recipes.id_medicine','=','medicines.id')
     ->where('recipes.id_aditional_information','=',$id_aditional_information)
     ->get();

     $information_aditionals=recipe_aditional_information::where('id','=',$id)->get();

    }else{
        $identity_value=2;//$request->input('id_medical_quote');
        $id_aditional_information=3;//$request->input('id_medical_quote');

        $identity_column_recipes='recipes.id_medical_quote';
        $indications= indications::join('medicines','indications.id_medicine','=','medicines.id')
        ->where('indications.id_medical_quote','=',$id_aditional_information)
        ->get();
        
        $recipes= recipes::join('diseases','recipes.id_disease','=','diseases.CIE10')
       ->join('medicines','recipes.id_medicine','=','medicines.id')
        ->where('recipes.id_medical_quote','=',$id_aditional_information)
        ->get();

        $information_aditionals=User::join('medicals_quotes','medicals_quotes.id_user','=','users.id')
        ->where('medicals_quotes.id','=',$identity_value)
        ->select('users.name as name_patient','users.cedula')
        ->get();

    }
    if( $identity_value =! ''){


     $pdf = App::make('dompdf.wrapper');

     $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
     if (count($medical_id)>0) {
         $medical_id=$medical_id[0];
     }else{
         $medical_id=0;
         
     }

     $medicals= medicals::join('specialties',function($join){
        $join->on('medicals.id_specialty','=','specialties.id');})
        ->join('clinics',function($join){
            $join->on('medicals.id_clinic','=','clinics.id');})
        ->where('medicals.id','=',$medical_id)        
        ->select('medicals.*','specialties.name as nameSpecialty','clinics.name as nameClinic','clinics.home as homeClinic')
        ->take(1)->get();// medicals::all();*/

    $fecha=date("Y-m-d H:i:s");

    $view =View::make('public.recipes.viewPdf', compact('medicals', 'recipes','information_aditionals','indications','fecha'))->render();


    $pdf = $pdf->loadHtml($view)->setPaper('a5','landscape')->setWarnings(false);    

    $info = ['info'=>'info'];

            $sending= recipe_aditional_information::find($id_aditional_information);
  //  $data["email"]=$sending->email;
   // $data["client_name"]=$sending->name_patient." ".$sending->lastname_patient;
   // $data["subject"]="Envío de receta medline";

   /* Mail::send('public.recipes.mail', $data, function($message)use($data,$pdf) {
        $message->to($data["email"], $data["client_name"])
        ->subject($data["subject"])
        ->attachData($pdf->output(), "invoice.pdf");
        });*/
   // Mail::to($request->user())->send(new OrderShipped($order));

    Mail::send("public.templates.mail.recipe",['nombre'=>"andres"], function ($mail) use ($pdf) {
        $mail->from('info@medlineapp.com', 'Medlineapp');
        $mail->to('gerencia@ipegrupocomercial.com');
        $mail->attachData($pdf->output(), 'MirecetaMedline.pdf');
    });
  // return $pdf->stream('MiRecetaMedline'.$fecha.'.pdf');
}else{
    return response()->json(['message'=>0]);
}
}
}
