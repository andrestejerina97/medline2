<?php

namespace App\Http\Controllers;

use App\clinic_histories;
use App\medical_reports;
use App\medicals;
use App\medicals_quotes;
use App\patients;
use App\report_files;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReportFilesController extends Controller
{
    public function save($id_clinic_history,Request $request)
    {

        $clinical_history=clinic_histories::find($id_clinic_history);
        if ($clinical_history->id_medical_report >0) {
            $report=medical_reports::find($clinical_history->id_medical_report);
            $report->evaluation=$request->input('evaluation');
            $report->observation=$request->input('observation');
            $report->save();
    
        }else{
            $report= new medical_reports();
            $report->evaluation=$request->input('evaluation');
            $report->observation=$request->input('observation');
            $report->save();
            $clinical_history->id_medical_report=$report->id;
            $clinical_history->save();
        }

        $id=$report->id;

        if ($request->hasFile('files')) {
            $files = $request->file('files');
            $allowedfileExtension=['jpg','png','jpeg'];
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
                if($check){
                    $report_file= new report_files();
                    $report_file->id_report=$id;
                    $report_file->photo=$id."-".$filename;
                    $path = $file->storeAs('public/reports',$report_file->photo);
                    $report_file->save();
                }
            }
              
            }


        return response()->json(['result'=>1]);

}
}

