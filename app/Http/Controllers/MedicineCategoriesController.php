<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\medicines;

use App\medicine_categories;

class MedicineCategoriesController extends Controller
{
 function index( $category){
    $medicines= medicines::join('medicine_categories',function($join){
        $join->on('medicines.id_category','=','medicine_categories.id');})
        ->where('medicine_categories.id','=',$category)
        ->select('medicines.*','medicine_categories.name as nameCategory')
        ->simplePaginate(6);
        
    
    $categories=medicine_categories::all();
    return view('public.medicines.FilterCategory')
    ->with('medicines',$medicines)
    ->with('categories',$categories);

 }
}
