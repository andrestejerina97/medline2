<?php

namespace App\Http\Controllers;

use App\clinic_histories;
use App\medicals;
use App\medicals_quotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class QueriesController extends Controller
{
    public function home()
    {
        $medical_id= medicals::select('id')->where('id_user','=',Auth::user()->id)->pluck('id');
        if (count($medical_id)>0) {
            $medical_id=$medical_id[0];
        }else{
            $medical_id=0;
        }
        $quotesGeneral=clinic_histories::join('medicals_quotes','medicals_quotes.id','clinic_histories.id_medical_quote')
                ->join('patients','patients.id','clinic_histories.id_user')
                ->join('users','patients.id_user','users.id')
                ->where('medicals_quotes.status','=',1)
                ->where('medicals_quotes.id_medical','=',$medical_id)
                ->select('medicals_quotes.*','users.name as nameUser','clinic_histories.id as clinical_history_id')
                ->get();



            return view('public.queries.mycalendar')
            ->with('quotesGeneral',$quotesGeneral);
    }
}
