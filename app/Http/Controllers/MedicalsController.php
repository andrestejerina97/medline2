<?php

namespace App\Http\Controllers;

use App\clinic_histories;
use App\clinics;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\medicals;
use App\medicals_quotes;
use App\packeges;
use App\report_files;
use App\specialties;
use Illuminate\Support\Facades\Auth;

class MedicalsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function home()
    {
        

        $medicals=medicals::join('specialties',function($join){
            $join->on('medicals.id_specialty','=','specialties.id');})
            ->where('medicals.destake','=','1')
            ->select('medicals.*','specialties.name as nameSpecialty')
            ->simplePaginate(12);// medicals::all();
        $quotes_pendings=medicals_quotes::where('medicals_quotes.id_user','=',Auth::user()->id)
        ->where('medicals_quotes.status','=',1)        
        ->get()
        ->count();// medicals::all();
        $quotes_aprove=medicals_quotes::where('medicals_quotes.id_user','=',Auth::user()->id)
        ->where('medicals_quotes.status','=',0)        
        ->get()
        ->count();// medicals::all();

        $clinics=clinics::All();
        $specialties=specialties::orderBy('name','ASC')->get();
        $packeges=packeges::all();
        return view('public.medicals.home')
        ->with('packeges',$packeges)
        ->with('specialties',$specialties)
        ->with('clinics',$clinics)
        ->with('quotes_aprove',$quotes_aprove)
        ->with('quotes_pendings',$quotes_pendings)
        ->with('medicals',$medicals);
    }
    public function save(Request $request)
    {
     $medical=new medicals();
      
       // $currentUser = Auth::user();
     $medical->name= $request->input('name');
     $medical->age= $request->input('age');
     $medical->cellphone=$request->input('cellphone');
     $medical->phone= $request->input('phone');
     $medical->home=$request->input('home');
     $medical->mail=$request->input('mail');
     $medical->ci=$request->input('ci');
     $medical->birthdate=$request->input('birthdate');
     $medical->id_clinic=$request->input('clinics');
     $medical->id_specialty=$request->input('specialties');
     $medical->patients_per_day= $request->input('patients_per_day');
     $medical->work_phone=$request->input('price_inquire');
     $medical->id_packeges=$request->input('packeges');
     $medical->price_inquire=$request->input('price_inquire');
     $medical->attention_hours=$request->input('attention_hours');
     $medical->consulting_room=$request->input('consulting_room');

     $medical->photo='';

     $medical->save();


     $file = $request->file('photo');

     $medical->photo="medical-".$medical->id.".". $file->getClientOriginalExtension();
     

     $path = $file->storeAs('public/medicals',$medical->photo);
     $medical->photo=Storage::url('public/medicals/'.$medical->photo);
     $medical->update();
      $medicals=medicals::all();
      return redirect('/admin/Medicals')
      ->with('medicals',$medicals);

    }
    public function delete(Request $request)
    {
        $medical= medicals:: find($request->input('id'));
        $medical->delete();

        $medicals=medicals::all();
        return redirect('/admin/Medicals')->with('medicals',$medicals);
    }
    public function update(Request $request)
    {
        $medical= medicals:: find($request->input('idU'));
        $medical->name= $request->input('nameU');
        $medical->update();


        $medicals=medicals::all();
        return redirect('/admin/Medicals')->with('medicals',$medicals);
    }

    public function showMedical($idMedical)
    {
        $medicals=medicals::join('specialties',function($join){
            $join->on('medicals.id_specialty','=','specialties.id');})
            ->where('medicals.id','=',$idMedical)
            ->select('medicals.*','specialties.name as nameSpecialty')
            ->simplePaginate(6);// medicals::all();
        $clinics=clinics::All();
        $specialties=specialties::All();
        $packeges=packeges::all();
        return view('public.medicals.home')
        ->with('packeges',$packeges)
        ->with('specialties',$specialties)
        ->with('clinics',$clinics)
        ->with('medicals',$medicals);   
    }

    function filter( $category){
        
            $medicals=medicals::join('specialties',function($join){
                $join->on('medicals.id_specialty','=','specialties.id');})
                ->where('medicals.id_specialty','=',$category)
                ->select('medicals.*','specialties.name as nameSpecialty')
                ->simplePaginate(6);// medicals::all();
            $quotes_pendings=medicals_quotes::where('medicals_quotes.id_user','=',Auth::user()->id)
            ->where('medicals_quotes.status','=',1)        
            ->get()
            ->count();// medicals::all();
            $quotes_aprove=medicals_quotes::where('medicals_quotes.id_user','=',Auth::user()->id)
            ->where('medicals_quotes.status','=',0)        
            ->get()
            ->count();// medicals::all();
    
            $clinics=clinics::All();
            $specialties=specialties::All();
            $packeges=packeges::all();
            return view('public.medicals.home')
            ->with('packeges',$packeges)
            ->with('specialties',$specialties)
            ->with('clinics',$clinics)
            ->with('quotes_aprove',$quotes_aprove)
            ->with('quotes_pendings',$quotes_pendings)
            ->with('medicals',$medicals);
    
     }

 
}
