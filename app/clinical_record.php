<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clinical_record extends Model
{
    protected $table="clinical_records";
    protected $guarded = ['id','created_at','modified_at'];

}
