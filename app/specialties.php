<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class specialties extends Model
{
    protected $table = 'specialties';
    protected $fillable = ['name'];
    protected $guarded = ['id'];
}
