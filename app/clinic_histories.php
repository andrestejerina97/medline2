<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clinic_histories extends Model
{
    protected $table = 'clinic_histories';
    protected $fillable = ['description','id_user',"id_medical","id_medical_report","id_medical_quote"];
    protected $guarded = ['id'];
}
