<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\medicines_request_items;
use Illuminate\Support\Facades\Facade;

class medicines_request extends Model
{
    protected $table = 'medicines_requests';
    protected $fillable = ['id_user','name_customer','state_province','phone','country_name','code_postal','description_home','home','payment','arrival','total'];
    protected $guarded = ['id'];


}
