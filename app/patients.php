<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class patients extends Model
{
    protected $table = 'patients';
    protected $guarded = ['id','created_at','modified_at','id_user'];
}
