<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class packeges extends Model
{
    protected $table = 'packeges';
    protected $fillable = ['name'];
    protected $guarded = ['id'];
}
