<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clinics extends Model
{
    protected $table = 'clinics';
    protected $fillable = ['name'];
    protected $guarded = ['id'];
}
