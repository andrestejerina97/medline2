<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carousel extends Model
{
    protected $table = 'carousels';
    protected $fillable = ['name','description','name','file'];
    protected $guarded = ['id'];
}
